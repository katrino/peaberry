#!/usr/bin/env/python

import Peaberry as Pb
import numpy as np

# Choose the examples you want to test:
EXAMPLES = [ 4 ]


## Example 1: Create a graph without plotting features, save to contents file.

if 1 in EXAMPLES:

	# Construct a graph from random data
	g = Pb.Base.Graph("random")
	N = 10
	xdata = np.linspace(0, 10, N)
	ydata = np.random.normal(5, 1, N)
	for x,y in zip(xdata,ydata):
		g.addPoint(x, x*y, xerr=0.1, yerr=1)
		#g.addPoint(x, y, yerr=np.sqrt(N))

	# Export the graph's contents to CSV (Gnuplot) & JSON files
	g.save("test.dat")


## Example 2: Load a graph from saved file, apply fit, and plot.

if 2 in EXAMPLES:

	# Create a plot to hold multiple graphs
	p = Pb.Main.Plot("Example Plot")
	p.setLabels("index", "data")

	# Read data from previously saved CSV file
	g = Pb.CSV.Graph("graph", plot=p)
	g.fromFile("test.dat", x=1, y=2, xerr=(3,4), yerr=(5,6))  # read asymm. errors

	# ... or read data from a ROOT Canvas
	#g = Pb.Root.Graph("test", plot=p)
	#g.fromROOTCanvas("test.root")

	# Prepare a function to fit the data
	#func = Pb.Fits.Constant()
	func = Pb.Fits.Linear()
	#func = Pb.Fits.Parabolic()

	# Construct a fit model using a specific method
	#f = Pb.Base.Fit(func)
	#f = Pb.LMFit.Fit(func)
	#f = Pb.MINUIT.Fit(func)
	f = Pb.MCMC.Fit(func)

	f.useErrors(xerr=True, yerr=True)

	# Add the fit function to the graph
	g.setFit(f)

	# Add callback to apply the fit to the data
	g.addCallback(Pb.Callbacks.ApplyFit)
	#g.addCallback(Pb.Callbacks.ApplyFit, plot=True)

	# Add callbacks to save the data/results to JSON files
	g.addCallback(Pb.Callbacks.SaveGraphData)
	g.addCallback(Pb.Callbacks.SaveFitResults)

	# Show the plot
	p.generate(preview=True)


## Alternate version of example 2 which shows another fit on a second y-axis

if 2.5 in EXAMPLES:

	# Create a plot to hold multiple graphs
	p = Pb.Main.Plot("Example Plot")
	p.setLabels("index", "data")

	# Read data from previously saved CSV file
	g = Pb.CSV.Graph("graph", plot=p)
	g.fromFile("test.dat", x=1, y=2, xerr=(3,4), yerr=(5,6))  # read asymm. errors

	# ... or read data from a ROOT Canvas
	#g = Pb.Root.Graph("test", plot=p)
	#g.fromROOTCanvas("test.root")

	# Prepare a function to fit the data
	func = Pb.Fits.Constant()
	#func = Pb.Fits.Linear()
	#func = Pb.Fits.Parabolic()

	# Construct a fit model using a specific method
	f = Pb.Base.Fit(func)
	#f = Pb.LMFit.Fit(func)
	#f = Pb.MCMC.Fit(func)
	#f = Pb.MINUIT.Fit(func)

	# Add the fit function to the graph
	g.setFit(f)

	# Add callback to apply the fit to the data
	g.addCallback(Pb.Callbacks.ApplyFit)
	#g.addCallback(Pb.Callbacks.ApplyFit, plot=True)

	# Add callbacks to save the data/results to JSON files
	g.addCallback(Pb.Callbacks.SaveGraphData)
	g.addCallback(Pb.Callbacks.SaveFitResults)

	# Show the plot
	p.runCallbacks()
	p.plotAll()

	# Add second axis to the data frame
	ax0 = p.pltFrames[0]
	ax1 = ax0.twinx()

	# Make sure that legend is not overdrawn by second data set
	ax1.add_artist(p.legend)

	# Create another graph
	gg = Pb.Base.Graph("othergraph", plot=p)
	#gg.setStyle('', dict(color='r'))

	# Add simple fit function to the graph
	ff = Pb.Base.Fit(Pb.Fits.Linear())
	ff.fitfunc.setFitValues(dict(slope=-0.5, offset=-1))
	#ff.fitfunc.setFitDrawRange(3,9)

	gg.setFit(ff)

	# Plot the new data onto the second axis
	p.plotFit(ax1, gg)
	p.plotLegend(ax1)

	# Colorize the second axis
	ax0.spines['right'].set_color('r')  # note that ax0 is used here
	ax1.tick_params(axis='y', colors='r')

	p.savePlot("test")


## Example 3: Load graph contents and fit results from files

if 3 in EXAMPLES:

	# Create a plot to hold multiple graphs
	p = Pb.Main.Plot("Example Plot")
	p.setLabels("index", "data")

	# Adjust style options of the plot
	p.setStyle('plot', dict(residuals=False))
	p.setStyle('data', dict(marker='s', ms=5, capsize=0))
	p.setStyle('fit', dict(ls='--', lw=2))

	# Construct a new fit model using the function defined above
	f = Pb.Base.Fit(func)

	# Construct an empty graph with a fit model
	g = Pb.Base.Graph("graph", plot=p, fit=f)

	# Add callbacks to load the data/results from JSON files
	g.addCallback(Pb.Callbacks.LoadGraphData)
	g.addCallback(Pb.Callbacks.LoadFitResults)

	# Show the plot and save to graphics file
	p.generate(preview=True, savename="example")


## Example 4: Shows advanced plotting features

if 4 in EXAMPLES:

	# Create a plot to hold multiple graphs
	p = Pb.Main.Plot("Example Plot")
	p.setLabels("distance", "frequency", "ampltiude")
	p.setMargins(lmargin=0.1, rmargin=0.1, tmargin=0.15, bmargin=0.25)
	p.setLimits(ylimits=(0, 5))
	p.setStyle('plot', dict(grid=True, legend=False))
	p.setStyle('data', dict(ms=5))

	# Construct primary graph
	g = Pb.Base.Graph("primary", plot=p)
	g.setStyle('data', dict(color='k', lw=2, ls='-'))

	N = 100
	xdata = np.linspace(0, 10, N)
	ydata = np.random.normal(1, 0.1, N)

	g.setPoints(x=xdata, y=ydata)

	# Construct more graphs from random data
	modifiers = [ 1, 3, 5 ]
	for value in modifiers:
		g = Pb.Base.Graph("secondary", plot=p, colorvalue=value, secondary=True)

		N = 50
		xdata = np.linspace(0, 10, N)
		ydata = np.square(xdata + value)
		ydata *= np.random.normal(1, 0.05, N)

		g.setPoints(x=xdata, y=ydata)
		g.setAbsoluteErrors(xerr=0.05)
		g.setRelativeErrors(yerr=0.05)

	# Activate colormap with default settings (call this after adding graphs!)
	p.setColorMap()

	# Plot graphs and generate plot layout (call this before drawing any extras!)
	p.plotAll()

	# Draw second x-axis on top, showing reciprocal ticks of main axis
	cb = lambda x: "%.2f" % (1./x)
	p.drawSecondXaxis(label=u"invserse distance", callback=cb)

	# Draw colorbar below the plot at customized position
	p.drawColorBar(position='below', label=u"modifier value", ticks=modifiers)

	# Plot inset of the data, magnified to the first data points
	p.plotInset(position=(0.15, 0.55, 0.3, 0.25), xlimits=(-0.1,1.1), ylimits2=(-5, 50))

	# Show the plot and save to graphics file
	p.finalize(preview=True, savename="example")

