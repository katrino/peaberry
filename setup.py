#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from datetime import date

pkgversion = date.today().strftime('%y%m%d')

try:
      from Cython.Build import cythonize
      modules = cythonize("Peaberry/*.pyx")
except:
      print("*** Could not import Cython - skipping cythonized classes! ***")
      modules = []


from distutils.core import setup
setup(name        = 'Peaberry',
      version     = pkgversion,
      description = 'A plotting & post-analysis framework written in Python',
      author      = 'Jan Behrens',
      author_email= 'jan.d.behrens@uni-muenster.de',
      url         = 'https://gitlab.com/katrino/peaberry',
      packages    = ['Peaberry'],
      ext_modules = modules,
      )
