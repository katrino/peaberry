Peaberry package
================

Submodules
----------

Peaberry.Main module
--------------------

.. automodule:: Peaberry.Main
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.Base module
--------------------

.. automodule:: Peaberry.Base
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.Fits module
--------------------

.. automodule:: Peaberry.Fits
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.Callbacks module
-------------------------

.. automodule:: Peaberry.Callbacks
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.Beans module
---------------------

.. automodule:: Peaberry.Beans
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.Kassiopeia module
--------------------------

.. automodule:: Peaberry.Kassiopeia
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.MCMC module
--------------------

.. automodule:: Peaberry.MCMC
    :members:
    :undoc-members:
    :show-inheritance:

Peaberry.GaussProcMCMC module
-----------------------------

.. automodule:: Peaberry.GaussProcMCMC
    :members:
    :undoc-members:
    :show-inheritance:

Module contents
---------------

.. automodule:: Peaberry
    :members:
    :undoc-members:
    :show-inheritance:
