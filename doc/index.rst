========
Peaberry
========

.. Peaberry documentation master file, created by
   sphinx-quickstart on Thu Jun 25 16:30:36 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Peaberry's documentation!
====================================

Installation
------------

Peaberry uses a number of Python packages to do all the fancy stuff.
Since Peaberry is structured into submodules, it is possible to skip any
submodule that has missing package dependencies. However, in most case
you'd want to keep the core functionality and probably the data handling
interfaces. Here is a full list of the requirements:

* Packages needed for core functionality:
   * NumPy_
   * SciPy_
   * Matplotlib_
* Packages needed for data handling:
   * Interfaces to read output from Beans and Kassiopeia:
      * PyROOT_
      * root_numpy_
* Extensions packages:
   * Markov-Chain Monte Carlo (MCMC) fit algorithm:
      * emcee_
      * corner_ (formerly known as triangle_plot)
   * Gaussian Process extension for MCMC:
      * george_
      * Also needs the MCMC dependencies from above.

Most of these packages can be installed very easily with the PIP
(PIP installs Python) command.

.. note::
   You need to make sure that the packages are installed for the same
   Python version (2.x or 3.x) that you will be using.
   Peaberry itself is compatible with Python 3 as well, but this might
   not be true for some of the required packages (specifically PyROOT).

PIP allows to install packages in user-space, i.e. without the need
for root priviliges (a.k.a. ``sudo``). To install a package in your
home directory (under ``$HOME/local/lib/pythonX.Y/``), run the
following command on a terminal::
   pip install --user PACKAGE

Replace ``PACKAGE`` with the name of the package from the above list
(in lowercase letters, e.g. ``numpy``).

.. hint::
   If both Python 2 and 3 are installed on your system, you might need
   to use ``pip2`` instead of ``pip`` in order to install the package
   specifically for Python 2.

.. _NumPy: http://www.numpy.org/
.. _SciPy: http://www.scipy.org/
.. _Matplotlib: http://matplotlib.org/
.. _PyROOT: https://root.cern.ch/drupal/content/pyroot/
.. _root_numpy: https://pypi.python.org/pypi/root_numpy/
.. _emcee: http://dan.iel.fm/emcee/
.. _corner: https://github.com/dfm/corner.py
.. _george: http://dan.iel.fm/george/

Contents
--------

.. toctree::
   :maxdepth: 4

   Peaberry

Todo list
---------

.. todolist::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

