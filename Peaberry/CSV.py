#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function

import numpy as np
import csv

csv.register_dialect('gnuplot', delimiter=str('\t'), quoting=csv.QUOTE_NONE, skipinitialspace=True)

from Peaberry.Main import message
import Peaberry.Base as Base

########################################################################
########################################################################

"""
A simple class that represents a graph.

The class holds all data points and fit parameters, and can be used
to retrieve existing data from a CSV file, e.g. in Gnuplot's format.
"""
class Graph(Base.Graph):
    def __init__(self, name, label="", plot=None, fit=None):
        super(Graph, self).__init__(name, label, plot, fit)

        self.inputPath = '{}'

        self.dialect = 'gnuplot'
        self.comment = '#'
        self.decimal = '.'

    def fromFile(self, filename="", x=1, y=2, xerr=None, yerr=None):
        """
        Read data points from a CSV file.

        Note:
            The file format can be changed by the `self.dialect` option
            which is passed on to `csv.reader`. It defaults to `gnuplot`,
            but other dialects can be registered directly with the
            `csv.register_dialect` method from the `csv` module.

        Note:
            Columns are indexed by the order as they appear in the file,
            starting at one. For the x- and y-values, column `0` refers
            to the line number. By default, x- and y-values are
            retrieved from the first two columns and no errors are
            read from the file.

        Args:
            filename: Passed on to the run() function of the fit method.
            xvalues: The column index to use for x-values.
            yvalues: The column index to use for y-values.
            xerrors: The column index to use for x-errors.
                    Can be a 2-tuple to use asymmetric errors from two columns.
            yerrors: The column index to use for y-errors.
                    Can be a 2-tuple to use asymmetric errors from two columns.
        """
        if not filename:
            filename = self.name + ".dat"
        filename = self.inputPath.format(filename)

        with open(filename, 'rb') as csvfile:
            message("Reading CSV data from file '{0}' ...".format(filename), severity=1)

            def _read_float(row, index):
                if len(row) < index:
                    return None
                return float(row[index-1].replace(self.decimal, '.'))

            reader = csv.reader( csvfile, dialect=self.dialect )

            linecount = 0
            for row in reader:
                if len(self.comment) and row[0].startswith(self.comment):
                    message("Skipping comment: '{}'".format(' '.join(row)), severity=0, level=2)
                    continue
                linecount += 1

                if x == 0:
                    xvalue = linecount
                else:
                    xvalue = _read_float(row, x )

                if y == 0:
                    yvalue = linecount
                else:
                    yvalue = _read_float(row, y )

                xerror = None
                if xerr != None:
                    try:
                        xerror = ( _read_float(row, xerr[0]), _read_float(row, xerr[1]) )
                    except TypeError:
                        xerror = _read_float(row, xerr)

                yerror = None
                if yerr != None:
                    try:
                        yerror = ( _read_float(row, yerr[0]), _read_float(row, yerr[1]) )
                    except TypeError:
                        yerror = _read_float(row, yerr)

                self.addPoint( x=xvalue, y=yvalue, xerr=xerror, yerr=yerror )

            self.filename = filename

        return self

########################################################################
