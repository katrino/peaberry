#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from matplotlib.ticker import MaxNLocator, ScalarFormatter
from scipy import stats
from scipy import optimize

import emcee
import corner

import os
import json
from decimal import Decimal

from Peaberry.Main import message, exception
import Peaberry.Base as Base

########################################################################
########################################################################

class Fit(Base.Fit):
    """
    A class for fitting data points that implements Markov-Chain Monte Carlo.

    This basic implementation includes a simple model with a uniform
    prior (that will take care of any parameter constraints). It uses
    the MCMC methods from the ``emcee`` package to apply a likelihood
    fit to a number of data points defined in a graph or histogram
    object.
    The graph or histogram has to be associated with the fit object.

    The initializer sets the number of free fit parameters (based
    on the list of parameter names) and sets their start values to
    zero, their limits to ``None`` (i.e. no limits).

    Args:
        graph (instance): A graph/histogram to which the fit associates.
        parnames (list): A list of parameter names that also defines
            the number of free parameters for the fit function.

    Raises:
        TypeError: If ``graph`` is not an instance of ``Peaberry.Base.Graph``.

    See also:
        :py:class:`Peaberry.Base.Graph` is the base class for all graphs
        to which a fit can be applied.
    """
    def __init__(self, fitfunc, graph=None):
        super(Fit, self).__init__(fitfunc, graph)

        self.mcmcParams = OrderedDict()

        # make sure that MCMC parameters are initialized correctly
        self.ndim = len(self.fitfunc)
        self.kdim = len(self.mcmcParams)

    @staticmethod
    def _format_error(param, extend=0, texmode=False):
        value = Decimal(float(param.getValue()))
        errlo = Decimal(float(param.getErrorDown()))
        errhi = Decimal(float(param.getErrorUp()))

        vdigits = value.adjusted()
        edigits = min(errlo.adjusted(), errhi.adjusted())
        value   = Base.Fit._adjust_digits(value, edigits)
        errlo   = Base.Fit._adjust_digits(errlo, edigits)
        errhi   = Base.Fit._adjust_digits(errhi, edigits)

        if texmode:
            if errlo == errhi:
                return r"%s \pm %s" % (value, errlo)
            else:
                return r"%s_{-%s}^{+%s}" % (value, errlo, errhi)
        else:
            if errlo == errhi:
                return u"%s±%s" % (value, errlo)
            else:
                return u"%s-%s+%s" % (value, errlo, errhi)

    def _constrain(self, theta, limits):
        # add constraints to fit parameters
        for i in range(self.ndim + self.kdim):
            lo, hi = limits[i]
            if (lo == hi):  # override fixed parameters
                theta[i] = lo
                continue
            if not (lo <= theta[i] <= hi):
                return False

        return True

    def _lnprior(self, theta, limits, callback=None):
        if not self._constrain(theta, limits):
            return -np.inf

        # add extra information provided by callback function
        if callback != None:
            return -0.5 * callback(theta[:self.ndim])

        return 0.

    def _residuals(self, theta, x, y, xerr, yerr):
        p, k = theta[:self.ndim], theta[-self.kdim:]

        model = self.fitfunc.fit(x, *p)
        delta = y - model

        if yerr == None:
            sigma = model
        else:
            if xerr == None:
                sigma = yerr
            else:
                d = self._derivative(x, model)
                sigma = yerr + xerr * np.abs(d)

        return (delta, sigma)

        ## asymmetric errors
        ## [1] http://arxiv.org/pdf/physics/0406120v1.pdf
        ## [2] http://arxiv.org/pdf/physics/0403046v1.pdf
        #sigma = (2. * yerr[1] * yerr[0]) / (yerr[1] + yerr[0])
        #dsigma = (yerr[1] - yerr[0]) / (yerr[1] + yerr[0])
        #sigma_sqr = np.square(sigma + dsigma * delta)
        #delta_sqr = np.square(delta)
        #return -0.5 * np.sum( delta_sqr / sigma_sqr + np.log(sigma_sqr) )

    def _chisquare(self, p, x, y, xerr=None, yerr=None):

        try:
            delta, sigma = self._residuals(p, x, y, xerr, yerr)
        except AssertionError:
            return np.full(x.size, np.inf)

        return np.square(delta / sigma)

    def _lnlike(self, theta, x, y, xerr, yerr):

        try:
            delta, sigma = self._residuals(theta, x, y, xerr, yerr)
        except AssertionError:
            return -np.inf

        chi2 = np.square(delta / sigma)

        return -0.5 * ( chi2 + 2. * np.log(sigma) ).sum()

    def _lnprob(self, theta, x, y, xerr, yerr, limits, callback=None):
        # check constraints
        lp = self._lnprior(theta, limits, callback)
        if not np.isfinite(lp):
            return -np.inf

        # compute log-likelihood
        ll = self._lnlike(theta, x, y, xerr, yerr)
        if not np.isfinite(ll):
            return -np.inf

        return lp + ll

    def getLikelihood(self, popt=[]):
        """
        Compute the log-likelihood using the given fit parameters.

        If no fit parameters are defined, the current parameter set
        of the fit is used. This method returns the lnlike value.

        Args:
            popt (list): Parameters to be used in the calculation,\
                if not using the current values.

        Returns:
            lnlike (double): logarithmic likelihood.
        """
        if not len(popt):
            popt = self.fitfunc.getFitValues().values()

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            yerrors  = self.graph.getYerrors(filtered=True)

        xerrors = None
        if self.useXerrors and self.graph.hasErrorsX():
            xerrors  = self.graph.getXerrors(filtered=True)

        lnlike = self._lnlike(popt, xvalues, yvalues, xerrors, yerrors)

        return lnlike

    def printResults(self):
        chi2, ndf, prob = self.getChiSquare()
        ll              = self.getLikelihood()

        topline = u"order=%d ndf=%d chi2=%.3f chi2/ndf=%.3f p=%.6g ln(L)=%.4g" % \
                    (len(self), ndf, chi2, chi2/ndf if ndf > 0 else 0., prob, ll)
        linewidth = max(len(topline), 72)

        message(topline, level=3, bold=True)

        # parameter values
        message("=" * linewidth, level=3)
        message(u"%-12s    %12s %12s %12s %12s" % ("", "value", "error-", "error+", "error"), level=3, bold=True)
        for p in self.fitfunc.getFitParams().values():
            message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % \
                (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # nuisance parameter values
        if len(self.mcmcParams) > 0:
            message("-" * linewidth, level=3)
            for p in self.mcmcParams.values():
                message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % \
                    (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # extra values (for specialized fit functions)
        extra = self.fitfunc.getExtraResults()
        if extra:
            message("-" * linewidth, level=3)
            for p in extra.values():
                message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % \
                    (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # parameter correlations
        message("-" * linewidth, level=3)
        message(u"%12s   %-12s  %12s %12s %12s" % ("", "", "corr-", "corr+", "covar"), level=3, bold=True)
        params = self.fitfunc.getFitParams().values()
        while len(params) > 0:
            p = params[0]
            for q in params:
                message(u"%12s : %-12s  %+12.6g %+12.6g %12.6g" % \
                    (p.getName(), q.getName(), p.getCorrelationDown(q), p.getCorrelationUp(q), p.getCovariance(q) ), level=3)
            del params[0]

        message("-" * linewidth, level=3)

    def load(self, filename, exclude=[]):
        """
        Import fit results from a JSON file.

        Args:
            filename (string): The filename of the input file.
            exclude (list): List of parameter names to keep instead of
                    overriding them with results from the input file.
        """
        super(Fit, self).load(filename, exclude)

        # make sure that MCMC parameters are initialized correctly
        self.ndim = len(self.fitfunc)
        self.kdim = len(self.mcmcParams)

    def prepare(self):
        """
        Prepare the fit by setting all parameters and constraints.

        Returns:
            tuple: Starting parameters, parameter constraints
                and parameter overrides (fixed parameters),
                like ``( [p0, p1, ...], [ (p1_lo,p1_hi), (p2_lo,p2_hi), ... ], [o0, o1, ...] )``.
        """
        self.ndim = len(self.fitfunc)
        self.kdim = len(self.mcmcParams)

        params, limits = [], []

        # append fit parameters
        for p in self.fitfunc.getFitParams().values():
            params.append(p.getValue())
            if p.isFixed():
                limits.append((p.getValue(), p.getValue()))
            else:
                limits.append(p.getLimits())

        # append nuisance parameters
        for p in self.mcmcParams.values():
            params.append(p.getValue())
            limits.append(p.getLimits())

        return ( params, limits )

    def finish(self, samples, covar_cut=True):
        """
        Finish the fit by computing the uncertainties and saving the result.

        The uncertainties are computed from the projection of the sampled
        MCMC values, using quantiles to determine the 1-sigma intervals
        around the median value.

        Args:
            samples (list): MCMC samples after burn-in from ``emcee.EnsembleSampler``.
            covar_cut (bool): If `` True``, reduce the parameter space to 2-sigma
                regions before computing covariances (i.e. remove MCMC samples
                that are far away from the actual fit result).

        Returns:
            tuple: Resulting fit parameters and errors,
                like ``( [p0, p1, ...], [ (e1_lo,e1_hi), (e2_lo,p2_hi), ... ] )``.
        """
        # compute quantiles, e.g. 1 sigma = 68% -> 50% ± 34% -> q = 16 / 50 / 84
        #                         2 sigma = 95% -> 50% ± 48% -> q =  2 / 50 / 98
        quant = np.percentile(samples, [ 2, 16, 50, 84, 98 ], axis=0)
        result  = map(lambda x: x[2], zip(*quant))
        errors  = map(lambda x: (x[2]-x[1], x[3]-x[2]), zip(*quant))

        if covar_cut:
            # cut off samples outside sigma range to compute covariances
            for i in xrange(self.ndim + self.kdim):
                samples = samples[ (quant[0][i] <= samples[:,i]) & (samples[:,i] <= quant[-1][i]) ]

        covar = np.cov(samples.T)  # input data is transposed

        params = self.fitfunc.getFitParams().values()
        for i in xrange(self.ndim):
            params[i].setValue(result[i])
            params[i].setErrors(errors[i])
            for j in xrange(self.ndim):
                params[i].setCovariance(params[j], covar[i][j])

        params = self.mcmcParams.values()
        for i in xrange(self.kdim):
            k = i + self.ndim
            params[i].setValue(result[k])
            params[i].setErrors(errors[k])

        return ( result, errors )

    def run(self, plot=False, traceplot=False, triplot=False, symerr=True, verbose=True,
            steps=1000, burnin=200, heatup=0, walkers=20, radius=0.1, likelihood=False, callback=None, savefile=None, loadfile=None, **kwargs):
        """
        Run the fit method using the defined fit function and parameters.

        This implementation uses the minimization methods from
        ``emcee``. Extra parameters are passed on to the
        ``emcee.EnsembleSampler.run_mcmc`` function.
        The fit is initialized with the currently defined set of parameters.
        After running the fit, the parameters and their errors are updated
        with the fit results, and returned via ``getFitParams()``.

        Args:
            plot (bool): If `True`, produce a set of fit-performance
                plots (chi-square, probability).
            traceplot (bool): If `True`, produce a plot of the MCMC
                trace and auto-correlation.
            triplot (bool): If `True`, produce extra
                triangle/corner plot of fit results.
            symerr (bool): If `True`, always use symmetric errors
                in the chi-square minimization even if asymmetric
                errors are present in the data set.
            verbose (bool): If `True`, print progress while sampling.
            steps (int): The total number of MCMC steps per chain(!) that
                are used to determine the fit parameters (i.e. without burn-in).
            burnin (int): The number of MCMC steps per chain (!) to run
                before the "productive" part starts (the burn-in phase
                is used to ensure that MCMC is sampling close to the
                fit minimum to determine the fit parameters).
            heatup (int): The number of extra MCMC steps to run before the
                burn-in phase is started; the resulting parameters are
                then mixed again before the main MCMC run starts.
            walkers (int): The number of chains to run in parallel using
                emcee's ensemble sampler (see ``emcee.EnsembleSampler``).
            radius (float): A radius of a gaussian ball around the
                initial fit parameters, used to start the parallel chains
                at slightly different positions in parameter space.
            loglike (bool): If ``True``, a simple minimzation of the
                log-likelihood is performed to get a better set of
                initial parameters; may or may not improve the sampling.
            progress (int): Number of MCMC steps after which the current
                progress is displayed (percent completed + steps left).
            callback (func): A callback function that is passed on to
                ``:py:func:_lnprob`` and can be used to modify the
                log-likelihood used in the fitting process; e.g. to
                apply user-defined constraints to the parameters.
            savefile (string): Optional file to store the full chain
                after the MCMC run is finished; can be restored to
                add more steps to an existing chain (see ``loadfile``).
            loadfile (string): Optional file to initialize the chain
                before the MCMC run is started; new steps will be
                added to the existing chain (see ``savefile``).
            **kwargs (dict): Additional arguments passed to
                ``emcee.EnsembleSampler``.
        """
        if len(self) == 0:
            return

        if len(self.graph) < len(self):
            raise RuntimeError("{0}: number of data points {1} is too small for fit of order {2}".format(self.graph.className(), len(self.graph), len(self)))

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            if symerr:
                yerrors = self.graph.getYerrors(filtered=True)
            else:
                raise RuntimeError("Asymmetric errors are not supported (yet)!")
                #yerrors = self.graph.getYerrorsAsym(filtered=True)

        xerrors = None
        if self.useXerrors and self.graph.hasErrorsX():
            if symerr:
                xerrors = self.graph.getXerrors(filtered=True)
                #yerrors += self.graph.getTransformedXerrors(filtered=True)
            else:
                raise RuntimeError("Asymmetric errors are not supported (yet)!")
                #xerrors = self.graph.getXerrorsAsym(filtered=True)
                #yerrors += self.graph.getTransformedXerrorsAsym(filtered=True)

        message("Beginning the MCMC fit.", level=1)

        params, limits = self.prepare()
        message("Parameter constraints: {0}".format(" | ".join([ "{} .. {}".format(l,h) for l,h in limits ])), level=2)
        message("Starting parameters:  {0}".format(" | ".join([ str(p) for p in params ])), level=2)

        lnlike = lambda p: self._lnprob(p, xvalues, yvalues, xerr=xerrors, yerr=yerrors, limits=limits, callback=callback)

        # find max-likelihood value
        if likelihood:
            message("Computing max-likelihood.", level=1)
            chi2  = lambda p: -2 * lnlike(p)
            ml    = optimize.minimize(chi2, params)

            start = ml['x']
            message("Result: {0}".format(" | ".join([ str(p) for p in start ])), level=3)
        else:
            start = params

        # initialize sampler
        message("Initializing sampler.", level=1)
        dim     = self.ndim + self.kdim
        pos     = emcee.utils.sample_ball(start, radius * np.random.randn(dim), walkers)

        self.sampler = emcee.EnsembleSampler(walkers, dim, lnlike, **kwargs)

        # run MCMC
        if heatup > 0 and loadfile == None:
            # heat-up phase: sample a few values to rearrange the initial parameter distribution of the walkers
            message("Heating up MCMC ...", level=1)
            p0, prob, _ = self.sampler.run_mcmc(pos, heatup, rstate0=np.random.get_state())

            pos = [ p0[np.argmax(prob)] + np.power(radius, 4) * np.random.randn(dim) for i in xrange(walkers) ]
            self.sampler.reset()

        chain = None
        if loadfile != None:
            if os.path.exists(loadfile):
                message("Loading previous MCMC state from file...", level=1)
                with open(loadfile, 'r') as f:
                    state = json.load(f)
                    chain = np.array(state)
                    pos   = chain[:, -1, :].reshape((-1, dim))  # last step of saved chain is new start pos
            else:
                message("Could not find MCMC state file '{}'.".format(loadfile), level=1, severity=2)

        # run MCMC
        N = burnin + steps
        if chain != None:
            N -= chain.shape[1]  # take previous steps into account

        if N <= 0:
            message("Skipping MCMC (number of steps: {0}) ...".format(N), level=1)
        else:
            message("Running MCMC ({0} x {1}+{2}) ...".format(walkers, burnin, steps), level=1)
            nsteps = 0
            for result in self.sampler.sample(pos, iterations=N, storechain=True):
                nsteps += 1
                if verbose and (nsteps % 50) == 0:
                    message("{}% ({} steps left)".format(int(100*nsteps/N), N-nsteps), level=3)

            self.status = 0
            message("Done!", level=3)

        # strip burn-in
        if chain == None:
            chain = self.sampler.chain
        else:
            chain = np.concatenate((chain, self.sampler.chain), axis=1)
        samples = chain[:, burnin:, :].reshape((-1, dim))

        if N > 0:
            # only save if new step were computed
            if savefile != None:
                message("Saving current MCMC state to file...", level=1)

                with open(savefile, 'w') as f:
                    # shape = (walkers, iterations, dim)
                    state = chain.tolist()
                    json.dump(state, f, indent=4)

        message("Computing results.", level=1)
        result, errors = self.finish(samples)
        message("Result: {0}".format(" | ".join([ str(p) for p in result ])), level=2)

        if plot or traceplot or triplot:
            message("Generating fit result plots...", level=1)
            try:
                if plot:
                    self.plotProbability(result, errors)
                    self.plotChiSquare(result, errors)
                if traceplot:
                    self.plotTrace(chain, burnin, result=result, errors=errors, start=start, limits=limits)
                    self.plotAutocorr(chain, burnin)
                if triplot:
                    self.plotTriangle(samples, result=result)
                message("Done!", level=3)
            except Exception as e:
                exception(e, "Failed to plot fit results!")

    def plotLikelihood(self, result, errors, scale=2):
        dim = self.ndim
        params = self.fitfunc.getFitParams().values()

        message("Plotting likelihood ratio distributions", level=2)
        plt.figure()
        fig, axes = plt.subplots(dim, 1, sharex=False, figsize=(10, 10))
        for i in range(dim):
            N = 100
            pmin = result[i] - scale * errors[i][0]
            pmax = result[i] + scale * errors[i][1]
            prange = np.linspace(pmin, pmax, N)
            pcurve = np.zeros(N)
            theta = result[:]  # make copy
            for k in xrange(N):
                theta[i] = prange[k]
                loglike = self.getLikelihood(theta)
                pcurve[k] = loglike
            loglike = self.getLikelihood()  # best fit
            pcurve = -2 * (pcurve - loglike)  # log-likelihood ratio
            axes[i].plot(prange, pcurve, color='b', lw=2)
            axes[i].set_xlim(pmin, pmax)
            axes[i].set_ylim(pcurve.min(), pcurve.max())
            axes[i].axhline(stats.chi2.ppf(0.68, dim), color="r", lw=1)
            axes[i].axhline(stats.chi2.ppf(0.95, dim), color="r", lw=1, ls="--")
            axes[i].axhline(stats.chi2.ppf(0.99, dim), color="r", lw=1, ls=":-")
            axes[i].axvline(result[i], color='k', ls='--', lw=1)
            axes[i].axvline(result[i] - errors[i][0], color='k', ls=':', lw=1)
            axes[i].axvline(result[i] + errors[i][1], color='k', ls=':', lw=1)
            axes[i].xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i].yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i].xaxis.set_major_locator(MaxNLocator(6))
            axes[i].yaxis.set_major_locator(MaxNLocator(4))
            axes[i].set_ylabel(params[i].getName())
        fig.tight_layout(h_pad=0.0)
        self.savePlot(fig, "fit-mcmc-likelihood")

    def plotSamples(self, samples, result=None, start=None, size=100):
        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)
        yerrors = self.graph.getYerrorsAsym(filtered=True)
        fitrange = np.linspace(xvalues.min(), xvalues.max(), 100)

        message("Plotting {0} samples on top of result".format(size), level=2)
        plt.figure()
        fig, axes = plt.subplots(1, 1, sharex=False, figsize=(10, 10))
        for theta in samples[np.random.randint(len(samples), size=size)]:   # select random samples to plot
            axes.plot(fitrange, self.fitfunc.eval(fitrange, theta[:self.ndim]), color="k", alpha=10./size)
        if start != None:
            axes.plot(fitrange, self.fitfunc.eval(fitrange, start[:self.ndim]), color="b", lw=1)
        if result != None:
            axes.plot(fitrange, self.fitfunc.eval(fitrange, result[:self.ndim]), color="r", lw=2)
        axes.errorbar(xvalues, yvalues, yerr=yerrors, fmt=".k")
        axes.xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
        axes.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
        try:
            axes.set_xlabel(self.graph.plot.xlabel)
            axes.set_ylabel(self.graph.plot.ylabel)
        except:
            axes.set_xlabel("x")
            axes.set_ylabel("y")
        fig.tight_layout()
        self.savePlot(fig, "fit-mcmc-samples")

    def plotAutocorr(self, chain, burnin, maxtau=200):
        dim = self.ndim + self.kdim
        maxtau = min(maxtau, chain.shape[1])  # chain may be shorter than maxtau values

        tau = np.arange(0, maxtau, 1)
        acor = [ emcee.autocorr.function(np.mean(chain[:, burnin:, i], axis=0)) for i in range(dim) ]
        params = self.fitfunc.getFitParams().values() + self.mcmcParams.values()

        message("Plotting autocorrelation", level=2)
        plt.figure()
        fig, axes = plt.subplots(dim, 1, sharex=True, figsize=(10, 10))
        if dim == 1:
            axes = [axes]
        for i in range(dim):
            axes[i].stem(tau, acor[i][:maxtau], linefmt="-k", markerfmt=" ", basefmt="-k")
            axes[i].xaxis.set_major_locator(MaxNLocator(8))
            axes[i].yaxis.set_major_locator(MaxNLocator(4))
            axes[i].set_ylabel(params[i].getName())
        axes[dim-1].set_xlabel("tau")
        fig.tight_layout(h_pad=0.0)
        self.savePlot(fig, "fit-mcmc-autocorr")

    def plotTrace(self, chain, burnin, result=None, errors=None, start=None, limits=None):
        dim = self.ndim + self.kdim
        params = self.fitfunc.getFitParams().values() + self.mcmcParams.values()

        message("Plotting sample trace", level=2)
        plt.figure()
        fig = plt.figure(figsize=(10, 10))
        grid = gs.GridSpec(dim, 2, width_ratios=[4, 1])
        axes = np.array([ plt.subplot(g) for g in grid ]).reshape((dim, -1))
        if dim == 1:
            axes = [axes]
        for i in range(dim):
            data = chain[:, burnin:, i]
            axes[i][0].plot(chain[:, :, i].T, color="k", alpha=0.2)
            axes[i][1].hist(data.ravel(), color=".5", bins=25, normed=True, orientation='horizontal')
            axes[i][0].axvspan(0, burnin, facecolor="y", edgecolor="none", alpha=0.2)
            if result != None:
                axes[i][0].axhline(result[i], color="r", lw=2)
                axes[i][1].axhline(result[i], color="r", lw=2)
            if errors != None:
                axes[i][0].axhspan(result[i]-errors[i][0], result[i]+errors[i][1], color="r", alpha=0.2)
                axes[i][1].axhspan(result[i]-errors[i][0], result[i]+errors[i][1], color="r", alpha=0.2)
            if start != None:
                axes[i][0].axhline(start[i], color="b", lw=1)
                axes[i][1].axhline(start[i], color="b", lw=1)
            if limits != None:
                if limits[i][0] >= data.min():
                    axes[i][0].axhline(limits[i][0], color="b", lw=1, ls='--')
                    axes[i][1].axhline(limits[i][0], color="b", lw=1, ls='--')
                if limits[i][1] <= data.max():
                    axes[i][0].axhline(limits[i][1], color="b", lw=1, ls='--')
                    axes[i][1].axhline(limits[i][1], color="b", lw=1, ls='--')
            axes[i][0].xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i][0].yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i][0].xaxis.set_major_locator(MaxNLocator(8))
            axes[i][1].xaxis.set_major_locator(MaxNLocator(4))
            axes[i][0].yaxis.set_major_locator(MaxNLocator(4))
            axes[i][1].yaxis.set_major_locator(MaxNLocator(4))
            if i < dim-1:
                axes[i][0].axes.xaxis.set_ticklabels([])
            axes[i][1].axes.xaxis.set_ticklabels([])
            axes[i][1].axes.yaxis.set_ticklabels([])
            axes[i][1].set_ylim(axes[i][0].get_ylim())
            axes[i][0].set_ylabel(params[i].getName())
            axes[i][0].set_xlim(0, chain.shape[1])
        axes[dim-1][0].set_xlabel("step number")
        #axes[dim-1][1].set_xlabel("steps")
        fig.tight_layout(h_pad=0, w_pad=0)
        self.savePlot(fig, "fit-mcmc-trace")

    def plotTriangle(self, samples, result=None):
        dim = self.ndim + self.kdim
        params = self.fitfunc.getFitParams().values() + self.mcmcParams.values()

        message("Creating triangle plot", level=2)
        plt.figure()
        fig = corner.corner(samples, truth_color='b',
                    labels=map(lambda p: p.getName(), params),
                    truths=result, quantiles=[0.025,0.16,0.5,0.84,0.975])  # 1-/2-sigma
        self.savePlot(fig, "fit-mcmc-triangle")

########################################################################

class NuisanceFit(Fit):

    def __init__(self, fitfunc, graph=None):
        super(NuisanceFit, self).__init__(fitfunc, graph)

        self.mcmcParams = OrderedDict()
        self.mcmcParams["ln(f)"] = Base.FitParam("ln(f)", value=0, limits=(-10,10))

        # make sure that MCMC parameters are initialized correctly
        self.ndim = len(self.fitfunc)
        self.kdim = len(self.mcmcParams)

    @staticmethod
    def _lnlike(theta, self, x, y, yerr):
        p, k = theta[:self.ndim], theta[-self.kdim:]
        nuisance = np.exp(2. * k[0])  # parameter is logarithmic

        model = self.fitfunc.fit(self, x, *p)
        delta = y - model

        # symmetric errors, adjusted by nuisance parameter
        sigma_sqr = np.square(yerr) + np.square(model) * nuisance
        delta_sqr = np.square(delta)
        return -0.5 * np.sum( delta_sqr / sigma_sqr + np.log(sigma_sqr) )

        ## asymmetric errors, adjusted by nuisance parameter
        ## [1] http://arxiv.org/pdf/physics/0406120v1.pdf
        ## [2] http://arxiv.org/pdf/physics/0403046v1.pdf
        #sigma = (2. * yerr[1] * yerr[0]) / (yerr[1] + yerr[0])
        #dsigma = (yerr[1] - yerr[0]) / (yerr[1] + yerr[0])
        #sigma_sqr = np.square(sigma + dsigma * delta) + nuisance
        #delta_sqr = np.square(delta)
        #return -0.5 * np.sum( delta_sqr / sigma_sqr + np.log(sigma_sqr) )

########################################################################
