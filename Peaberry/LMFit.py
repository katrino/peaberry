#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator, ScalarFormatter

import scipy.misc as sm
import lmfit

import os
from decimal import Decimal

from Peaberry.Main import message, exception
import Peaberry.Base as Base

########################################################################
########################################################################

class Fit(Base.Fit):
    """
    A class for fitting data points that implements various methods
    using chi-square minimization module.

    This basic implementation use the ``minimize`` method from the
    ``LMFit`` package, which will apply a chi-square minimization fit
    to a number of data points defined in a graph or histogram object.
    The graph or histogram has to be associated with the fit object.

    The initializer sets the number of free fit parameters (based
    on the list of parameter names) and sets their start values to
    zero, their limits to ``None`` (i.e. no limits).

    Args:
        graph (instance): A graph/histogram to which the fit associates.
        parnames (list): A list of parameter names that also defines
            the number of free parameters for the fit function.

    Raises:
        TypeError: If ``graph`` is not an instance of ``Peaberry.Base.Graph``.

    See also:
        :py:class:`Peaberry.Base.Graph` is the base class for all graphs
        to which a fit can be applied.
    """
    def __init__(self, fitfunc, graph=None):
        super(Fit, self).__init__(fitfunc, graph)

    def _residuals(self, p, x, y, xerr=None, yerr=None):

        model = self.fitfunc.fit(x, *p)

        delta = y - model

        if yerr == None:
            sigma = model
        else:
            if xerr == None:
                sigma = yerr
            else:
                d = self._derivative(x, model)
                sigma = yerr + xerr * np.abs(d)

        return (delta, sigma)

        ## asymmetric errors
        ## [1] http://arxiv.org/pdf/physics/0406120v1.pdf
        ## [2] http://arxiv.org/pdf/physics/0403046v1.pdf
        #sigma = (2. * yerr[1] * yerr[0]) / (yerr[1] + yerr[0])
        #dsigma = (yerr[1] - yerr[0]) / (yerr[1] + yerr[0])
        #sigma_sqr = np.square(sigma + dsigma * delta)
        #delta_sqr = np.square(delta)
        #return -0.5 * np.sum( delta_sqr / sigma_sqr + np.log(sigma_sqr) )

    def _chisquare(self, p, x, y, xerr=None, yerr=None):

        try:
            delta, sigma = self._residuals(p, x, y, xerr, yerr)
        except AssertionError:
            return np.full(x.size, np.inf)

        return np.square(delta / sigma)

    def printResults(self):
        chi2, ndf, prob = self.getChiSquare()

        topline = u"order=%d ndf=%d chi2=%.3f chi2/ndf=%.3f p=%.6g" % \
                    (len(self), ndf, chi2, chi2/ndf if ndf > 0 else 0., prob)
        linewidth = max(len(topline), 72)

        message(topline, level=3, bold=True)

        # parameter values
        message("=" * linewidth, level=3)
        message(u"%-12s    %12s %12s %12s %12s" % ("", "value", "error-", "error+", "error"), level=3, bold=True)
        for p in self.fitfunc.getFitParams().values():
            message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # extra values (for specialized fit functions)
        extra = self.fitfunc.getExtraResults()
        if extra:
            message("-" * linewidth, level=3)
            for p in extra.values():
                message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # parameter correlations
        message("-" * linewidth, level=3)
        message(u"%12s   %-12s  %12s %12s %12s" % ("", "", "corr-", "corr+", "covar"), level=3, bold=True)
        params = self.fitfunc.getFitParams().values()
        while len(params) > 0:
            p = params[0]
            for q in params:
                message(u"%12s : %-12s  %+12.6g %+12.6g %12.6g" % \
                    (p.getName(), q.getName(), p.getCorrelationDown(q), p.getCorrelationUp(q), p.getCovariance(q) ), level=3)
            del params[0]

        message("-" * linewidth, level=3)

    def prepare(self):
        """
        Prepare the fit by setting all parameters and constraints.

        Returns:
            list: Starting parameters as defined in LMFit,
                    see ``lmfit.Parameter``.
        """
        self.ndim = len(self.fitfunc)

        params = lmfit.Parameters()
        for p in self.fitfunc.getFitParams().values():
            params.add(p.getName(), value=p.getValue(), min=p.getLimitLow(), max=p.getLimitHigh(), vary=(not p.isFixed()))

        return params

    def finish(self, fitresult, conf=False, verbose=False):
        """
        Finish the fit by computing the uncertainties and saving the result.

        Args:
            fitresult: The result from ``fitter.minimize``.
            conf (bool): If ``True``, use F-tests to compute uncertainties.
            verbose (bool): If ``True``, print results from LMFit.

        Returns:
            tuple: Resulting fit parameters and errors,
                like ``( [p0, p1, ...], [ (e1_lo,e1_hi), (e2_lo,p2_hi), ... ] )``.
        """
        values = fitresult.params.values()
        result = [ p.value for p in values ]
        errors = [ ( (0,0) if (p.stderr == None) else (p.stderr, p.stderr) ) for p in values ]  # symmetric errors

        if conf:
            pnames = [ str(p.name) for p in values ]  # conf_interval doesn't work with unicode parameter names...
            ci = lmfit.conf_interval(self.fitter, fitresult, p_names=pnames, sigmas=[0.68])  # 1-sigma
            if verbose:
                lmfit.printfuncs.report_ci(ci)

            conf   = [ ci[p.name] for p in values ]
            errors = [ (c[1][1]-c[0][1], c[2][1]-c[1][1]) for c in conf ]

        params = self.fitfunc.getFitParams().values()
        for i in xrange(self.ndim):
            params[i].setValue(result[i])
            params[i].setErrors((errors[i][0], errors[i][1]))

            for j in xrange(self.ndim):
                if params[i].isFixed() or params[j].isFixed():
                    params[i].setCovariance(params[j], 0.)
                    continue
                try:
                    cov = fitresult.covar[i][j]
                except:
                    cov = 0.
                params[i].setCovariance(params[j], cov)

        return ( result, errors )

    def run(self, plot=False, symerr=True, verbose=False,
            method='leastsq', leastsq=True, conf=False, scale_covar=False, **kwargs):
        """
        Run the fit method using the defined fit function and parameters.

        This implementation uses the minimization methods from
        ``lmfit.minimize``. Extra parameters are passed on to this function.
        The fit is initialized with the currently defined set of parameters.
        After running the fit, the parameters and their errors are updated
        with the fit results, and returned via ``getFitParams()``.

        Args:
            plot (bool): If `True`, produce a set of fit-performance
                plots (chi-square, probability).
            symerr (bool): If `True`, always use symmetric errors
                in the chi-square minimization even if asymmetric
                errors are present in the data set.
            verbose (bool): If `True`, print results from LMFit.
            method (string): The fit method to use,
                e.g. ``nelder`` or ``leastsq`` (see ``lmfit.minimize``).
            leastsq (bool): If ``True`` and ``method`` is other than
                ``leastsq``, perform a Least-Squares minimization
                after the initial fit to compute uncertainties
                (see ``lmfit.minimize``).
            conf (bool): If ``True``, apply advanced estimation of
                confidence intervals using F-tests for each parameter
                (see ``lmfit.confidence``).
            scale_covar (bool): If `True`, uncertainties are scaled
                by the chi-square value which effectively makes the
                resulting fit error independent from the y-errors
                of the data set (off by default).
            **kwargs (dict): Additional arguments passed to
                ``lmfit.minimize``.
        """
        if len(self) == 0:
            return

        if len(self.graph) < len(self):
            raise RuntimeError("{0}: number of data points {1} is too small for fit of order {2}".format(self.graph._className(), len(self.graph), len(self)))

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            if symerr:
                yerrors = self.graph.getYerrors(filtered=True)
            else:
                raise RuntimeError("Asymmetric errors are not supported (yet)!")
                #yerrors = self.graph.getYerrorsAsym(filtered=True)

        xerrors = None
        if self.useXerrors and self.graph.hasErrorsX():
            if symerr:
                xerrors = self.graph.getXerrors(filtered=True)
                #yerrors += self.graph.getTransformedXerrors(filtered=True)
            else:
                raise RuntimeError("Asymmetric errors are not supported (yet)!")
                #xerrors = self.graph.getXerrorsAsym(filtered=True)
                #yerrors += self.graph.getTransformedXerrorsAsym(filtered=True)

        message("Beginning the LMFit {} fit.".format(method.title()), level=1)

        params = self.prepare()
        message("Parameter constraints: {0}".format(" | ".join([ "{} .. {}".format(p.min,p.max) for p in params.values() ])), level=2)
        message("Starting parameters:  {0}".format(" | ".join([ "{}{}".format(p.value, '' if p.vary else '*') for p in params.values() ])), level=2)

        chi2 = lambda p: self._chisquare(map(lambda x: x.value, p.values()), xvalues, yvalues, xerr=xerrors, yerr=yerrors)

        self.fitter = lmfit.Minimizer(chi2, params, scale_covar=scale_covar, **kwargs)

        message("Running minimizer ...", level=1)
        #fitresult = lmfit.minimize(chi2, params, method=method,
        #        scale_covar=scale_covar, **kwargs)
        fitresult = self.fitter.minimize(method=method)

        if leastsq and (method != 'leastsq'):
            message("Result: {0}".format(" | ".join([ str(p.value) for p in fitresult.params.values() ])), level=3)

            # Run additional least-squares fit to compute parameter errors and covariances
            message("Running least-squares ...".format(method), level=2)
            #fitresult = lmfit.minimize(chi2, fitresult.params,
            #        scale_covar=scale_covar)
            fitresult = self.fitter.minimize(method='leastsq')

        if verbose:
            print(lmfit.fit_report(fitresult))

        if not fitresult.success:
            self.status = 1
            return

        self.status = 0
        message("Done!", level=3)

        message("Computing results.", level=1)
        if conf:
            message("Using F-tests to compute asymmetric errors.", level=3)
        result, errors = self.finish(fitresult, conf=conf, verbose=verbose)
        message("Result: {0}".format(" | ".join([ str(p) for p in result ])), level=2)

        if plot:
            message("Generating fit result plots...", level=1)
            try:
                self.plotProbability(result, errors)
                self.plotChiSquare(result, errors)
                message("Done!", level=3)
            except Exception as e:
                exception(e, "Failed to plot fit results!")

########################################################################
