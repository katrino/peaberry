#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
import json

from Peaberry.Main import message

########################################################################
########################################################################

def SaveGraphData(graph, filename=None, fileformat=None):
    """
    Save graph data to a file.

    The file format is chosen automatically by the file ending,
    or can be overridden with the user.

    Note:
        If no filename is given, the graph's name will be used and
        appended with a '.json' ending.

    Args:
        filename (string): The filename of the output file.
        fileformat (string): A file format supported by `

    See also:
        `py:func:Base.Graph.export` is the base function used here.
    """
    message("SaveGraphData(filename={2}, fileformat={3}) @ {0} [{1}]".format(graph.name, graph.label, filename, fileformat),
                severity=1)

    if graph.isEmpty():
        return

    if filename == None:
        filename = "{}.json".format(graph.name)

    graph.save(filename, fileformat=fileformat)

########################################################################

def LoadGraphData(graph, filename=None):
    """
    Load graph data from a JSON file.

    Note:
        If no filename is given, the graph's name will be used and
        appended with a '.json' ending.

    Args:
        filename (string): The filename of the output file.

    See also:
        `py:func:Base.Graph.import` is the base function used here.
    """
    message("LoadGraphData(filename={2}) @ {0} [{1}]".format(graph.name, graph.label, filename),
                severity=1)

    if filename == None:
        filename = "{}.json".format(graph.name)

    graph.load(filename)

########################################################################

def CopyFitParams(graph, source=None):
    """
    Copy over fit parameters from another fit.

    Because callback functions are called in the ordering in which they
    were added, it is possible to execute another fit (in another
    graph) by its ApplyFit callback, and then use the CopyFitParams
    callback to re-use the results.

    The function should be added to the callback list before ApplyFit.

    Note:
        All the callback functions change the data in-place.
    """
    message("CopyFitParams @ {0} [{1}]".format(graph.name, graph.label),
                severity=1)

    if source == None:
        return

    if graph.isEmpty():
        return

    if not graph.hasFit():
        return
    fit = graph.fit

    if not graph.hasErrorsY():
        message("Can't apply a fit method without having y-errors defined.", severity=3)
        return

    for key in fit.fitfunc.fitParams.keys():
        this  = fit.fitfunc.fitParams[key]
        other = source.fitfunc.fitParams[key]

        this.value  = other.getValue()
        this.errors = other.getErrors()
        this.limits = other.getLimits()

########################################################################

def GuessFitParams(graph, values=True, limits=True):
    """
    Guess the start parameters for a fit method.

    A fit must have been added to the corresponding graph before.
    This function calls the guess() method of the used fit class,
    which if defined should estimate an initial set of fit parameters
    that help the fit method to converge more quickly.

    The function should be added to the callback list before ApplyFit.

    Note:
        All the callback functions change the data in-place.
    """
    message("GuessFitParams(value={2}, limits={3}) @ {0} [{1}]".format(graph.name, graph.label, values, limits),
                severity=1)

    if graph.isEmpty():
        return

    if not graph.hasFit():
        return
    fit = graph.fit

    fit.fitfunc.guess(graph)

    if values == False:
        fit.fitfunc.resetFitValues()
    if limits == False:
        fit.fitfunc.resetFitLimits()

########################################################################

def SetFitParams(graph, values={}, limits={}, fixed=[]):
    """
    Set the start parameters for a fit method.

    This callback can be used in combination with ``ApplyFit`` to run
    the fit method multiple times with different parameter settings.

    A fit must have been added to the corresponding graph before.
    The exact fit routine is defined by the used fit class, this
    function merely initiates the fit process and shows the result.

    Note:
        All the callback functions change the data in-place.

    Args:
        values (dict): The new fit parameters that override existing ones.
        limits (dict): The new fit limits that override existing ones.
        fixed (list): A list of parameters to keep fixed in the fit.
    """
    message("SetFitParams(values={2},limits={3},fixed={4}) @ {0} [{1}]".format(graph.name, graph.label,
                ",".join([ "{0}={1}".format(k,v) for k,v in values.items() ]),
                ",".join([ "{0}={1}".format(k,v) for k,v in limits.items() ]),
                ",".join([ "{0}".format(k) for k in fixed ])),
                severity=1)

    if graph.isEmpty():
        return

    if not graph.hasFit():
        return
    fit = graph.fit

    fit.fitfunc.setFitValues(values)
    fit.fitfunc.setFitLimits(limits)
    fit.fitfunc.setFixedParams(fixed)

########################################################################

def ApplyFit(graph, show=True, **kwargs):
    """
    Apply a fit method to a data set.

    A fit must have been added to the corresponding graph before.
    The exact fit routine is defined by the used fit class, this
    function merely initiates the fit process and shows the result.

    Note:
        All the callback functions change the data in-place.

    Args:
        show (bool): If ``True``, print the fit results to the terminal.
        \*\*kwargs: Passed on to the run() function of the fit method.
    """
    message("ApplyFit(show={2},{3}) @ {0} [{1}]".format(graph.name, graph.label,
                show, ",".join([ "{0}={1}".format(k,v) for k,v in kwargs.items() ])),
                severity=1)

    if graph.isEmpty():
        return

    if not graph.hasFit():
        return
    fit = graph.fit

    fit.run(**kwargs)

    if show:
        fit.printResults()

########################################################################

def SaveFitResults(graph, filename=None):
    """
    Save fit results to a JSON file.

    If no filename is given, the graph's name will be used and appended
    with a '.fit.json' ending.
    """
    message("SaveFitResults(filename={2}) @ {0} [{1}]".format(graph.name, graph.label, filename),
                severity=1)

    if graph.isEmpty():
        return

    if not graph.hasFit():
        return
    fit = graph.fit

    if filename == None:
        filename = "{}.fit.json".format(graph.name)

    fit.save(filename)

########################################################################

def LoadFitResults(graph, filename=None, exclude=[]):
    """
    Load fit results from a JSON file.

    If no filename is given, the graph's name will be used. The file
    endinf '.fit.json' will be appended automatically in either case.
    """
    message("LoadFitResults(filename={2}, exclude={3}) @ {0} [{1}]".format(graph.name, graph.label, filename, exclude),
                severity=1)

    if not graph.hasFit():
        return
    fit = graph.fit

    params = fit.fitfunc.getParNames()

    if filename == None:
        filename = "{}.fit.json".format(graph.name)

    fit.load(filename, exclude)
    fit.printResults()

########################################################################

def ScaleErrors(graph, factor=1, minError=0):
    """
    Scale the error bars of a data set by the given factor.

    Note:
        All the callback functions change the data in-place.

    Args:
        factor (float): The scale factor.
        minError (float): The minimal error to keep.
    """

    message("ScaleErrors(factor={2},minError={3}) @ {0} [{1}]".format(graph.name, graph.label, factor, minError),
                severity=1)

    if graph.isEmpty():
        return

    if graph.hasErrorsY():
        yerrorsLo, yerrorsHi = graph.getYerrorsAsym()

    if graph.hasErrorsY():
        graph.yerrorsLo = np.fmax(factor * yerrorsLo, minError)
        graph.yerrorsHi = np.fmax(factor * yerrorsHi, minError)

########################################################################

def Duplicate(graph, repeats=1, spacing=0.):
    """
    Duplicate a graph by a given number of times.

    Each data point is added at the right end of the data set, shifted
    by the x-range of the original data, so that in the end there
    will be a full copy of the original data at the end of the data set.
    The whole process is repeated for the given number of times.

    Note:
        All the callback functions change the data in-place.

    Args:
        repeats (int): How often the data set should be repeated.
        spacing (float): Additional spacing between the repeated segments.
    """
    message("Duplicate(repeats={2},spacing={3}) @ {0} [{1}]".format(graph.name, graph.label, repeats, spacing),
                severity=1)

    if graph.isEmpty():
        return

    xvalues = graph.getXvalues()
    yvalues = graph.getYvalues()
    if graph.hasErrorsX():
        xerrorsLo, xerrorsHi = graph.getXerrorsAsym()
    if graph.hasErrorsY():
        yerrorsLo, yerrorsHi = graph.getYerrorsAsym()

    width = xvalues.max() - xvalues.min()
    for i in xrange(repeats):
        graph.xvalues = np.append(graph.xvalues, xvalues + (i+1) * (width + spacing))
        graph.yvalues = np.append(graph.yvalues, yvalues)
        if graph.hasErrorsX():
            graph.xerrorsLo = np.append(graph.xerrorsLo, xerrorsLo)
            graph.xerrorsHi = np.append(graph.xerrorsHi, xerrorsHi)
        if graph.hasErrorsY():
            graph.yerrorsLo = np.append(graph.yerrorsLo, yerrorsLo)
            graph.yerrorsHi = np.append(graph.yerrorsHi, yerrorsHi)

########################################################################

def MovingAverage(graph, alpha=0.1):
    """
    Apply an exponential moving average to a data set.

    The data points are replaced by the result of the moving average,
    starting with the second data point (since two points are needed
    to compute the result).

    .. math::
        Y'_i =  \\begin{cases}
                    Y_1                                     & \\text{for} \\quad i = 1  \\\\
                    \\alpha Y_{i-1} + (1 - \\alpha) Y_i     & \\text{for} \\quad i > 1
                \\end{cases}

    Note:
        All the callback functions change the data in-place.

    Args:
        alpha (float): The moving average weight coefficient.
    """
    message("MovingAverage(alpha={2}) @ {0} [{1}]".format(graph.name, graph.label, alpha),
                severity=1)

    if graph.isEmpty():
        return

    yvalues = graph.getYvalues()
    if graph.hasErrorsX():
        xerrorsLo, xerrorsHi = graph.getXerrorsAsym()
    if graph.hasErrorsY():
        yerrorsLo, yerrorsHi = graph.getYerrorsAsym()

    for i in xrange(1, len(graph)):  # skip first point

        if yvalues[i] == None or yvalues[i-1] == None:
            yvalues[i] = None
            continue

        graph.yvalues[i] = alpha * yvalues[i] + (1 - alpha) * yvalues[i-1]

        if graph.hasErrorsY():
            graph.yerrorsLo[i] = np.fabs(alpha * yerrorsLo[i] + (1 - alpha) * yerrorsLo[i-1])
            graph.yerrorsHi[i] = np.fabs(alpha * yerrorsHi[i] + (1 - alpha) * yerrorsHi[i-1])

########################################################################

def Differentiate(graph, reverse=False):
    """
    Apply numerical differentiation to a data set.

    The data points are replaced by the result of the differentiation,
    starting with the second data point (since two points are needed
    to compute the result). The direction can be reversed.

    .. math::
        Y'_i =  \\frac{ Y_i - Y_{i-1} }{ X_i - X_{i-1} }

    Note:
        All the callback functions change the data in-place.

    Args:
        reverse (bool): Differentiate data point right-to-left,
                i.e. start with the last data point instead of the first.
    """
    message("Differentiate(reverse={2}) @ {0} [{1}]".format(graph.name, graph.label, reverse),
                severity=1)

    if graph.isEmpty():
        return

    xvalues = graph.getXvalues()
    yvalues = graph.getYvalues()
    if graph.hasErrorsX():
        xerrorsLo, xerrorsHi = graph.getXerrorsAsym()
    if graph.hasErrorsY():
        yerrorsLo, yerrorsHi = graph.getYerrorsAsym()

    if reverse == False:
        for i in xrange(1, len(graph)):  # skip first point
            dx = xvalues[i] - xvalues[i-1]
            dy = yvalues[i] - yvalues[i-1]
            graph.yvalues[i] = dy / dx

            if graph.hasErrorsY():
                yerrLo = (yerrorsLo[i] + yerrorsLo[i-1]) / yvalues[i]
                yerrHi = (yerrorsHi[i] + yerrorsHi[i-1]) / yvalues[i]
                graph.yerrorsLo[i] = np.fabs(graph.yvalues[i] * yerrLo)
                graph.yerrorsHi[i] = np.fabs(graph.yvalues[i] * yerrHi)
    else:
        for i in xrange(len(graph)-1, 0, -1):  # skip last point
            dx = xvalues[i] - xvalues[i+1]
            dy = yvalues[i] - yvalues[i+1]
            graph.yvalues[i] = dy / dx

            if graph.hasErrorsY():
                yerrLo = (yerrorsLo[i] + yerrorsLo[i+1]) / yvalues[i]
                yerrHi = (yerrorsHi[i] + yerrorsHi[i+1]) / yvalues[i]
                graph.yerrorsLo[i] = np.fabs(graph.yvalues[i] * yerrLo)
                graph.yerrorsHi[i] = np.fabs(graph.yvalues[i] * yerrHi)

########################################################################

def FilterPoints(graph, threshold=4):
    """
    Remove outliers from a data set.

    A fit must have been added to the corresponding graph before.
    The residuals of the fit are computed including their mean and
    standard deviation (sigma). The distance of the residual of each
    data point to the residual mean is then computed, and scaled by the
    residual sigma. If this value is larger than the given threshold,
    the data point is removed.

    The function should be added as callback after ApplyFit to have the
    fit results (and residuals) available. Another instance of ApplyFit
    can be added afterwards to re-fit the data after filtering.

    .. todo::
        It would be better to not remove data points, but just exclude
        them from the fit.

    Note:
        All the callback functions change the data in-place.

    Args:
        threshold (float): The moving average weight coefficient.
    """
    message("FilterPoints(thresh={2}) @ {0} [{1}]".format(graph.name, graph.label, threshold),
                severity=1)

    if graph.isEmpty():
        return

    if not graph.hasFit():
        return
    fit = graph.fit

    xvalues = graph.getXvalues()
    yvalues = graph.getYvalues()
    if graph.hasErrorsX():
        xerrorsLo, xerrorsHi = graph.getXerrorsAsym()
    if graph.hasErrorsY():
        yerrorsLo, yerrorsHi = graph.getYerrorsAsym()

    mu,sigma = fit.getResiduals()
    rvalues = fit.getRvalues()
    message("current residual: mu=%.4g sigma=%.4g (threshold=%g sigmas)" % (mu,sigma,threshold), level=1)

    graph.clear()  # remove all data points

    for i in xrange(len(graph)):
        dist = abs(rvalues[i] - mu) / sigma
        if dist >= threshold:
            message("outlier at x=%.4g y=%.4g (dist=%.4g sigmas)" % (xvalues[i], yvalues[i], dist), level=2)
            continue

        graph.xvalues = np.append(graph.xvalues, xvalues[i])
        graph.yvalues = np.append(graph.yvalues, yvalues[i])
        if graph.hasErrorsX():
            graph.xerrorsLo = np.append(graph.xerrorsLo, xerrorsLo[i])
            graph.xerrorsHi = np.append(graph.xerrorsHi, xerrorsHi[i])
        if graph.hasErrorsY():
            graph.yerrorsLo = np.append(graph.yerrorsLo, yerrorsLo[i])
            graph.yerrorsHi = np.append(graph.yerrorsHi, yerrorsHi[i])

    mu,sigma = fit.getResiduals()
    message("residual after filtering: mu=%.4g sigma=%.4g" % (mu,sigma), level=1)

########################################################################
