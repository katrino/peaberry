#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator, ScalarFormatter
from scipy import stats

import ROOT

import os
from decimal import Decimal

from Peaberry.Main import message, exception
import Peaberry.Base as Base

########################################################################
########################################################################

class Fit(Base.Fit):
    """
    A class for fitting data points that implements various methods
    using chi-square minimization module.

    This implementation use the ``Minuit2Minimizer`` class from the
    ``ROOT`` package, which will apply a chi-square minimization fit
    to a number of data points defined in a graph or histogram object.
    The graph or histogram has to be associated with the fit object.

    The initializer sets the number of free fit parameters (based
    on the list of parameter names) and sets their start values to
    zero, their limits to ``None`` (i.e. no limits).

    Args:
        graph (instance): A graph/histogram to which the fit associates.
        parnames (list): A list of parameter names that also defines
            the number of free parameters for the fit function.

    Raises:
        TypeError: If ``graph`` is not an instance of ``Peaberry.Base.Graph``.

    See also:
        :py:class:`Peaberry.Base.Graph` is the base class for all graphs
        to which a fit can be applied.
    """
    def __init__(self, fitfunc, graph=None):
        super(Fit, self).__init__(fitfunc, graph)

    @staticmethod
    def _format_error(param, extend=0, texmode=False):
        value = Decimal(float(param.getValue()))
        errlo = Decimal(float(param.getErrorDown()))
        errhi = Decimal(float(param.getErrorUp()))

        vdigits = value.adjusted()
        edigits = min(errlo.adjusted(), errhi.adjusted())
        value   = Base.Fit._adjust_digits(value, edigits)
        errlo   = Base.Fit._adjust_digits(errlo, edigits)
        errhi   = Base.Fit._adjust_digits(errhi, edigits)

        if texmode:
            if errlo == errhi:
                return r"%s \pm %s" % (value, errlo)
            else:
                return r"%s_{-%s}^{+%s}" % (value, errlo, errhi)
        else:
            if errlo == errhi:
                return u"%s±%s" % (value, errlo)
            else:
                return u"%s-%s+%s" % (value, errlo, errhi)

    def _residuals(self, p, x, y, xerr=None, yerr=None):

        model = self.fitfunc.fit(x, *p)

        delta = y - model

        if yerr == None:
            sigma = model
        else:
            if xerr == None:
                sigma = yerr
            else:
                d = self._derivative(x, model)
                sigma = yerr + xerr * np.abs(d)

        return (delta, sigma)

        ## asymmetric errors
        ## [1] http://arxiv.org/pdf/physics/0406120v1.pdf
        ## [2] http://arxiv.org/pdf/physics/0403046v1.pdf
        #sigma = (2. * yerr[1] * yerr[0]) / (yerr[1] + yerr[0])
        #dsigma = (yerr[1] - yerr[0]) / (yerr[1] + yerr[0])
        #sigma_sqr = np.square(sigma + dsigma * delta)
        #delta_sqr = np.square(delta)
        #return -0.5 * np.sum( delta_sqr / sigma_sqr + np.log(sigma_sqr) )

    def _chisquare(self, p, x, y, xerr=None, yerr=None):

        try:
            delta, sigma = self._residuals(p, x, y, xerr, yerr)
        except AssertionError:
            return np.full(x.size, np.inf)

        return np.square(delta / sigma)

    class _PyFCN(ROOT.TPyMultiGenFunction):
        def __init__(self, ndim, fit, args):
            ROOT.TPyMultiGenFunction.__init__(self, self)

            self.ndim = ndim
            self.fit  = fit
            self.args = args

        def NDim(self):
            return self.ndim

        def DoEval(self, popt):
            p = [ popt[i] for i in range(self.ndim) ]
            xvalues, yvalues, xerrors, yerrors = self.args

            return self.fit._chisquare(p, xvalues, yvalues, xerr=xerrors, yerr=yerrors).sum()

    def printResults(self):
        chi2, ndf, prob = self.getChiSquare()

        topline = u"order=%d ndf=%d chi2=%.3f chi2/ndf=%.3f p=%.6g" % \
                    (len(self), ndf, chi2, chi2/ndf if ndf > 0 else 0., prob)
        linewidth = max(len(topline), 72)

        message(topline, level=3, bold=True)

        # parameter values
        message("=" * linewidth, level=3)
        message(u"%-12s    %12s %12s %12s %12s" % ("", "value", "error-", "error+", "error"), level=3, bold=True)
        for p in self.fitfunc.getFitParams().values():
            message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # extra values (for specialized fit functions)
        extra = self.fitfunc.getExtraResults()
        if extra:
            message("-" * linewidth, level=3)
            for p in extra.values():
                message(u"%-12s    %12.6g %12.6g %12.6g %12.6g" % (p.getName(), p.getValue(), p.getErrorDown(), p.getErrorUp(), p.getErrorMean()), level=3)

        # parameter correlations
        message("-" * linewidth, level=3)
        message(u"%12s   %-12s  %12s %12s %12s" % ("", "", "corr-", "corr+", "covar"), level=3, bold=True)
        params = self.fitfunc.getFitParams().values()
        while len(params) > 0:
            p = params[0]
            for q in params:
                message(u"%12s : %-12s  %+12.6g %+12.6g %12.6g" % \
                    (p.getName(), q.getName(), p.getCorrelationDown(q), p.getCorrelationUp(q), p.getCovariance(q) ), level=3)
            del params[0]

        message("-" * linewidth, level=3)

    def prepare(self, args, step=0.01):
        """
        Prepare the fit by setting all parameters and constraints.

        Args:
            step (float): The initial step size for all variables,
                    passed to Minuit2.

        Returns:
            tuple: Python FCN object as used by Minuit2,
                    starting parameters and parameter constraints,
                    like ``( chi2, [p0, p1, ...], [ (p1_lo,p1_hi), (p2_lo,p2_hi), ... ] )``.
        """
        self.ndim = len(self.fitfunc)

        chi2 = self._PyFCN(self.ndim, self, args)
        self.fitter.SetFunction(chi2)

        params = []
        limits = []

        index = 0
        for p in self.fitfunc.getFitParams().values():
            if p.isFixed():
                self.fitter.SetFixedVariable(index, str(p.getName()), p.getValue())
            else:
                if not p.hasLimits():  # no limit
                    self.fitter.SetVariable(index, str(p.getName()), p.getValue(), step)
                elif p.getLimitHigh() == np.inf:  # only lower limit
                    self.fitter.SetLowerLimitedVariable (index, str(p.getName()), p.getValue(), step, p.getLimitLow())
                elif p.getLimitLow() == -np.inf:  # only upper limit
                    self.fitter.SetUpperLimitedVariable (index, str(p.getName()), p.getValue(), step, p.getLimitHigh())
                else:  # both limits
                    self.fitter.SetLimitedVariable(index, str(p.getName()), p.getValue(), step, p.getLimitLow(), p.getLimitHigh())

            params.append( (p.getValue(), p.isFixed()) )
            limits.append( p.getLimits() )

            index += 1

        # Warning: chi2 must be returned to prevent garbage-collection
        return ( chi2, params, limits )

    def finish(self, minos=False):
        """
        Finish the fit by computing the uncertainties and saving the result.

        The uncertainties can optionally computed by MINOS, which determines
        asymmetric uncertainties (on by default). Otherwise, symmetric
        errors from the Minimizer result are used.

        Args:
            minos (bool): If ``True``, use MINOS to compute uncertainties.

        Returns:
            tuple: Resulting fit parameters and errors,
                like ``( [p0, p1, ...], [ (e1_lo,e1_hi), (e2_lo,p2_hi), ... ] )``.
        """
        result = [ self.fitter.X()[i] for i in xrange(self.ndim) ]

        errors = []
        for i in xrange(self.ndim):
            if minos:
                el, eh = ROOT.Double(), ROOT.Double()
                self.fitter.GetMinosError(i, el, eh)
                errors.append( (-el, eh) )  # asymmetric errors; lower error is a negative number here!
            else:
                err = self.fitter.Errors()[i]
                errors.append( (err, err) )  # symmetric errors

        params = self.fitfunc.getFitParams().values()
        for i in xrange(self.ndim):
            params[i].setValue(result[i])
            params[i].setErrors((errors[i][0], errors[i][1]))

            for j in xrange(self.ndim):
                cov = self.fitter.CovMatrix(i,j)
                params[i].setCovariance(params[j], cov)

        return ( result, errors )

    def run(self, plot=False, symerr=True, verbose=0,
             method="Minuit2", algo="migrad", minos=False, step=0.01, maxfcn=None, maxiter=None, tol=None, prec=None, **kwargs):
        """
        Run the fit method using the defined fit function and parameters.

        This implementation uses the minimization methods from
        ``ROOT.Minuit2``. Extra parameters are passed on to the
        ``ROOT.Math.Minimizer.Minimize`` function.
        The fit is initialized with the currently defined set of parameters.
        After running the fit, the parameters and their errors are updated
        with the fit results, and returned via ``getFitParams()``.

        Args:
            plot (bool): If `True`, produce a set of fit-performance
                plots (chi-square, probability).
            symerr (bool): If `True`, always use symmetric errors
                in the chi-square minimization even if asymmetric
                errors are present in the data set.
            verbose (int): The print level passed to the ROOT fitter.
            method (string): The fit method to use,
                e.g. ``minuit2`` or ``linear`` (see ``ROOT.Math.Factory``).
            algo (string): The fit algorithm to use,
                e.g. ``migrad`` or ``simplex`` (see ``ROOT.Math.Factory``).
            minos (bool): If `True`, asymmetric uncertainties are computed
                using ``ROOT.Minuit2.MnMinos``.
            step (float): Initial error guess for the fit parameters
                (step size), see ``ROOT.Math.Minimizer``.
            maxfcn (float): Maximum number of function calls in the fit,
                see ``ROOT.Math.Minimizer``.
            maxiter (float): Maximum number of iterations in the fit,
                see ``ROOT.Math.Minimizer``.
            tol (float): Absolute tolerance of the fit,
                see ``ROOT.Math.Minimizer``.
            prec (float): Evaluation precision of the fit function,
                see ``ROOT.Math.Minimizer``.
            **kwargs (dict): Additional arguments passed to
                ``ROOT.Minimizer.Minimize``.
        """
        if len(self) == 0:
            return

        if len(self.graph) < len(self):
            raise RuntimeError("{0}: number of data points {1} is too small for fit of order {2}".format(self.graph.className(), len(self.graph), len(self)))

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            if symerr:
                yerrors = self.graph.getYerrors(filtered=True)
            else:
                raise RuntimeError("Asymmetric errors are not supported (yet)!")
                #yerrors = self.graph.getYerrorsAsym(filtered=True)

        xerrors = None
        if self.useXerrors and self.graph.hasErrorsX():
            if symerr:
                xerrors = self.graph.getXerrors(filtered=True)
                #yerrors += self.graph.getTransformedXerrors(filtered=True)
            else:
                raise RuntimeError("Asymmetric errors are not supported (yet)!")
                #xerrors = self.graph.getXerrorsAsym(filtered=True)
                #yerrors += self.graph.getTransformedXerrorsAsym(filtered=True)

        message("Beginning the {} {} fit.".format(method, algo), level=1)

        self.fitter = ROOT.Math.Factory.CreateMinimizer(str(method), str(algo))
        self.fitter.SetPrintLevel(verbose)

        chi2, params, limits = self.prepare((xvalues, yvalues, xerrors, yerrors), step=step)
        message("Parameter constraints: {0}".format(" | ".join([ "{} .. {}".format(l,h) for l,h in limits ])), level=2)
        message("Starting parameters:  {0}".format(" | ".join([ "{}{}".format(p, '*' if f else '') for p,f in params ])), level=2)

        message("Running minimizer ...", level=1)

        if maxfcn != None:
            self.fitter.SetMaxFunctionCalls(maxfcn)
        if maxiter != None:
            self.fitter.SetMaxIterations(maxiter)
        if tol != None:
            self.fitter.SetTolerance(tol)
        if prec != None:
            self.fitter.SetPrecision(prec)

        self.fitter.Minimize(**kwargs)

        if verbose == True:
            self.fitter.PrintResults()

        self.status = self.fitter.Status()
        if self.status > 1:  # status=0 and status=1 is "valid minimum"
            return

        message("Done!", level=3)

        message("Computing results.", level=1)
        if minos:
            message("Using MINOS to compute asymmetric errors.", level=3)
        result, errors = self.finish(minos=minos)
        message("Result: {0}".format(" | ".join([ str(p) for p in result ])), level=2)

        if plot:
            message("Generating fit result plots...", level=1)
            try:
                self.plotProbability(result, errors)
                self.plotChiSquare(result, errors)
                message("Done!", level=3)
            except Exception as e:
                exception(e, "Failed to plot fit results!")

########################################################################
