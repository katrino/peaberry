#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function

import numpy as np
import ROOT

from Peaberry.Main import message
import Peaberry.Base as Base

########################################################################
########################################################################

class Fit(Base.FitFunction):
    """
    A special fit model that uses a ROOT TF1 internally.

    Note::
        This fit model uses the parameters defined in the TF1 object.

        Details can be found in the ROOT_ documentation.

    .. _ROOT:
        https://root.cern.ch/root/html/TF1.html
    """
    def __init__(self, rootfit):
        super(Fit, self).__init__([])  # empty parameter list will be filled later

        xmin, xmax = ROOT.Double(), ROOT.Double()
        lmin, lmax = ROOT.Double(), ROOT.Double()

        rootfit.GetRange(xmin, xmax)
        self.fitRange = (xmin, xmax)

        numparams = rootfit.GetNpar()
        for i in xrange(numparams):
            name  = rootfit.GetParName(i)
            value = rootfit.GetParameter(i)
            error = rootfit.GetParError(i)
            rootfit.GetParLimits(i, lmin, lmax)
            self.fitParams[name] = Base.FitParam(name, value=value, errors=(error,error), limits=(lmin,lmax))

        self.rootfit = rootfit

    @staticmethod
    def _fit(self, x, *params):
        return np.vectorize(self.rootfit.Eval)(x, *params)

########################################################################

"""
A simple class that represents a ROOT graph.

The class holds all data points and fit parameters, and can be used
to retrieve existing data from a ROOT file that contains a TGraph object.
"""
class Graph(Base.Graph):
    def __init__(self, name, label="", plot=None, fit=None, **kwargs):
        super(Graph, self).__init__(name, label, plot, fit, **kwargs)

    def _read_graph(self, rootgraph, xerrors=False, yerrors=False):
        numpoints = rootgraph.GetN()

        self.xvalues = np.array([ rootgraph.GetX()[i] for i in xrange(numpoints) ])
        self.yvalues = np.array([ rootgraph.GetY()[i] for i in xrange(numpoints) ])

        if xerrors:
            try:
                self.xerrorsLo = np.array([ rootgraph.GetErrorXlow(i) for i in xrange(numpoints) ])
                self.xerrorsHi = np.array([ rootgraph.GetErrorXhigh(i) for i in xrange(numpoints) ])
            except:
                self.xerrorsLo = self.xerrorsHi = np.array([ rootgraph.GetErrorX(i) for i in xrange(numpoints) ])

        if yerrors:
            try:
                self.yerrorsLo = np.array([ rootgraph.GetErrorYlow(i) for i in xrange(numpoints) ])
                self.yerrorsHi = np.array([ rootgraph.GetErrorYhigh(i) for i in xrange(numpoints) ])
            except:
                self.yerrorsLo = self.yerrorsHi = np.array([ rootgraph.GetErrorY(i) for i in xrange(numpoints) ])

    def _read_hist(self, roothist, xerrors=False, yerrors=False):
        binsx = roothist.GetNbinsX()

        self.xvalues = np.array([ roothist.GetXaxis().GetBinCenter(i) for i in xrange(binsx) ])
        self.yvalues = np.array([ roothist.GetBinContent(i) for i in xrange(binsx) ])

        if yerrors:
            self.yerrorsLo = self.yerrorsHi = np.array([ rootgraph.GetBinError(i) for i in xrange(binsx) ])

    def _read_fit(self, rootgraph, fitname):
        rootfit = rootgraph.GetListOfFunctions().FindObject(fitname);
        if not rootfit:
            message("Could not find TF1 '{0}' in file.".format(fitname), severity=2, level=1)
            return

        if self.fit:
            message("Replacing existing fit with TF1 from file.", severity=1, level=1)
        self.fit = Base.Fit(fitfunc=Fit(rootfit))

    def fromROOT(self, filename="", plotname="Graph", fitname="", xerrors=False, yerrors=True, histogram=False):
        if not filename:
            filename = self.name + ".root"

        rootfile = ROOT.TFile(filename)
        if not rootfile:
            message("Could not open TFile '{0}'.".format(filename), severity=3)
            return None

        rootgraph = rootfile.Get(plotname)
        if not rootgraph:
            message("Could not find TGraph '{0}' in file.".format(plotname), severity=3, level=1)
            return None

        self.filename = filename
        message("Reading graph '{1}' from file '{0}' ...".format(filename, plotname), severity=1)

        if histogram:
            self._read_hist(rootgraph, xerrors, yerrors)
        else:
            self._read_graph(rootgraph, xerrors, yerrors)

        if fitname:
            self._read_fit(rootgraph, fitname)

        rootfile.Close()
        return self

    def fromROOTCanvas(self, filename="", plotname="Graph", canvasname="Canvas_1", fitname="", xerrors=False, yerrors=True, histogram=False):
        if not filename:
            filename = self.name + ".root"

        rootfile = ROOT.TFile(filename)
        if not rootfile:
            message("Could not open TFile '{0}'.".format(filename), severity=3)
            return None

        rootcanvas = rootfile.Get(canvasname)
        if not rootcanvas:
            message("Could not find TCanvas '{0}' in file.".format(canvasname), severity=3, level=1)
            return None

        rootgraph = rootcanvas.GetPrimitive(plotname)
        if not rootgraph:
            message("Could not find TGraph '{0}' inside canvas.".format(plotname), severity=3, level=1)
            return None

        self.filename = filename
        message("Reading graph '{1}' from file '{0}' ...".format(filename, plotname), severity=1)

        if histogram:
            self._read_hist(rootgraph, xerrors, yerrors)
        else:
            self._read_graph(rootgraph, xerrors, yerrors)

        if fitname:
            self._read_fit(rootgraph, fitname)

        rootfile.Close()
        return self

########################################################################

"""
A simple class that represents a ROOT 1-d histogram.

The class holds all data points and fit parameters, and can be used
to retrieve existing data from a ROOT file that contains a TH1x object.
"""
class Hist(Base.Hist, Graph):
    def __init__(self, name, label="", plot=None, fit=None, **kwargs):
        super(Hist, self).__init__(name, label, plot, fit, **kwargs)

    def _read_hist(self, roothist):
        binsx = roothist.GetNbinsX()

        self.xbins   = np.array([ roothist.GetXaxis().GetBinLowEdge(i) for i in xrange(binsx+1) ])
        self.xvalues = np.array([ roothist.GetXaxis().GetBinCenter(i) for i in xrange(binsx) ])
        self.yvalues = np.array([ roothist.GetBinContent(i) for i in xrange(binsx) ])

    def fromROOT(self, filename="", plotname="Histogram", fitname=""):
        if not filename:
            filename = self.name + ".root"

        rootfile = ROOT.TFile(filename)
        if not rootfile:
            message("Could not open TFile '{0}'.".format(filename), severity=3)
            return None

        roothist = rootfile.Get(plotname)
        if not roothist:
            message("Could not find TH1 '{0}' in file.".format(plotname), severity=3, level=1)
            return None

        self.filename = filename
        message("Reading histogram '{1}' from file '{0}' ...".format(filename, plotname), severity=1)

        self._read_hist(roothist)

        if fitname:
            self._read_fit(roothist, fitname)

        rootfile.Close()
        return self

    def fromROOTCanvas(self, filename="", plotname="Histogram", canvasname="Canvas_1", fitname=""):
        if not filename:
            filename = self.name + ".root"

        rootfile = ROOT.TFile(filename)
        if not rootfile:
            message("Could not open TFile '{0}'.".format(filename), severity=3)
            return None

        rootcanvas = rootfile.Get(canvasname)
        if not rootcanvas:
            message("Could not find TCanvas '{0}' in file.".format(canvasname), severity=3, level=1)
            return None

        roothist = rootcanvas.GetPrimitive(plotname)
        if not roothist:
            message("Could not find TH1 '{0}' inside canvas.".format(plotname), severity=3, level=1)
            return None

        self.filename = filename
        message("Reading histogram '{1}' from file '{0}' ...".format(filename, plotname), severity=1)

        self._read_hist(roothist)

        if fitname:
            self._read_fit(roothist, fitname)

        rootfile.Close()
        return self

########################################################################

"""
A simple class that represents a ROOT 2-d histogram.

The class holds all data points and fit parameters, and can be used
to retrieve existing data from a ROOT file that contains a TH1x object.
"""
class Hist2D(Base.Hist2D, Graph):
    def __init__(self, name, label="", plot=None, fit=None, **kwargs):
        super(Hist2D, self).__init__(name, label, plot, fit, **kwargs)

    def _read_hist(self, roothist):
        binsx, binsy = roothist.GetNbinsX(), roothist.GetNbinsY()

        self.xbins   = np.array([ roothist.GetXaxis().GetBinLowEdge(i) for i in xrange(binsx+1) ])
        self.ybins   = np.array([ roothist.GetYaxis().GetBinLowEdge(j) for j in xrange(binsy+1) ])
        self.xvalues = np.array([ roothist.GetXaxis().GetBinCenter(i) for i in xrange(binsx) ])
        self.yvalues = np.array([ roothist.GetYaxis().GetBinCenter(j) for j in xrange(binsy) ])
        self.zvalues = np.array([ [ roothist.GetBinContent(i,j) for j in xrange(binsy) ] for i in xrange(binsx) ])

    def fromROOT(self, filename="", plotname="Histogram", fitname=""):
        if not filename:
            filename = self.name + ".root"

        rootfile = ROOT.TFile(filename)
        if not rootfile:
            message("Could not open TFile '{0}'.".format(filename), severity=3)
            return None

        roothist = rootfile.Get(plotname)
        if not roothist:
            message("Could not find TH1 '{0}' in file.".format(plotname), severity=3, level=1)
            return None

        self.filename = filename
        message("Reading histogram '{1}' from file '{0}' ...".format(filename, plotname), severity=1)

        self._read_hist(roothist)

        if fitname:
            self._read_fit(roothist, fitname)

        rootfile.Close()
        return self

    def fromROOTCanvas(self, filename="", plotname="Histogram", canvasname="Canvas_1", fitname=""):
        if not filename:
            filename = self.name + ".root"

        rootfile = ROOT.TFile(filename)
        if not rootfile:
            message("Could not open TFile '{0}'.".format(filename), severity=3)
            return None

        rootcanvas = rootfile.Get(canvasname)
        if not rootcanvas:
            message("Could not find TCanvas '{0}' in file.".format(canvasname), severity=3, level=1)
            return None

        roothist = rootcanvas.GetPrimitive(plotname)
        if not roothist:
            message("Could not find TH1 '{0}' inside canvas.".format(plotname), severity=3, level=1)
            return None

        self.filename = filename
        message("Reading histogram '{1}' from file '{0}' ...".format(filename, plotname), severity=1)

        self._read_hist(roothist)

        if fitname:
            self._read_fit(roothist, fitname)

        rootfile.Close()
        return self

########################################################################
