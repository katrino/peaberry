#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import print_function

import sys
import traceback
import importlib

# This file tries to load a set of sub-modules allowing them to fail,
# so modules with optional dependencies don't break the whole module.

def _try_import(modules):
    loaded = []
    #print("Module paths:\n" + "\n\t".join(sys.path))
    for mod in modules:
        try:
            importlib.import_module('.' + mod, __name__)
            loaded.append(mod)
        except ImportError as e:
            print("WARNING: Not loading {} module.".format(mod))
            #print(e)
            #print(traceback.format_exc())
        except Exception as e:
            print("ERROR: Could not load {} module.".format(mod))
            print(e)
            print(traceback.format_exc())
    return loaded

MODULES = [
    "Main",
    "Base",
    "Fits",
#    "SpecialFits",
#    "CySpecialFits",
    "Callbacks",
    "CSV",
    "Root",
#    "Beans",
#    "Kassiopeia",
    "VTK",
    "LMFit",
    "MINUIT",
    "MCMC",
    "GaussProcMCMC",
#    "KROOT",
]

__all__ = _try_import(MODULES)
