#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
import george

from Peaberry.Main import message, exception
import Peaberry.Base as Base
import Peaberry.MCMC as MCMC

########################################################################
########################################################################

class Fit(MCMC.Fit):

    # basic kernels
    ConstantKernel      = george.kernels.ConstantKernel
    WhiteKernel         = george.kernels.WhiteKernel
    DotProductKernel    = george.kernels.DotProductKernel

    # radial kernels
    ExpKernel           = george.kernels.ExpKernel
    ExpSquaredKernel    = george.kernels.ExpSquaredKernel
    Matern32Kernel      = george.kernels.Matern32Kernel
    Matern52Kernel      = george.kernels.Matern52Kernel

    # periodic kernels
    CosineKernel        = george.kernels.CosineKernel
    ExpSine2Kernel      = george.kernels.ExpSine2Kernel

    @staticmethod
    def _lnlike(theta, fit, x, y, yerr):
        p, k = theta[:fit.ndim], theta[-fit.kdim:]

        model = fit.fitfunc._fit(x, *p)

        gp = george.GP(k[0] * fit.kernel(np.exp(k[1])))
        try:
            gp.compute(x, yerr)
            return gp.lnlikelihood(y - model)
        except:
            return -np.inf

    def __init__(self, fitfunc, graph=None, kernel=WhiteKernel):
        super(Fit, self).__init__(fitfunc, graph)

        self.kernel = kernel

        self.mcmcParams = OrderedDict()
        self.mcmcParams["ln(a)"]   = Base.FitParam("ln(a)",   value=0, limits=(0,1))
        self.mcmcParams["ln(tau)"] = Base.FitParam("ln(tau)", value=0, limits=(-10,10))


########################################################################
