#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
from scipy import special

from Peaberry.Main import message
import Peaberry.Base as Base

########################################################################
########################################################################

class Constant(Base.FitFunction):
    """
    A simple constant fit model.

    Note::
        This fit model uses one parameter:

        .. math::
            y(x) =  \\textit{const}
    """
    def __init__(self, parnames=[ "const" ]):
        super(Constant, self).__init__(parnames)

    def fit(self, x,const):
        return x * 0 + const  # make sure a numpy array is returned

    def guess(self, graph):
        """
        Estimate the fit parameters from the associated data.

        Returns:
            dict: A dictionary of the fit results, like ``{ 'name': (value, errors) }``.

        See also:
            :py:func:`Peaberry.Base.Fit.guess` explains the basic
            concept of this function.
        """
        yvalues = graph.getYvalues(filtered=True)

        const = np.mean(yvalues)

        params = self.fitParams.keys()

        self.setFitValues({
            params[0]: const,
        })
        return self.getFitValues()

########################################################################

class Linear(Base.FitFunction):
    """
    A simple linear fit model.

    Note::
        This fit model uses two parameters:

        .. math::
            y(x) =  \\textit{slope} \\cdot x
                 +  \\textit{offset}
    """
    def __init__(self, parnames=[ "slope","offset" ]):
        super(Linear, self).__init__(parnames)

    def fit(self, x,slope,offset):
        # standard regression
        return slope * x + offset

    def guess(self, graph):
        """
        Estimate the fit parameters from the associated data.

        Returns:
            dict: A dictionary of the fit results, like ``{ 'name': (value, errors) }``.

        See also:
            :py:func:`Peaberry.Base.Fit.guess` explains the basic
            concept of this function.
        """
        xvalues = graph.getXvalues(filtered=True)
        yvalues = graph.getYvalues(filtered=True)

        xmin, xmax = xvalues.min(), xvalues.max()
        ymin = yvalues(np.argmin(xvalues))
        ymax = yvalues(np.argmax(xvalues))

        slope  = (ymax - ymin) / (xmax - xmin)
        offset = ymin if (slope > 0) else ymax

        params = self.fitParams.keys()

        self.setFitValues({
            params[0]: slope,
            params[1]: offset,
        })
        return self.getFitParams()

########################################################################

class Parabolic(Base.FitFunction):
    """
    A simple parabolic fit model.

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot (x - \\textit{mean})^2
                 +  \\textit{offset}
    """
    def __init__(self, parnames=[ "mean","amp","offset" ]):
        super(Parabolic, self).__init__(parnames)

    def fit(self, x,mu,amp,offset):
        # standard parabola
        return amp * np.square(x-mu) + offset

########################################################################

class Exponential(Base.FitFunction):
    """
    A simple exponential fit model (decay curve).

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\exp\\left( \\frac{-x}{\\textit{const}} \\right)
                 +  \\textit{offset}
    """
    def __init__(self, parnames=[ "amp","const","offset" ]):
        super(Exponential, self).__init__(parnames)

    def fit(self, x,amp,lam,offset):
        # standard decay curve
        return amp * np.exp(-x/lam) + offset

########################################################################

class DoubleExponential(Base.FitFunction):
    """
    A double-exponential fit model (decay curve).

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\textit{amp}_0 \\cdot \\exp\\left( \\frac{-x}{\\textit{const}_0} \\right)
                 +  \\textit{amp}_1 \\cdot \\exp\\left( \\frac{-x}{\\textit{const}_1} \\right)
                 +  \\textit{offset}
    """
    def __init__(self, parnames=[ "amp0","const0","amp1","const1","offset" ]):
        super(DoubleExponential, self).__init__(parnames)

    def fit(self, x,amp0,lam0,amp1,lam1,offset):
        # double decay curve
        return amp0 * np.exp(-x/lam0) + amp1 * np.exp(-x/lam1) + offset

########################################################################

class ExponentialShifted(Base.FitFunction):
    """
    A shifted exponential fit model (modified decay curve).

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\exp\\left( \\frac{-(x - \\textit{shift})}{\\textit{const}} \\right)
                 +  \\textit{offset}
    """
    def __init__(self, parnames=[ "amp","const","offset","shift" ]):
        super(ExponentialShifted, self).__init__(parnames)

    def fit(self, x,amp,lam,offset,mu):
        # standard decay curve
        return amp * np.exp(-(x-mu)/lam) + offset

########################################################################

class Gaussian(Base.FitFunction):
    """
    A simple gaussian fit model.

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\exp\\left( \\frac{-(x - \\textit{mean})^2}{(2 \\cdot \\textit{width})^2)} \\right)
    """
    def __init__(self, parnames=[ "mean","width","amp" ]):
        super(Gaussian, self).__init__(parnames)

        self.normalized = True

    def fit(self, x,mu,sigma,amp):
        # standard gauss function
        if self.normalized:
            norm = 1. / (np.sqrt(2*np.pi) * sigma)
        else:
            norm = 1.
        return amp * norm * np.exp( -0.5 * np.square((x - mu) / sigma) )

########################################################################

class GaussianShifted(Base.FitFunction):
    """
    A shifted gaussian fit model (gaussian with offset).

    Note::
        This fit model uses four parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\exp\\left( \\frac{-(x - \\textit{mean})^2}{(2 \\cdot \\textit{width})^2)} \\right)
                    + \\textit{offset}
    """
    def __init__(self, parnames=[ "mean","width","amp","offset" ]):
        super(GaussianShifted, self).__init__(parnames)

        self.normalized = True

    def fit(self, x,mu,sigma,amp,bkg):
        # shifted gauss function
        if self.normalized:
            norm = 1. / (np.sqrt(2*np.pi) * sigma)
        else:
            norm = 1.
        return amp * norm * np.exp(-np.square(x-mu)/(2.*sigma**2)) + bkg

########################################################################

class GaussianExpModified(Base.FitFunction):
    """
    A exponentially-modified gaussian fit model.

    Note::
        This fit model uses four parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\frac{\\textit{skew}}{2}
                    \\cdot \\exp\\left( \\frac{\\textit{skew}}{2} (2 \\cdot \\textit{mean} + \\textit{skew} \\cdot \\textit{sigma}^2 - 2 x) \\right)
                    \\cdot \\text{erfc}\\left( \\frac{\\textit{mean} + \\textit{skew} \\cdot \\textit{sigma}^2 - x}{\\sqrt{2} \\cdot \\textit{sigma}} \\right)

            \\text{erfc}(x) = 1 - \\text{erf}(x)
    """
    def __init__(self, parnames=[ "mean","width","amp","skew" ]):
        super(GaussianExpModified, self).__init__(parnames)

    def fit(self, x,mu,sigma,amp,lam):
        def erfc(x):
            # complementary error function
            return 1. - special.erf(x)

        sigma2 = sigma**2
        return amp * lam/2. * np.exp(lam/2. * (2*mu + lam*sigma2 - 2.*x)) * erfc((mu + lam*sigma2 - x) / (np.sqrt(2)*sigma) )

########################################################################

class GaussianSkewed(Base.FitFunction):
    """
    A skewed gaussian fit model.

    Note::
        This fit model uses four parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\phi\\left( \\frac{x - \\textit{mean}}{\\textit{width}} \\right)
                                 \\cdot \\Phi\\left( \\textit{skew} \\cdot \\frac{x - \\textit{mean}}{\\textit{width}}\\right)

            \\phi(x) = \\text{gauss}(x)
                     = \\frac{1}{\\sqrt{2 \\pi}} \\exp\\left( \\frac{-x^2}{2} \\right)

            \\Phi(x) = \\text{cdf}(x)
                     = \\frac{1}{2} \\left[ 1 + \\text{erf}\\left( \\frac{x}{\\sqrt{2}} \\right) \\right]

        Details can be found in the Wikipedia_ article.

    .. _Wikipedia:
        https://en.wikipedia.org/wiki/Skew_normal_distribution

    """
    def __init__(self, parnames=[ "mean","width","amp","skew" ]):
        super(GaussianSkewed, self).__init__(parnames)

    def fit(self, x,mu,sigma,amp,alpha):
        def gauss(x):
            # standard gauss function
            return 1./np.sqrt(2.*np.pi) * np.exp(-0.5*np.square(x))

        def cdf(x):
            # cdf for skewed gaussian
            return 0.5 * (1. + special.erf(x/np.sqrt(2.)))

        k = (x-mu) / sigma
        return amp * gauss(k) * cdf(alpha * k)

########################################################################

class GaussianSkewedShifted(Base.FitFunction):
    """
    A skewed gaussian fit model wit shift.

    Note::
        This fit model uses four parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\phi\\left( \\frac{x - \\textit{mean}}{\\textit{width}} \\right)
                                 \\cdot \\Phi\\left( \\textit{skew} \\cdot \\frac{x - \\textit{mean}}{\\textit{width}}\\right)
                    + \\textit{offset}

            \\phi(x) = \\text{gauss}(x)
                     = \\frac{1}{\\sqrt{2 \\pi}} \\exp\\left( \\frac{-x^2}{2} \\right)

            \\Phi(x) = \\text{cdf}(x)
                     = \\frac{1}{2} \\left[ 1 + \\text{erf}\\left( \\frac{x}{\\sqrt{2}} \\right) \\right]

        Details can be found in the Wikipedia_ article.

    .. _Wikipedia:
        https://en.wikipedia.org/wiki/Skew_normal_distribution

    """
    def __init__(self, parnames=[ "mean","width","amp","skew","offset" ]):
        super(GaussianSkewedShifted, self).__init__(parnames)

    def fit(self, x,mu,sigma,amp,alpha,offset):
        def gauss(x):
            # standard gauss function
            return 1./np.sqrt(2.*np.pi) * np.exp(-0.5*np.square(x))

        def cdf(x):
            # cdf for skewed gaussian
            return 0.5 * (1. + special.erf(x/np.sqrt(2.)))

        k = (x-mu) / sigma
        return amp * gauss(k) * cdf(alpha * k) + offset

########################################################################

class ErrorFunction(Base.FitFunction):
    """
    An error-function model.

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\frac{\\textit{amp}}{2}
                    \\cdot \\left[ 1 + \\text{erf}\\left( \\frac{(x - \\textit{mean}}{\\sqrt{2} \\cdot \\textit{width}} \\right) \\right]
    """
    def __init__(self, parnames=[ "mean","width","amp" ]):
        super(ErrorFunction, self).__init__(parnames)

    def fit(self, x,mu,sigma,amp):
        # error function
        #return amp/2 * (1 + special.erf((x-mu)/(np.sqrt(2)*sigma)));
        def cdf(x):
            # cdf for error function
            return 0.5 * (1. + special.erf(x/np.sqrt(2.)))

        return amp * cdf((x-mu)/sigma)

########################################################################

class ErrorFunctionExpModified(Base.FitFunction):
    """
    An error-function model based on an exponentially-modified gaussian.

    Note::
        This fit model uses four parameters:

        .. math::
            y(x) =  \\frac{\\textit{amp}}{2}
                    \\cdot \\left[ 1 + \\text{erf}\\left( \\frac{(x - \\textit{mean}}{\\sqrt{2} \\cdot \\textit{width}} \\right) \\right]
    """
    def __init__(self, parnames=[ "mean","width","amp","skew" ]):
        super(ErrorFunctionExpModified, self).__init__(parnames)

    def fit(self, x,mu,sigma,amp,lam):
        def cdf(x):
            # cdf for error function
            return 0.5 * (1. + special.erf(x/np.sqrt(2.)))

        u = lam * (x - mu)
        v = lam * sigma

        return amp * (cdf(u/v) - np.exp( -u + 0.5*v**2 + np.log(cdf((u - v**2)/v)) ))

########################################################################

class Voigt(Base.FitFunction):
    """
    A Voigt-profile fit model.

    Note::
        This fit model uses three parameters:

        .. math::
            y(x) =  \\textit{amp} \\cdot \\exp\\left( \\frac{-(x - \\textit{mean})^2}{(2 \\cdot \\textit{width})^2)} \\right)
    """
    def __init__(self, parnames=[ "mean","sigma","gamma","amp" ]):
        super(Voigt, self).__init__(parnames)

    def fit(self, x,mu,sigma,gamma,amp):
        z = (x - mu + 1j * gamma) / (sigma * np.sqrt(2))
        return amp * np.real(special.wofz(z)) / (sigma * np.sqrt(2 * np.pi))
