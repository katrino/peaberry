#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function

import numpy as np
import vtk

from Peaberry.Main import message
import Peaberry.Base as Base

########################################################################
########################################################################

"""
A simple class that represents a VTK graph.

The class holds all data points and fit parameters, and can be used
for post-analysis.
"""
class Graph(Base.Graph):

    def __init__(self, name, label="", plot=None, fit=None, **kwargs):
        super(Graph, self).__init__(name, label, plot, fit, **kwargs)

        self.inputPath = '{0}'

    def fromVTK(self, filename="", xname="initial_time", yname="final_time", callback=None):
        if not filename:
            filename = self.name + ".vtp"
        filename = self.inputPath.format(filename)

        reader = PolydataReader()
        reader.openFile(filename)
        if callback != None:
            reader.select([ xname, yname ])

        for item in iter(reader):
            if callback != None:
                x, y = callback(item)
            else:
                x, y = item[xname], item[yname]

            self.addPoint(x, y)

        return self

########################################################################

class Hist(Base.Hist, Graph):

    def __init__(self, name, label="", plot=None, fit=None, **kwargs):
        super(Hist, self).__init__(name, label, plot, fit, **kwargs)

        self.inputPath = '{0}'
        self.treePath = '{0}_DATA'

    def fromKassiopeia(self, filename="", group="output_track_world", xname="initial_time", callback=None, bins=20, **kwargs):
        if not filename:
            filename = self.name + ".root"
        filename = self.inputPath.format(filename)

        reader = PolydataReader()
        reader.openFile(filename)
        if callback != None:
            reader.select([ xname ])

        for item in iter(reader):
            if callback != None:
                x = callback(item)
            else:
                x = item[xname]

            self.addPoint(x)

        self.build(bins=bins, **kwargs)
        return self

########################################################################

class Hist2D(Base.Hist, Graph):

    def __init__(self, name, label="", plot=None, fit=None, **kwargs):
        super(Hist, self).__init__(name, label, plot, fit, **kwargs)

        self.inputPath = '{0}'
        self.treePath = '{0}_DATA'

    def fromKassiopeia(self, filename="", group="output_track_world", xname="initial_time", yname="final_time", callback=None, bins=(20,20), **kwargs):
        if not filename:
            filename = self.name + ".root"
        filename = self.inputPath.format(filename)

        reader = PolydataReader()
        reader.openFile(filename)
        if callback != None:
            reader.select([ xname, yname ])

        for item in iter(reader):
            if callback != None:
                x, y = callback(item)
            else:
                x, y = item[xname], item[yname]

            self.addPoint(x, y)

        self.build(bins=bins, **kwargs)
        return self

########################################################################

class PolydataReader:

    def __init__(self, filename=None):

        self.file = None
        self.iev  = 0
        self.nev  = 0

        self.polydata = None
        #self.points = None
        self.arrays = {}

        if filename:
            self.openFile(filename)

    def __len__(self):
        return int(self.nev)

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def openFile(self, filename):
        """
        Open ROOT file for reading.

        Args:
            filename (string): The name of the ROOT file.
        """
        if self.file:
            self.closeFile()

        self.file = vtk.vtkXMLPolyDataReader()
        self.file.SetFileName(filename)
        self.file.Update()

        self.polydata = self.file.GetOutput()

        pointdata = self.polydata.GetPointData()
        n = pointdata.GetNumberOfArrays()
        for i in range(n):
            name = pointdata.GetArrayName(i)
            self.arrays[name] = i

        #self.points = polydata.GetPoints()

        self.iev = 0
        self.nev = self.polydata.GetPoints().GetNumberOfPoints()

        print("Loaded VTK polydata: {} points with {} arrays".format(self.nev, len(self.arrays)))

    def closeFile(self):
        """
        Close previously opened ROOT file.
        """
        if not self.file:
            return

        self.file = None

    def select(self, columns=[]):
        """
        Select a given list of columns to be read at each iteration.

        Args:
            columns (list): A list of column names to read.
        """
        arrays = {}
        for name in columns:
            if name in self.arrays.keys():
                arrays[name] = self.arrays[name]

        self.arrays = arrays

    def next(self):
        """
        Iterate through the tree that was loaded before.

        Returns:
            (dict): Contents of the tree at the current entry, like
                    { 'column1': value1, 'column2': value2, ... }.
        """
        if not self.file:
            return

        if self.iev >= self.nev:
            self.reset()
            raise StopIteration()

        #print("Reading from VTK polydata: index {}".format(self.iev))

        point = self.polydata.GetPoints().GetPoint(self.iev)

        data = {}
        data['position_x'] = point[0]
        data['position_y'] = point[1]
        data['position_z'] = point[2]
        for name, idx in self.arrays.items():
            data[name] = self.polydata.GetPointData().GetArray(idx).GetValue(self.iev)

        self.iev += 1

        return data

    def reset(self):
        """
        Reset the iterator to start at the first tree entry.
        """
        self.iev = 0
