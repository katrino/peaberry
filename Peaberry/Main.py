#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
from scipy import stats, interpolate
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from matplotlib import rcParams, colors, colorbar
from matplotlib.ticker import MaxNLocator, ScalarFormatter, LogFormatter

import copy
import sys
import os

########################################################################
########################################################################

# A global variable to disable all output messages below a certain severity level
SILENT = 0

# A global variable to enable colorized messages
COLORS = True

########################################################################

def rc(family='DejaVu Sans', size=14, titlesize='medium', labelsize='small', dpi=300):

    rcParams['font.family'] = family
    rcParams['font.size']   = size

    #rcParams['figure.titlesize'] = titlesize

    rcParams['legend.fontsize'] = labelsize

    rcParams['axes.titlesize'] = titlesize
    rcParams['axes.labelsize'] = labelsize

    rcParams['xtick.labelsize'] = labelsize
    rcParams['ytick.labelsize'] = labelsize

    rcParams['savefig.dpi'] = dpi

########################################################################

def message(text, level=0, severity=0, colors=COLORS, bold=False):
    """
    Print a message to the terminal, colored according to severity.

    Messages will be padded from the left according to the
    indentation level, which can be used to align messages
    that are related (e.g. from the same function).

    Args:
        text (string): The message to display.
        level (int): The level of indentation.
    """
    if severity < SILENT:
        return

    msg = u""
    if colors:
        if bold:
            msg += '\033[1m'
        if severity == 0:       # normal
            msg += '\033[94m'
        elif severity == 1:     # info
            msg += '\033[92m'
        elif severity == 2:     # warning
            msg += '\033[93m'
        elif severity >= 3:     # error
            msg += '\033[91m'
    msg += u"%3s " % (">" * max(0,3-level))
    msg += text
    if colors:
        msg += u'\033[0m'
    print(msg)

def exception(e, text):
    """
    Print an exception to the terminal.

    The message also contains the line number and source file where
    the exception occured.

    Args:
        e (Exception): An exception that was raised.
        text (string): An additional message to explain the exception.
    """
    message("{2}:{3}:{4} - {0} ({1})".format(text, str(e),
            os.path.basename(sys.exc_info()[2].tb_frame.f_code.co_filename),
            sys.exc_info()[2].tb_lineno,
            sys.exc_info()[0].__name__),
            severity=2)

########################################################################

class Plot(object):
    """
    A class that represents a plot of BEANS data.

    Note:
        The ``style`` dictionary allows to override the default global
        plotting style for all graphs. The following style groups are available:
            * ``(empty)`` - Global options.
            * ``data`` - Options for plotting the data points.
            * ``errorband`` - Options for plotting the error bands (instead of error bars).
            * ``fit`` - Options for plotting the fit results.
            * ``fitband`` - Options for plotting the fit uncertainty bands.
            * ``residuals`` - Options for plotting the residuals.
            * ``residband`` - Options for plotting the residual uncertainty bands.
            * ``residhist`` - Options for plotting the residual histograms.
            * ``residprob`` - Options for plotting the residual probability (via `stats.probplot`).
            * ``stats`` - Options for displaying graph statistics (*not used*).
            * ``label`` - Options for displaying the graphs in the legend.
            * ``indicator`` - Options for plotting indicators (fixed x- or y-values).
            * ``legend`` - Options for displaying the legend.

        Additionally, the ``plot`` dictionary allows to change the
        plot contents:
            * ``grid`` - If ``True``, add a grid to the plot.
            * ``errorband`` - If ``True``, plot errors as colored band instead of error bars.
            * ``fit`` - If ``True``, show the fit results with the data points.
            * ``fitband`` - If ``True``, show the fit uncertainties as colored band.
            * ``residuals`` - If ``True``, show the residuals below the main plot ()
            * ``residband`` - If ``True``, show the residual mean and std.dev. with the residuals.
            * ``residhist`` - If ``True``, show the residual histogram next to the residuals.
            * ``residprob`` - If ``True``, show the residual probability plot next to the residuals.
            * ``indicators`` - If ``True``, add any defined indicators to the plot.
            * ``legend`` - A string to set the position of the legend (default: `best`).
            * ``colors`` - A string to set the color map to plot the different graphs (default: `brg`).
            * ``residratio`` - A float to set the height of the residuals plot, larger numbers decrease the size (default: `10` = 1/10 of total height).
            * ``residnorm`` - If ``True``, residuals are normalized with the associated y-errors (default);
                    otherwise, residuals are just the difference between data points and fit model.
            * ``fitsplinefitspline`` - If ``True``, the fit line and uncertainty band will be computed at a small number of points;
                    the fit band will then use spline interpolation for drawing; this is useful for fit functions that take
                    a long time to evaluate and thus slow down the plotting process.

    It provides a structure to hold one or more BEANS graphs, and allows
    to plot the simultaneously in one figure (multiplot).

    The BEANS data is read from the ROOT output file provided by the BEANS
    script. When adding a plot to the class instance, the given
    TGraphErrors object is converted to a BeansGraph instance (including
    error bars). If possible, the ROOT fit parameters are also put into the
    BeansGraph object, but without the actual fit function. The function
    has to be provided by the Python class itself (as a method named
    _fitfunc), and one should take care to ensure that both ROOT and Python
    function match.

    When generating the final plot, all graphs are added on top of each
    other. If a fit result was provided by BEANS, the fit curve will
    also be drawn in the plot. The fit curve will be shaded with a 2-sigma
    band computed from the fit errors, if available. The plot will also
    include a plot of the residuals (data - fit). If the legend is enabled,
    it will use the _labelfunc class function to generate labels for each
    graph. The plot function can execute a series of callback functions
    for each graph prior to drawing.
    """
    def __init__(self, title):
        self.title = title
        self.index = 0

        rc()

        self.setLabels()
        self.setLimits()
        self.setTicks()
        self.setLogscale(False, False)
        self.setMargins()

        self.setOutputPathShared()
        self.setFitPoints()
        self.setSigma()

        self.graphs = OrderedDict()
        self.indicators = []

        self.colormap  = None
        self.colornorm = None

        self.xmin  = self.xmax = None
        self.ymin  = self.ymax = None
        self.ymin2 = self.ymax2 = None

        self.plot = {
            'grid':         False,      # draw grid
            'errorband':    False,      # draw data errors as band (instead of errorbars)
            'fit':          True,       # draw fit curve
            'fitband':      True,       # draw fit uncertainty bands (1-sigma etc.)
            'residuals':    True,       # draw residuals
            'residband':    True,       # draw residual uncertainty bands (1-sigma etc.)
            'residhist':    False,      # draw residual histogram
            'residprob':    False,      # draw residual prob-plot (qq-plot)
            'indicators':   True,       # draw indicators (horizontal/vertical line markers)
            'legend':       'best',     # draw legend at given position
            'colors':       'brg',      # default colormap for plotted graphs
            'residratio':   9,          # heifht scale of residuals sub-plot
            'residnorm':    True,       # compute normalized residuals for drawing
            'fitspline':    False,      # use spline interpolation to draw fit curve
            'fitexcluded':  True,       # recolor data points excluded from fit
            'rasterized':   False,      # rasterize data points etc. to reduce PDF file size
            'thinning':     0,          # thin out data points to speed up plotting time & reduce file size
        }

        self.style      = {
            '':             {},
            'data':         {},
            'errorband':    {},
            'fit':          {},
            'fitband':      {},
            'residuals':    {},
            'residband':    {},
            'residhist':    {},
            'residprob':    {},
            'stats':        {},
            'label':        {},
            'indicator':    {},
            'legend':       {},
        }

    def __str__(self):
        result = "Plot with {0} elements".format(len(self.graphs))
        if self.title:
            result += " ({0})".format(self.title)
        return result

    def __len__(self):
        return len(self.graphs)

    def __getitem__(self, key):
        if key in self.graphs:
            return self.graphs[key]
        return None

    def __delitem__(self, key):
        if key in self.graphs:
            del self.graphs[key]

    def __contains__(self, key):
        return (key in self.graphs)

    def _className(self):
        return self.__class__.__name__

    def _update_limits(self, xvalues, yvalues, xerrors=None, yerrors=None, secondary=False):

        def _min(old, new):
            if old == None:
                return new
            return min(old, new)

        def _max(old, new):
            if old == None:
                return new
            return max(old, new)

        if xerrors == None:
            self.xmin = _min(self.xmin, np.min(xvalues))
            self.xmax = _max(self.xmax, np.max(xvalues))
        else:
            self.xmin = _min(self.xmin, np.min(xvalues-xerrors))
            self.xmax = _max(self.xmax, np.max(xvalues+xerrors))

        if yerrors == None:
            if secondary:
                self.ymin2 = _min(self.ymin2, np.min(yvalues))
                self.ymax2 = _max(self.ymax2, np.max(yvalues))
            else:
                self.ymin = _min(self.ymin, np.min(yvalues))
                self.ymax = _max(self.ymax, np.max(yvalues))
        else:
            if secondary:
                self.ymin2 = _min(self.ymin2, np.min(yvalues-yerrors))
                self.ymax2 = _max(self.ymax2, np.max(yvalues+yerrors))
            else:
                self.ymin = _min(self.ymin, np.min(yvalues-yerrors))
                self.ymax = _max(self.ymax, np.max(yvalues+yerrors))

    def _set_limits(self, ax, xmin=None, xmax=None, ymin=None, ymax=None, ymin2=None, ymax2=None):

        if xmin != None and xmax != None:
            ext = 0.05 * (xmax - xmin)
            xmin -= ext
            xmax += ext

            if self.xlimits and self.xlimits[0] != None:
                xmin = self.xlimits[0]
            if self.xlimits and self.xlimits[1] != None:
                xmax = self.xlimits[1]

            ax.set_xlim(xmin, xmax)

        if ymin != None and ymax != None:
            ext = 0.05 * (ymax - ymin)
            ymin -= ext
            ymax += ext

            if self.ylimits and self.ylimits[0] != None:
                ymin = self.ylimits[0]
            if self.ylimits and self.ylimits[1] != None:
                ymax = self.ylimits[1]

            ax.set_ylim(ymin, ymax)

        if ymin2 != None and ymax2 != None:
            ext = 0.05 * (ymax2 - ymin2)
            ymin2 -= ext
            ymax2 += ext

            if self.ylimits2 and self.ylimits2[0] != None:
                ymin2 = self.ylimits2[0]
            if self.ylimits2 and self.ylimits2[1] != None:
                ymax2 = self.ylimits2[1]

            ax.set_ylim(ymin2, ymax2)

    def append(self, graph):
        """
        Add a graph to this plot.

        Args:
            graph (``Peaberry.Base.Graph``): The graph to add.
        """
        graph.setPlot(self)

    def setStyle(self, group, values):
        """
        Change style setting for the plot itself or the plot contents.

        The special style group ``plot`` is used to change general
        settings like the plot's layout. Other style groups
        like ``data`` or ``fit`` apply to the plot contents.

        Note:
            The values given to this function override the plot's
            default settings. These values can be overriden again in
            each graph that is plotted, using the graph's ``setStyle``
            method. This can be used to set a default setting (e.g.
            linewidth=2) for all graphs, and use different linestyles
            in the single graphs.

        Args:
            group (string): The style group in which the given values
                are applied, e.g. ``plot``, ``data`` or ``fit``.
            values (dict):  A dictionary with style settings for the
                selected group.
        """
        if group == 'plot':
            for k,v in values.items():
                # only allow to change existing values
                if not k in self.plot:
                    raise KeyError
                self.plot[k] = v
        elif group in self.style:
            for k,v in values.items():
                # allow to add new values (which overwrite defaults)
                self.style[group][k] = v

    def setColorMap(self, cmap=None, norm=None, vmin=0, vmax=0):
        """
        Set a colormap which is used to colorize the plot contents.

        When a colormap is define using this method, graphs will
        be colored according to their ``Base.Graph.colorvalue``
        attribute.

        Note:
            If the range of values is not defined by ``vmin``,``vmax``
            here, the range is auto-detected from the ``colorvalue``
            of all graphs in this plot.

        Args:
            cmap (``matplotlib.colors.colormap``): Colormap to use.
            norm (``matplotlib.colors.norm``): Optional norm to use
                when a color is chosen for a specific graph.
            vmin (float):   Minimum value to use in the colormap
                (only applies if ``norm`` is not defined).
            vmax (float):   Maximum value to use in the colormap
                (only applies if ``norm`` is not defined).
        """
        if cmap == None:
            cmap = plt.get_cmap('jet')

        if norm == None:
            if vmin == vmax:
                vmin = vmax = None
                for graph in self.graphs.values():
                    if vmin == None or graph.colorvalue < vmin:
                        vmin = graph.colorvalue
                    if vmax == None or graph.colorvalue > vmax:
                        vmax = graph.colorvalue
            norm = colors.Normalize(vmin=vmin, vmax=vmax)

        self.colormap  = cmap
        self.colornorm = norm

    def setAxis(self, axis='primary', color=None, nloc=None, offset=False, logscale=False):

        def _set_yaxis(ax):
            if logscale:
                ax.yaxis.set_major_formatter(LogFormatter())
            else:
                ax.yaxis.set_major_formatter(ScalarFormatter(useOffset=offset))

            if nloc != None:
                ax.yaxis.set_major_locator(MaxNLocator(nloc))

            if color != None:
                ax.tick_params(axis='y', colors=color)
                ax.yaxis.label.set_color(color)

        def _apply(axis, ax1, ax2):
            if axis == 'primary':
                _set_yaxis(ax1)
                if color != None:
                    ax1.spines['left'].set_color(color)
                    if ax2 == None:
                        ax1.spines['right'].set_color(color)

            elif axis == 'secondary':
                _set_yaxis(ax2)
                if color != None:
                    ax1.spines['right'].set_color(color)

        # apply style to main plot and any insets
        ax1 = self.pltFrames['primary']
        ax2 = self.pltFrames['secondary']
        _apply(axis, ax1, ax2)

        if 'inset' in self.pltFrames:
            for frames in self.pltFrames['inset']:
                ax1 = frames['primary']
                ax2 = frames['secondary']
                _apply(axis, ax1, ax2)

    def drawColorBar(self, fig=None, position='right', size=0.02, label=None, ticks=[], cmap=None, norm=None, **kwargs):
        """
        Draw a colorbar in the plot area.

        The colorbar uses the plot-wide colormap and norm. The position
        can be given as string (e.g. 'right') to make the colormap
        attach to the plot contents. Alternatively the position can
        be given as a 4-tuple which is passed to ``figure.add_axes``.

        Args:
            fig (``matplotlib.pyplot.figure``): Optional figure object
                    to attach the colorbar to (default: current figure).
            position (string or tuple): String or 4-tuple where to
                    place the colorbar (e.g. ``right`` or ``(x,y,w,h)``).
            placement (string): String to define the position of the
                    axis label and ticks.
            size (float):   Size of the colorbar (height or width)
                    in figure coordinates.
            label (string): Optional label for the colorbar.
            ticks (list):   Optional list of ticks for the colorbar.

        """
        if fig == None:
            fig = plt.gcf()

        if position == 'right':
            axes = ( 1-self.rmargin, self.bmargin, size, self.height )
            kwargs['orientation'] = 'vertical'
        elif position == 'left':
            axes = ( self.lmargin-size, self.bmargin, size, self.height )
            kwargs['orientation'] = 'vertical'
        elif position == 'top':
            axes = ( self.lmargin, 1-self.tmargin, self.width, size )
            kwargs['orientation'] = 'horizontal'
        elif position == 'bottom':
            axes = ( self.lmargin, self.bmargin-size, self.width, size )
            kwargs['orientation'] = 'horizontal'
        elif position == 'above':
            axes = ( self.lmargin, 1-self.tmargin+0.1, self.width, size )
            kwargs['orientation'] = 'horizontal'
        elif position == 'below':
            axes = ( self.lmargin, self.bmargin-0.1-size, self.width, size )
            kwargs['orientation'] = 'horizontal'
        else:
            axes = position

        ax = fig.add_axes(axes)
        self.pltFrames['colorbar'] = ax

        if cmap == None:
            cmap = self.colormap
        if norm == None:
            norm = self.colornorm

        cb = colorbar.ColorbarBase(ax, cmap=cmap, norm=norm, ticks=ticks, **kwargs)

        if self.plot['rasterized']:
            cb.solids.set_rasterized(True)

        if label != None:
            cb.set_label(label)

        if position in [ 'right', 'top', 'above', 'bottom' ]:
            cb.ax.xaxis.set_ticks_position('top')
            cb.ax.xaxis.set_label_position('top')
        elif position in [ 'left', 'below', 'bottom' ]:
            cb.ax.xaxis.set_ticks_position('bottom')
            cb.ax.xaxis.set_label_position('bottom')

        return cb

    def drawSecondXaxis(self, ax1=None, label=None, ticks=None, callback=None):
        """
        Draw second x-axis in the plot area.

        The new axis uses the same limits as the original one, but
        different ticks can be set. A callback function like
        ``lambda x: ("%.2f" % (1 / x))`` can be used if the axis
        should display different units than the original one.

        Args:
            ax1 (``matplotlib.pyplot.axes``): Axes object to attach the
                    new axis to (defaults to primary axes of the plot).
            label (string):     Optional label for the new axis.
            ticks (list):       Optional list of ticks for the new axis.
            callback (func):    Optional callback function to override
                    the tick labels.
        """

        if ax1 == None:
            ax1 = self.pltFrames['primary']

        ax2 = ax1.twiny()

        if ticks == None:
            ticks = ax1.get_xticks()

        ax2.set_xticks(ticks)
        ax2.set_xlim(ax1.get_xlim())

        if callback != None:
            # use callback function to override tick labels
            ax2.set_xticklabels(map(callback, ticks))

        if label != None:
            ax2.set_xlabel(label)

        return ax2

    def drawSecondYaxis(self, ax1=None, label=None, ticks=[], callback=None):
        """
        Draw second y-axis in the plot area.

        Note:
            This function is similar to ``:py:func:drawSecondXaxis``,
            but draws a second y-axis instead.
        """
        if ax1 == None:
            ax1 = self.pltFrames['primary']

        ax2 = ax1.twinx()

        if ticks == None:
            ticks = ax1.get_yticks()

        ax2.set_yticks(ticks)
        ax2.set_ylim(ax1.get_ylim())

        if callback != None:
            # use callback function to override tick labels
            ax2.set_yticklabels(map(callback, ticks))

        if label != None:
            ax2.set_ylabel(label)

        return ax2

    def setLabels(self, xlabel="", ylabel="", ylabel2=""):
        self.xlabel  = xlabel
        self.ylabel  = ylabel
        self.ylabel2 = ylabel2

    def setLimits(self, xlimits=None, ylimits=None, ylimits2=None):
        self.xlimits  = xlimits
        self.ylimits  = ylimits
        self.ylimits2 = ylimits2

    def setTicks(self, xticks=None, yticks=None, yticks2=None, xdist=0, ydist=0, ydist2=0):
        self.xticks  = xticks
        self.yticks  = yticks
        self.yticks2 = yticks2

        def _autoticks(dist, limits):
            ticks = []
            t = limits[0]
            while t <= limits[1] + dist:
                ticks.append( int(t / dist) * dist )
                t += dist
            return ticks

        if xdist > 0 and self.xlimits != None:
            self.xticks = _autoticks(xdist, self.xlimits)
        if ydist > 0 and self.ylimits != None:
            self.yticks = _autoticks(ydist, self.ylimits)
        if ydist2 > 0 and self.ylimits2 != None:
            self.yticks2 = _autoticks(ydists, self.ylimits2)

    def setLogscale(self, xlog=False, ylog=True, ylog2=True):
        self.xlogscale  = xlog
        self.ylogscale  = ylog
        self.ylogscale2 = ylog2

    def setMargins(self, lmargin=0.10, rmargin=0.05, bmargin=0.10, tmargin=0.05, hspace=0.07, vspace=0.06):
        self.lmargin = lmargin
        self.rmargin = rmargin
        self.bmargin = bmargin
        self.tmargin = tmargin

        self.hspace  = hspace
        self.vspace  = vspace

    def setOutputPathShared(self):
        self.outputPath = 'output/{0}.{1}'

    def setOutputPathSplit(self):
        self.outputPath = 'output-{1}/{0}.{1}'

    def setFitPoints(self, points=500):
        self.fitPoints = points

    def setSigma(self, sigma=[ 1 ], colors=None):
        try:
            self.sigma = list(sigma)
            self.sigmaColors = list(colors)
        except:
            self.sigma = sigma
            self.sigmaColors = colors

    def getStyle(self, key, style={}, graph=None, skip_global=False):
        if not skip_global:
            for k,v in self.style[''].items():  # use plot-global defaults
                style[k] = v

        if key in self.style:
            for k,v in self.style[key].items():  # use plot style
                style[k] = v

        if graph != None:
            if not skip_global:
                for k,v in graph.style[''].items():  # use graph-global defaults
                    style[k] = v

            if key in graph.style:
                for k,v in graph.style[key].items():  # use graph style
                    style[k] = v

        return style

    def getGraphName(self, name):
        newname, count = name, 0
        while newname in self:
            count  += 1
            newname = name + "-%d" % count
        return newname

    def getGraphColor(self, graph):

        if self.colormap:
            cmap = self.colormap
            norm = self.colornorm
        else:
            # fallback to old-style coloring
            cmap = plt.cm.get_cmap(self.plot['colors'])
            norm = colors.Normalize(vmin=0, vmax=len(self.graphs))

        return cmap(norm(graph.getColorValue()))  # colorvalue or index

    def addIndicator(self, x=None, y=None):
        self.indicators.append( (x,y) )

    def getLabel(self, graph):
        if graph.nolabel:
            return None

        labelstyle = self.getStyle('label', {}, graph, skip_global=True)
        label = graph.label if len(graph.label) else graph.name
        if graph.fit:
            fitlabel = graph.fit.getLabel(**labelstyle)
            if len(fitlabel) > 0:
                if len(label) > 0:
                    label += u"\n"
                label += fitlabel

        return label

    def plotData(self, ax, graph, xerr=True, yerr=True, thinning=0):
        if graph.isEmpty():
            return

        color = self.getGraphColor(graph)
        if graph.isHistogram():
            if graph.is2D():
                style = {}
            else:
                if graph.barplot:
                    style = {
                        'color':        color,
                        'lw':           1,
                        #'alpha':        1,
                    }
                else:
                    style = {
                        'marker':       '',
                        'color':        color,
                        'ms':           3,
                        'lw':           1,
                        'ls':           'steps',
                        #'alpha':        1,
                    }
        else:
            style = {
                'marker':       'o',
                'color':        color,
                'ms':           3,
                'lw':           1,
                'ls':           '',
                'capsize':      0   if self.plot['errorband'] else  1,
                'ecolor':       ''  if self.plot['errorband'] else  'k',
                #'alpha':        1,
            }
        style['zorder'] = 100
        style = self.getStyle('data', style, graph)

        label = self.getLabel(graph)

        xvalues = graph.getXvalues(filtered=False, thinning=thinning)
        yvalues = graph.getYvalues(filtered=False, thinning=thinning)

        if graph.is2D():
            zvalues = graph.getZvalues(filtered=False, thinning=thinning)

        xerrors, yerrors = None, None
        if not self.plot['errorband']:
            if xerr and graph.hasErrorsX():
                xerrors = graph.getXerrorsAsym(filtered=False, thinning=thinning)
            if yerr and graph.hasErrorsY():
                yerrors = graph.getYerrorsAsym(filtered=False, thinning=thinning)

        try:
            if graph.isHistogram():
                if graph.is2D():
                    xbins, ybins = graph.xbins, graph.ybins
                    ax.pcolorfast( xbins, ybins, zvalues.T,
                            label=label, rasterized=self.plot['rasterized'], **style )
                else:
                    if graph.barplot:
                        bins  = graph.xbins
                        width = np.array([ bins[i+1] - bins[i] for i in range(len(graph)) ])
                        ax.bar(xvalues, yvalues, yerr=yerrors, width=width,
                                label=label, rasterized=self.plot['rasterized'], **style)
                    else:
                        if graph.hasErrorsY():
                            ax.errorbar(xvalues, yvalues, yerr=yerrors,
                                    label=label, rasterized=self.plot['rasterized'], **style)
                        else:
                            ax.plot(xvalues, yvalues,
                                    label=label, rasterized=self.plot['rasterized'], **style)
            else:
                ax.errorbar(xvalues, yvalues, xerr=xerrors, yerr=yerrors,
                        label=label, rasterized=self.plot['rasterized'], **style)
        except Exception as e:
            exception(e, "Failed to plot data points!")

        self._update_limits(xvalues, yvalues, xerrors, yerrors, secondary=graph.secondary)
        if graph.isHistogram():
            if graph.is2D():
                self._update_limits(graph.xbins, graph.ybins, None, None, secondary=graph.secondary)
            else:
                self._update_limits(graph.xbins, yvalues, None, None, secondary=graph.secondary)

        if graph.fit and self.plot['fitexcluded']:
            # mask out data points that are not inside the fit range to redraw them with another color
            xvalues_inv = graph.getXvalues(filtered=True, invert=True, thinning=thinning)

            # split masked array into continuous chunks for correct drawing
            values_split = []
            xydata = []
            for i in xrange(len(xvalues)):
                if xvalues[i] in xvalues_inv:
                    xydata.append( (xvalues[i], yvalues[i]) )
                else:
                    values_split.append(xydata)
                    xydata = []
            values_split.append(xydata)  # always append last item

            style['color']  = '0.5'  # medium gray

            try:
                for xy in values_split:
                    if not len(xy):
                        continue
                    xvalues, yvalues = zip(*xy)

                    if graph.isHistogram():
                        if graph.is2D():
                            pass  # Not implementend
                        else:
                            if graph.barplot:
                                bins  = graph.xbins
                                width = [ bins[i+1] - bins[i] for i in range(len(graph)) ]
                                ax.bar(xvalues, yvalues, width=width,
                                        rasterized=self.plot['rasterized'], **style)
                            else:
                                ax.plot(xvalues, yvalues,
                                        rasterized=self.plot['rasterized'], **style)
                    else:
                        ax.errorbar(xvalues, yvalues,
                                rasterized=self.plot['rasterized'], **style)

            except Exception as e:
                exception(e, "Failed to plot filtered data points!")

    def plotErrorBand(self, ax, graph, thinning=0):
        if graph.isEmpty():
            return

        if not graph.hasErrorsY():
            return

        color = self.getGraphColor(graph)
        style = {
            'color':        color,
            'edgecolor':    'none',
            'alpha':        0.4,
        }
        style['zorder'] = 50
        style = self.getStyle('errorband', style, graph)

        xvalues = graph.getXvalues(filtered=False, thinning=thinning)
        yvalues = graph.getYvalues(filtered=False, thinning=thinning)
        yerrorsLo, yerrorsHi = graph.getYerrorsAsym(filtered=False, thinning=thinning)

        lower = yvalues - yerrorsLo
        upper = yvalues + yerrorsHi

        if self.ylogscale:  # plotting breaks if zero values exist in fill_between
            mask    = (lower != 0) & (upper != 0)
            xvalues = xvalues[mask]
            lower   = lower[mask]
            upper   = upper[mask]

        try:
            ax.fill_between(xvalues, lower, upper,
                    rasterized=self.plot['rasterized'], **style)
        except Exception as e:
            exception(e, "Failed to plot error band!")

    def plotFit(self, ax, graph, smooth=False):
        #if graph.isEmpty():
        #    return

        if not graph.hasFit():
            return
        fit = graph.fit

        label = None
        if graph.isEmpty():
            label = self.getLabel(graph)

        if fit.fitfunc.hasFitRange():
            # use range from first to last data point in fit range
            if graph.isEmpty():
                xmin, xmax = fit.fitfunc.getFitRange()
            else:
                xvalues = graph.getXvalues(filtered=True)
                xmin, xmax = xvalues.min(), xvalues.max()
        else:
            if graph.isEmpty():
                xmin, xmax = -10, 10
            else:
                xvalues = graph.getXvalues(filtered=False)
                xmin, xmax = xvalues.min(), xvalues.max()

        if fit.fitfunc.hasFitDrawRange():
            xmin, xmax = fit.fitfunc.getFitDrawRange()

        if smooth == False:
            fitrange = np.linspace(xmin, xmax, self.fitPoints)
            fitcurve = fit.eval(fitrange)

        else:
            # use spline interpolation for faster drawing
            if len(graph) < 50:
                rawrange = np.linspace(xmin, xmax, 50)
            else:
                rawrange = xvalues
            rawcurve = fit.eval(rawrange)

            spline = interpolate.UnivariateSpline(rawrange, rawcurve, s=0)

            fitrange = np.linspace(xmin, xmax, self.fitPoints)
            fitcurve = spline(fitrange)

        color = self.getGraphColor(graph)
        if graph.isHistogram():
            style = {
                'marker':       '',
                'color':        color,
                'lw':           1.5,
                'ls':           '-',
                #'alpha':        1.0,
            }
        else:
            style = {
                'marker':       '',
                'color':        color,
                'lw':           1,
                #'alpha':        1.0,
            }
        style['zorder'] = 80
        style = self.getStyle('fit', style, graph)

        try:
            ax.plot(fitrange, fitcurve,
                    label=label, rasterized=self.plot['rasterized'], **style)
        except Exception as e:
            exception(e, "Failed to plot fit curve!")

        self._update_limits(fitrange, fitcurve, None, None, secondary=graph.secondary)

    def plotFitBand(self, ax, graph, smooth=False):
        #if graph.isEmpty():
        #    return

        if not graph.hasFit():
            return
        fit = graph.fit

        if fit.fitfunc.hasFitRange():
            # use range from first to last data point in fit range
            if graph.isEmpty():
                xmin, xmax = fit.fitfunc.getFitRange()
            else:
                xvalues = graph.getXvalues(filtered=True)
                xmin, xmax = xvalues.min(), xvalues.max()
        else:
            if graph.isEmpty():
                xmin, xmax = -10, 10
            else:
                xvalues = graph.getXvalues(filtered=False)
                xmin, xmax = xvalues.min(), xvalues.max()

        if fit.fitfunc.hasFitDrawRange():
            xmin, xmax = fit.fitfunc.getFitDrawRange()

        fitrange = np.linspace(xmin, xmax, self.fitPoints)

        for k in range(len(self.sigma)):

            sigma = self.sigma[k]
            if sigma <= 0.:
                return

            if smooth == False:
                fiterror = fit.eval_error(fitrange, sigma=sigma)
                lower, upper = zip(*fiterror)
            else:
                # use spline interpolation for faster drawing
                if len(graph) < 50:
                    rawrange = np.linspace(xmin, xmax, 50)
                else:
                    rawrange = xvalues
                rawerror = zip(*fit.eval_error(rawrange, sigma=sigma))

                lspline = interpolate.UnivariateSpline(rawrange, rawerror[0], s=0)
                uspline = interpolate.UnivariateSpline(rawrange, rawerror[1], s=0)

                lower = lspline(fitrange)
                upper = uspline(fitrange)

            if self.sigmaColors:
                color = self.sigmaColors[k]
            else:
                color = self.getGraphColor(graph)
            style = {
                'color':        color,
                'edgecolor':    'none',
                'alpha':        0.2,
            }
            style['zorder'] = 40
            style = self.getStyle('fitband', style, graph)

            try:
                ax.fill_between(fitrange, lower, upper,
                        rasterized=self.plot['rasterized'], **style)
            except Exception as e:
                exception(e, "Failed to plot fit error band!")

            self._update_limits(fitrange, lower, None, None, secondary=graph.secondary)
            self._update_limits(fitrange, upper, None, None, secondary=graph.secondary)

    def plotResiduals(self, ax, graph, normalized=True, thinning=0):
        if graph.isEmpty():
            return

        if not graph.hasFit():
            return
        fit = graph.fit

        color = self.getGraphColor(graph)
        if graph.isHistogram():
            style = {
                'marker':       'o',
                'color':        color,
                'ms':           3,
                'lw':           1,
                'ls':           '',
                'capsize':      1,
                'ecolor':       'k',
                #'alpha':        1,
            }
        else:
            style = {
                'marker':       'o',
                'color':        color,
                'ms':           3,
                'lw':           1,
                'ls':           '',
                'capsize':      1,
                'ecolor':       'k',
                #'alpha':        1,
            }
        style['zorder'] = 100
        style = self.getStyle('residuals', style, graph)

        fit.computeResiduals(normalized=normalized)

        xvalues = graph.getXvalues(filtered=True, thinning=thinning)
        rvalues = fit.getRvalues(thinning=thinning)                     # filtered by def.

        xerrors, rerrors = None, None
        if graph.hasErrorsX():
            xerrors = graph.getXerrorsAsym(filtered=True, thinning=thinning)
        if graph.hasErrorsY():  # yerrors needed for residual errors
            rerrors = fit.getRerrorsAsym(thinning=thinning)             # filtered by def.

        try:
            ax.errorbar(xvalues, rvalues, xerr=xerrors, yerr=rerrors,
                    rasterized=self.plot['rasterized'], **style)
        except Exception as e:
            exception(e, "Failed to plot residuals!")

    def plotResidualsBand(self, ax, graph, normalized=True):
        if graph.isEmpty():
            return

        if not graph.hasFit():
            return
        fit = graph.fit

        if fit.fitfunc.hasFitRange():
            # use range from first to last data point in fit range
            if graph.isEmpty():
                xmin, xmax = fit.fitfunc.getFitRange()
            else:
                xvalues = graph.getXvalues(filtered=True)
                xmin, xmax = xvalues.min(), xvalues.max()
        else:
            if graph.isEmpty():
                xmin, xmax = -10, 10
            else:
                xvalues = graph.getXvalues(filtered=False)
                xmin, xmax = xvalues.min(), xvalues.max()

        if fit.fitfunc.hasFitDrawRange():
            xmin, xmax = fit.fitfunc.getFitDrawRange()

        if normalized:
            for k in range(len(self.sigma)):
                sigma = self.sigma[k]
                if sigma <= 0.:
                    return

                if self.sigmaColors:
                    color = self.sigmaColors[k]
                else:
                    color = self.getGraphColor(graph)
                style = {
                    'facecolor':    color,
                    'edgecolor':    'none',
                    'alpha':        0.2,
                }
                style['zorder'] = 80
                style = self.getStyle('residband', style, graph)

                try:
                    ax.axhspan(-sigma, sigma,
                            rasterized=self.plot['rasterized'], **style)
                except Exception as e:
                    exception(e, "Failed to plot residuals band!")

        else:
            fitrange = np.linspace(xmin, xmax, self.fitPoints)
            fitcurve = fit.eval(fitrange)

            for k in range(len(self.sigma)):
                sigma = self.sigma[k]
                if sigma <= 0.:
                    return

                fiterror = fit.eval_error(fitrange, sigma=sigma)
                lower, upper = zip(*fiterror)

                if self.sigmaColors:
                    color = self.sigmaColors[k]
                else:
                    color = self.getGraphColor(graph)
                style = {
                    'facecolor':    color,
                    'edgecolor':    'none',
                    'alpha':        0.2,
                }
                style['zorder'] = 80
                style = self.getStyle('residband', style, graph)

                try:
                    ax.fill_between(fitrange, lower-fitcurve, upper-fitcurve,
                            rasterized=self.plot['rasterized'], **style)
                except Exception as e:
                    exception(e, "Failed to plot residuals band!")

    def plotResidualsHist(self, ax, graph, bins=10, width=None, normalized=True):
        if graph.isEmpty():
            return

        if not graph.hasFit():
            return
        fit = graph.fit

        color = self.getGraphColor(graph)
        style = {
            'color':        color,
            'lw':           0,
            'alpha':        0.7,
            'histtype':     'stepfilled',
        }
        style['zorder'] = 100
        style = self.getStyle('residhist', style, graph)

        mu,sigma = fit.getResiduals(normalized=normalized)
        rvalues = fit.getRvalues()

        try:
            ax.hist(rvalues, bins=bins, range=width,
                    rasterized=self.plot['rasterized'], **style)
        except Exception as e:
            exception(e, "Failed to plot residuals histogram!")

    def plotResidualsProb(self, ax, graph):
        if graph.isEmpty():
            return

        if not graph.hasFit():
            return
        fit = graph.fit

        #color = self.getGraphColor(graph)
        style = {
                'dist':     'norm',
                'fit':      False,
            }
        style['zorder'] = 100
        style = self.getStyle('residprob', style, graph)

        rvalues = fit.getRvalues()

        try:
            stats.probplot(rvalues, plot=ax,
                    rasterized=self.plot['rasterized'], **style)
        except TypeError:                                               ## FIXME - workaround for issue with older versions of scipy.stats
            pass
        except Exception as e:
            exception(e, "Failed to plot residuals probability!")

    def plotIndicators(self, ax, xlines=True, ylines=True):
        style = {
            'color':        'k',
            'lw':           0.5,
            'ls':           '--',
        }
        style['zorder'] = 500
        style = self.getStyle('indicator', style)

        try:
            for values in self.indicators:
                x,y = values
                if xlines and x != None:
                    ax.axvline(x, **style)
                if ylines and y != None:
                    ax.axhline(y, **style)
        except Exception as e:
            exception(e, "Failed to plot indicators!")

    def plotGrid(self, ax):
        style = {
            'color':        '.8',
            'lw':           0.5,
            'ls':           '-',
        }
        style['zorder'] = 50
        style = self.getStyle('grid', style)

        try:
            ax.grid(**style)
        except Exception as e:
            exception(e, "Failed to plot grid!")

    def plotLegend(self, ax, position='best'):
        style = {
            'shadow':       False,
            'numpoints':    1,
            'ncol':         1 + int((len(self.graphs)-1) / 11),
        }
        style = self.getStyle('legend', style)

        try:
            self.legend = ax.legend(loc=position, **style)
            if self.legend:
                self.legend.set_zorder(1000)
        except Exception as e:
            exception(e, "Failed to plot the legend!")

    def plotDataFrame(self, ax1, ax2=None, inset=False):

        ### frame 1 - the actual plot

        # TODO broken axes - http://stackoverflow.com/questions/5656798/python-matplotlib-is-there-a-way-to-make-a-discontinuous-axis

        if inset == False:
            if ax1 != None:
                if self.xlimits:
                    ax1.set_xlim(self.xlimits)
                if self.ylimits:
                    ax1.set_ylim(self.ylimits)
            if ax2 != None:
                if self.xlimits:
                    ax2.set_xlim(self.xlimits)
                if self.ylimits2:
                    ax2.set_ylim(self.ylimits2)

        if ax1 != None:
            if self.xlogscale:
                ax1.set_xscale('log')
            if self.ylogscale:
                ax1.set_yscale('log')
        if ax2 != None:
            if self.xlogscale:
                ax2.set_xscale('log')
            if self.ylogscale:
                ax2.set_yscale('log')

        message("Plotting data points.", level=1)
        for graph in self.graphs.values():
            ax = ax2 if graph.secondary else ax1

            # plot data points
            self.plotData(ax, graph, thinning=self.plot['thinning'])

            if self.plot['errorband']:
                self.plotErrorBand(ax, graph)

        message("Plotting fit curves.", level=1)
        for graph in self.graphs.values():
            ax = ax2 if graph.secondary else ax1

            # plot fit curves
            if self.plot['fit'] and graph.hasFit():
                self.plotFit(ax, graph, smooth=self.plot['fitspline'])

                if self.plot['fitband']:
                    self.plotFitBand(ax, graph, smooth=self.plot['fitspline'])

        if ax1 != None:
            if not self.xlogscale:
                ax1.xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            if not self.ylogscale:
                ax1.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
        if ax2 != None:
            if not self.ylogscale2:
                ax2.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))

        # plot indicators
        if self.plot['indicators']:
            self.plotIndicators(ax1)

        # plot grid
        if self.plot['grid']:
            self.plotGrid(ax1)

        if inset == True:
            return

        # increase axes margins to make it look nicer (if there are no user-defined limits)
        if ax1 != None:
            xmin, xmax = ax1.get_xlim()
            ymin, ymax = ax1.get_ylim()
            self._set_limits(ax1, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
            if self.xmin < self.xmax:
                self._set_limits(ax1, xmin=self.xmin, xmax=self.xmax)
            if self.ymin < self.ymax:
                self._set_limits(ax1, ymin=self.ymin, ymax=self.ymax)

        if ax2 != None:
            xmin, xmax = ax2.get_xlim()
            ymin, ymax = ax2.get_ylim()
            self._set_limits(ax2, xmin=xmin, xmax=xmax, ymin2=ymin, ymax2=ymax)
            if self.xmin < self.xmax:
                self._set_limits(ax2, xmin=self.xmin, xmax=self.xmax)
            if self.ymin2 < self.ymax2:
                self._set_limits(ax2, ymin2=self.ymin2, ymax2=self.ymax2)

        if ax1 != None:
            if self.xticks:
                ax1.set_xticks(self.xticks)
            if self.yticks:
                ax1.set_yticks(self.yticks)
            if self.xmin < self.xmax:
                self._set_limits(ax1, xmin=self.xmin, xmax=self.xmax)
            if self.ymin < self.ymax:
                self._set_limits(ax1, ymin=self.ymin, ymax=self.ymax)

        if ax2 != None:
            if self.xticks:
                ax2.set_xticks(self.xticks)
            if self.yticks2 and ax2 != None:
                ax2.set_yticks(self.yticks2)
            if self.xmin < self.xmax:
                self._set_limits(ax2, xmin=self.xmin, xmax=self.xmax)
            if self.ymin2 < self.ymax2:
                self._set_limits(ax2, ymin2=self.ymin2, ymax2=self.ymax2)

        # plot legend
        if self.plot['legend']:
            self.plotLegend(ax1, position=self.plot['legend'])

        # plot x-axis labels (or remove labels/ticks if residuals are shown below)
        if not self.plot['residuals']:
            ax1.set_xlabel(self.xlabel)
        else:
            ax1.set_xlabel("")
            ax1.set_xticklabels([])

        # plot y-axis labels
        if ax1 != None:
            ax1.set_ylabel(self.ylabel)
        if ax2 != None:
            ax2.set_ylabel(self.ylabel2)

    def plotResidualsFrame(self, ax, axbase=None):

        ### frame 2 - residuals

        if self.xlimits:
            ax.set_xlim(self.xlimits[0], self.xlimits[1])

        if self.xlogscale:
            ax.set_xscale('log')

        message("Plotting residuals.", level=1)
        for graph in self.graphs.values():
            # plot residuals
            if graph.hasFit():
                self.plotResiduals(ax, graph, normalized=self.plot['residnorm'], thinning=self.plot['thinning'])

                if self.plot['residband']:
                    self.plotResidualsBand(ax, graph, normalized=self.plot['residnorm'])

        ax.axhline(0, color="k", lw=0.5, zorder=90)  # draw zero-residual line

        # plot grid
        if self.plot['grid']:
            self.plotGrid(ax)

        # plot indicators (x-axis only)
        if self.plot['indicators']:
            self.plotIndicators(ax, ylines=False)

        if not self.xlogscale:
            ax.xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
        ax.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))

        if self.xticks:
            ax.set_xticks(self.xticks)

        # always use x-axis range from main x-axis
        if axbase != None:
            ax.set_xticks(axbase.get_xticks())
            ax.set_xlim(axbase.get_xlim())

        # some magic to adjust y-axis range
        ax.yaxis.set_major_locator(MaxNLocator(5))
        yticks = ax.get_yticks()
        if min(yticks) < 0 and max(yticks) > 0:
            ax.set_yticks([ min(yticks), 0, max(yticks) ])
        else:
            ax.set_yticks([ min(yticks), max(yticks) ])
        ydelta = yticks[-1] - yticks[0]
        #ax.set_yticks(yticks)
        ax.set_ylim(yticks[0]-0.2*ydelta, yticks[-1]+0.2*ydelta)

        # plot x-axis labels
        ax.set_xlabel(self.xlabel)

        # plot y-axis labels
        if self.plot['residnorm']:
            label = "norm.res."
        else:
            label = "residual"

        ax.set_ylabel(label, color='0.3')
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_color('0.3')

    def plotResidualsHistFrame(self, ax, axbase=None):

        ### frame 4 - residuals histogram

        message("Plotting residuals histograms.", level=1)
        for graph in self.graphs.values():
            if graph.hasFit():
                self.plotResidualsHist(ax, graph, width=axbase.get_ylim(), normalized=self.plot['residnorm'])

        ax.axvline(0, color="k", lw=0.5, zorder=90)  # draw zero-residual line

        ax.xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
        ax.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))

        ax.yaxis.set_major_locator(MaxNLocator(4))

        # use x-axis range from residuals y-axis
        if axbase != None:
            ax.set_xlim(axbase.get_ylim())

        # some magic to adjust x-axis range
        yticks = axbase.get_yticks()
        if min(yticks) < 0 and max(yticks) > 0:
            ax.set_xticks([ min(yticks), 0, max(yticks) ])
        else:
            ax.set_xticks([ min(yticks), max(yticks) ])

        # plot x-axis labels
        ax.set_xlabel("resid.", color='0.3', size='x-small')
        ax.set_xticklabels([])
        #for tick in ax.xaxis.get_major_ticks():
        #    tick.label.set_color('0.3')

        # plot y-axis labels
        ax.set_yticklabels([])
        #for tick in ax.yaxis.get_major_ticks():
        #    tick.label.set_color('0.3')

    def plotResidualsProbFrame(self, ax, axbase=None):

        ### frame 3 - residuals probability

        message("Plotting residuals probability.", level=1)
        for graph in self.graphs.values():
            if graph.hasFit():
                self.plotResidualsProb(ax, graph)

        ax.xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
        ax.yaxis.set_major_formatter(ScalarFormatter(useOffset=False))

        ax.xaxis.set_major_locator(MaxNLocator(4))
        ax.yaxis.set_major_locator(MaxNLocator(4))

        # some magic to adjust x- and y-axis range
        xmin, xmax = ax.get_xlim()
        xdist = np.ceil( max(abs(xmin), abs(xmax)) )
        ax.set_xlim( -xdist, xdist )

        ymin, ymax = ax.get_ylim()
        ydist = np.ceil( max(abs(ymin), abs(ymax)) )
        ax.set_ylim( -ydist, ydist )

        # plot x-axis labels
        ax.set_xlabel("expected", color='0.3', size='x-small')
        ax.set_xticklabels([])
        #for tick in ax.xaxis.get_major_ticks():
        #    tick.label.set_color('0.3')

        # plot y-axis labels
        ax.set_ylabel("observed", color='0.3', size='x-small')
        ax.set_yticklabels([])
        #for tick in ax.yaxis.get_major_ticks():
        #    tick.label.set_color('0.3')

    def runCallbacks(self):
        message("Running callbacks.", level=1)
        for graph in self.graphs.values():
            # run callbacks before plotting anything
            graph.runCallbacks()

    def build(self, fig=None, twinx=False):

        if fig == None:
            fig = plt.gcf()

        def _axes(x,y,w,h):
            if x < 0:
                x = self.width + x
            if y < 0:
                y = self.height + y
            if w <= 0:
                w = self.width + w
            if h <= 0:
                h = self.height + h

            ax = fig.add_axes(( self.lmargin+x, self.bmargin+y, w, h ))
            ax.xaxis.set_zorder(1000)
            ax.yaxis.set_zorder(1000)

            return ax

        # do not show residuals if there are no fits to be plotted
        if self.plot['residuals']:
            show = False
            for graph in self.graphs.values():
                if graph.fit != None:
                    show = True
            self.plot['residuals'] = show

        # create layout with frames
        self.pltFrames = { 'primary': None, 'secondary': None, 'residual': None, 'probplot': None, 'histogram': None }

        self.width  = 1. - (self.lmargin + self.rmargin)
        self.height = 1. - (self.bmargin + self.tmargin)

        if self.plot['residuals']:
            self.rheight = 1. / self.plot['residratio']

            if self.plot['residprob'] or self.plot['residhist']:
                self.pltFrames['primary'] = _axes(  0.,           self.rheight,      -self.rheight-0.05, -self.rheight )  # main plot
                self.pltFrames['residual']  =  _axes(  0.,           0.,                -self.rheight-0.05,  self.rheight )  # residuals
                if self.plot['residprob']:
                    self.pltFrames['probplot'] = _axes( -self.rheight, self.rheight+0.05,  self.rheight,       self.rheight )  # residuals prob
                if self.plot['residhist']:
                    self.pltFrames['histogram'] = _axes( -self.rheight, 0. ,                self.rheight,       self.rheight )  # residuals hist

            else:
                self.pltFrames['primary'] = _axes(  0.,  self.rheight, 0., -self.rheight )  # main plot
                self.pltFrames['residual']  = _axes(  0.,  0.,           0.,  self.rheight )  # residuals
        else:
            self.rheight = 0.

            self.pltFrames['primary'] = _axes(  0., 0., 0., 0. )  # main plot

        # add second y-axis if needed
        if twinx:
            self.pltFrames['secondary'] = self.pltFrames['primary'].twinx()

        # adjust label positions
        if self.plot['residuals']:
            #self.pltFrames['primary'].xaxis.set_label_coords(self.lmargin+0.5*self.width, self.bmargin-self.vspace, transform=fig.transFigure)
            self.pltFrames['primary'].yaxis.set_label_coords(self.lmargin-self.hspace, self.bmargin+self.rheight+0.5*(self.height-self.rheight), transform=fig.transFigure)
            if self.pltFrames['secondary'] != None:
                self.pltFrames['secondary'].yaxis.set_label_coords(1.-(self.rmargin-self.hspace), self.bmargin+self.rheight+0.5*(self.height-self.rheight), transform=fig.transFigure)

            self.pltFrames['residual'].xaxis.set_label_coords(self.lmargin+0.5*self.width, self.bmargin-self.vspace, transform=fig.transFigure)
            self.pltFrames['residual'].yaxis.set_label_coords(self.lmargin-self.hspace, self.bmargin+0.5*self.rheight, transform=fig.transFigure)
        else:
            self.pltFrames['primary'].xaxis.set_label_coords(self.lmargin+0.5*self.width, self.bmargin-self.vspace, transform=fig.transFigure)
            self.pltFrames['primary'].yaxis.set_label_coords(self.lmargin-self.hspace, self.bmargin+0.5*self.height, transform=fig.transFigure)
            if self.pltFrames['secondary'] != None:
                self.pltFrames['secondary'].yaxis.set_label_coords(1.-(self.rmargin-self.hspace), self.bmargin+0.5*self.height, transform=fig.transFigure)

        return self.pltFrames

    def plotAll(self, fig=None):

        # check if second y-axis is needed
        twinx = False
        for g in self.graphs.values():
            if g.secondary:
                twinx = True
                break

        self.build(fig, twinx=twinx)

        # plot data and fit
        self.plotDataFrame(self.pltFrames['primary'], self.pltFrames['secondary'])

        # plot residuals and histogram
        if self.plot['residuals']:
            self.plotResidualsFrame(self.pltFrames['residual'], self.pltFrames['primary'])

            if self.plot['residprob']:
                self.plotResidualsProbFrame(self.pltFrames['probplot'], self.pltFrames['primary'])

            if self.plot['residhist']:
                self.plotResidualsHistFrame(self.pltFrames['histogram'], self.pltFrames['primary'])

    def plotInset(self, fig=None, position=(0.1, 0.1, 0.3, 0.3),
            xlabel=None, ylabel=None, ylabel2=None,
            xlimits=None, ylimits=None, ylimits2=None,
            xticks=None, yticks=None, yticks2=None):

        if fig == None:
            fig = plt.gcf()

        if not 'inset' in self.pltFrames:
            self.pltFrames['inset'] = []

        ax1 = fig.add_axes(position)
        ax2 = None
        if self.pltFrames['secondary']:
            ax2 = ax1.twinx()

        self.pltFrames['inset'].append({
            'primary':      ax1,
            'secondary':    ax2,
        })

        self.plotDataFrame(ax1, ax2, inset=True)

        ax1.xaxis.set_major_locator(MaxNLocator(6))
        ax1.yaxis.set_major_locator(MaxNLocator(4))
        if ax2 != None:
            ax2.yaxis.set_major_locator(MaxNLocator(4))

        if xlimits:
            ax1.set_xlim(xlimits)
        if ylimits:
            ax1.set_ylim(ylimits)
        if ylimits2 and ax2 != None:
            ax2.set_ylim(ylimits2)

        if xlabel != None:
            ax1.set_xlabel(xlabel)
        if ylabel != None:
            ax1.set_ylabel(ylabel)
        if ylabel2 != None and ax2 != None:
            ax2.set_ylabel(ylabel2)

        if xticks != None:
            ax1.set_xticks(xticks)
        if yticks != None:
            ax1.set_yticks(yticks)
        if yticks2 != None and ax2 != None:
            ax2.set_yticks(yticks2)

        ax1.xaxis.set_zorder(1000)
        ax1.yaxis.set_zorder(1000)
        if ax2 != None:
            ax2.xaxis.set_zorder(1000)
            ax2.yaxis.set_zorder(1000)

        return self.pltFrames['inset']

    def savePlot(self, savename, savelabel="", filetype='both', fig=None):
        if fig == None:
            fig = plt.gcf()

        def savefig(fig, filename, **kwargs):
            # make sure output directories exist
            try:
                path = os.path.dirname(filename)
                os.makedirs(path)
            except:
                pass

            fig.savefig(filename, **kwargs)

        output = savename
        if savelabel:
            output += '_' + savelabel

        message("Saving plot contents.", level=1)
        if filetype.lower() in [ 'both', 'png' ]:
            savefig(fig, self.outputPath.format(output, 'png'), dpi=150)
        if filetype.lower() in [ 'both', 'pdf' ]:
            savefig(fig, self.outputPath.format(output, 'pdf'), dpi=300)

    def finalize(self, fig=None, savename=None, savelabel="", filetype='both', preview=False):

        if fig == None:
            fig = plt.gcf()

        # show title
        if self.title != None:
            fig.suptitle(self.title)

        # save plot to file
        if savename != None:
            self.savePlot(savename, savelabel, filetype=filetype, fig=fig )

        # show plot preview
        if preview == True:
            plt.show()

    def generate(self, savename=None, savelabel="", filetype='both', preview=False, figsize=None):
        """
        Generate a plot from the graphs which were added before.

        If ``savelabel`` is defined, it will be appended to the
        ``savename`` with an underscore in between. By default, output
        files in PNG and PDF format are generated.

        Args:
            savename (string):  Base filename for output file(s).
            savelabel (string): Appendix for base filename, not used if empty.
            filetype (string):  Choose `pdf`, `png` or `both` to select the type of the output file.
            preview (bool):     If true, plot will be shown in a Matplotlib window after(!) saving.
        """
        self.runCallbacks()

        plt.close('all')
        fig = plt.figure(figsize=figsize)

        self.pltFigure = fig

        self.plotAll(fig)

        self.finalize(fig=fig, savename=savename, savelabel=savelabel, filetype=filetype, preview=preview)

        return fig

########################################################################
