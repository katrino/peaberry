#!/usr/bin/env python
# -*- coding: utf8 -*-

from __future__ import unicode_literals, print_function
from collections import OrderedDict

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator, ScalarFormatter
from scipy import stats
from scipy import optimize
from scipy import integrate

import os
import copy
import json
from decimal import Decimal

from Peaberry.Main import message, exception
import Peaberry.Main as Main

########################################################################
########################################################################

def thinned(values, thinning=0):
    """
    Return a thinned list of values (for plotting with a reduced number of points).
    """
    if values.size == 0:
        return values.copy()  # empty array

    if thinning <= 1:
        return values.copy()

    return values.T[::thinning].T  # numpy magic ;)

class FitParam(object):
    """
    Basic class which represents a fit parameter.

    The class holds the parameter name, its value, its errors and its
    limits. There are also a number of flags available which are used
    by other classes that work with fit parameters.
    """
    def __init__(self, name, value=0., errors=(0,0), limits=(-np.inf,np.inf), hidden=False, fixed=False, unit="", alias=""):
        self.name   = name
        self.unit   = unit
        self.alias  = alias
        self.hidden = hidden
        self.fixed  = fixed
        self.value  = value
        self.limits = limits
        self.errors = errors
        self.covar  = OrderedDict()

    def __str__(self):
        return "fit parameter {0}: {1} {2}".format(self.name, self.value, self.unit)

    def isHidden(self):
        return self.hidden

    def isFixed(self):
        return self.fixed

    def hasLimits(self):
        return (self.limits[0] > -np.inf or self.limits[1] < np.inf)

    def getName(self):
        return self.name

    def getUnit(self):
        return self.unit

    def setUnit(self, unit=""):
        self.unit = unit

    def getAlias(self):
        return self.alias

    def setAlias(self, alias=""):
        self.alias = alias

    def setHidden(self, hidden=True):
        self.hidden = hidden

    def setFixed(self, fixed=True):
        self.fixed = fixed

    def setValue(self, value):
        self.value = value

    def setErrors(self, errors):
        self.errors = (errors[0], errors[1])

    def setLimits(self, limits):
        self.limits = (limits[0], limits[1])

    def setCovariance(self, other, value):
        self.covar[other.getName()] = value
        other.covar[self.getName()] = value

    def getValue(self):
        return self.value

    def getErrorDown(self):
        return self.errors[0]

    def getErrorUp(self):
        return self.errors[1]

    def getErrorMean(self):
        if self.errors[0] == self.errors[1]:
            return self.errors[0]
        return np.sqrt( 0.5 * np.square(self.errors).sum() )

    def getErrors(self):
        return self.errors[:]

    def getValueAndErrors(self):
        return ( self.value, self.errors[:] )

    def getLimitLow(self):
        return self.limits[0]

    def getLimitHigh(self):
        return self.limits[1]

    def getLimits(self):
        return self.limits[:]

    def getCovariance(self, other):
        if other.getName() in self.covar:
            return self.covar[other.getName()]
        return 0.

    def getCorrelation(self, other):
        cov = self.getCovariance(other)
        err = ( self.getErrors()[0] * other.getErrors()[0], self.getErrors()[1] * other.getErrors()[1] )
        if err[0] > 0. and err[1] > 0.:
            return ( cov / err[0], cov / err[1] )
        return ( 0., 0. )

    def getCorrelationDown(self, other):
        return self.getCorrelation(other)[0]

    def getCorrelationUp(self, other):
        return self.getCorrelation(other)[1]

    def getCorrelationMean(self, other):
        cov = self.getCovariance(other)
        err = self.getErrorMean() * other.getErrorMean()
        if err > 0.:
            return cov / err

        return 0.

########################################################################

class FitFunction(object):
    """
    Basic class which resembles a fit function.

    The class holds all fit parameters, their erros, their limits for
    the fit, and a list of parameter names. The fit function itself is
    defined as a static method ``fit`` which is called by the fit
    routine. Another static method ``_err`` can be used to compute
    the resulting error by applying error propagation to the fit model.

    The actual implementation of different fit models with the ``fit``
    and ``_err`` functions is done in the ``Fits`` module.
    """
    def __init__(self, parnames=[]):
        self.fitParams    = OrderedDict([ (p, FitParam(p)) for p in parnames ])
        self.fitRange     = (-np.inf,np.inf)
        self.fitDrawRange = (-np.inf,np.inf)

        self.scale        = 1.

    def __len__(self):
        """
        Return the number of free fit parameters.

        Returns:
            int: Number of fit parameters.
        """
        return len(self.fitParams)

    def _className(self):
        return self.__class__.__name__

    def min(self, *popt):
        return -np.inf

    def max(self, *popt):
        return np.inf

    def fit(self, x, *popt):
        # popt = [ p1, p2, ... ]
        return np.zeros(x.size)

    def err(self, x, *popt):
        # popt = [ [ p1, e1 ], [ p2, e2 ], ... ]
        return np.zeros((x.size, 2))

    def int(self, xmin, xmax, *popt):
        # popt = [ p1, p2, ... ]
        return 0.

    def range(self, x, popt=[]):
        """
        Return the support of the fit function.
        """
        if not len(popt):
            popt = self.getFitValues().values()

        return ( self.min(*popt), self.max(*popt) )

    def eval(self, x, popt=[]):
        """
        Evaluate the fit function at a given point.
        """
        if not len(popt):
            popt = self.getFitValues().values()

        if np.ndim(x) == 0:
            values = np.array([x])
        else:
            values = x

        result = self.scale * self.fit(values, *popt)

        if np.ndim(x) == 0:
            return result[0]
        return result

    def eval_error(self, x, sigma=1., popt=[], perr=[], use_covar=False):
        """
        Evaluate the fit function at a given point.
        """
        if not len(popt):
            popt = self.getFitValues().values()
        if not len(perr):
            perr = self.getFitErrors().values()

        if np.ndim(x) == 0:
            values = np.array([x])
        else:
            values = x

        # use min/max error variation to compute errors
        fitcurve = self.eval(values, popt)
        lower = fitcurve.copy()
        upper = fitcurve.copy()
        for pkey,p in self.getFitParams().items():
            # variate downwards
            if p.getErrorDown() > 0.:
                params = self.getFitValues().copy()
                if use_covar:
                    for qkey,q in self.getFitParams().items():
                        if pkey == qkey:
                            params[qkey] = np.clip(self.getFitValues()[qkey] - sigma * p.getErrorDown(), q.getLimitLow(), q.getLimitHigh())
                        else:
                            params[qkey] = np.clip(self.getFitValues()[qkey] - sigma * p.getErrorDown() * p.getCovariance(q), q.getLimitLow(), q.getLimitHigh())
                else:
                    params[pkey] = np.clip(self.getFitValues()[pkey] - sigma * p.getErrorDown(), p.getLimitLow(), p.getLimitHigh())
                curve = self.eval(values, params.values())
                lower, upper = np.fmin(lower, curve), np.fmax(upper, curve)
            # variate upwards
            if p.getErrorUp() > 0.:
                params = self.getFitValues().copy()
                if use_covar:
                    for qkey,q in self.getFitParams().items():
                        if pkey == qkey:
                            params[qkey] = np.clip(self.getFitValues()[qkey] + sigma * p.getErrorUp(), q.getLimitLow(), q.getLimitHigh())
                        else:
                            params[qkey] = np.clip(self.getFitValues()[qkey] + sigma * p.getErrorUp() * p.getCovariance(q), q.getLimitLow(), q.getLimitHigh())
                else:
                    params[pkey] = np.clip(self.getFitValues()[pkey] + sigma * p.getErrorUp(), p.getLimitLow(), p.getLimitHigh())
                curve = self.eval(values, params.values())
                lower, upper = np.fmin(lower, curve), np.fmax(upper, curve)
        result = zip(lower, upper)

        if np.ndim(x) == 0:
            return result[0]
        return result

    def integrate(self, xmin=None, xmax=None, popt=[], numerical=True):
        """
        Evaluate the integral of the fit function between the given
        bounds.
        """
        if not len(popt):
            popt = self.getFitValues().values()

        if xmin == None:
            xmin = self.min(*popt)
        if xmax == None:
            xmax = self.max(*popt)

        result = None
        if numerical:
            result, error = integrate.quad(lambda x: self.fit(np.array([x]), *popt), xmin, xmax)
        else:
            result = self.int(xmin, xmax, *popt)

        return result

    def guess(self, graph):
        """
        Guess the fit parameters from a given data set.
        """
        return self.fitParams

    def setScale(self, scale=1.):
        self.scale = scale

    def getExtraResults(self):
        """
        Print additional output.

        This function is called when the main fit results are printed.
        """
        pass

    def getParNames(self):
        """
        Get the parameter names as a list.

        Returns:
            list: A list of parameter names.
        """
        return self.fitParams.keys()

    def setHiddenParams(self, params=[]):
        """
        Set the given parameters to be hidden.

        Hidden parameters are not shown in the ``getLabel()`` result,
        which is used in the the ``str()`` function and to print the fit
        parameters in the plot legend.

        Args:
            params (list): A list of parameter names.
        """
        for p in self.fitParams.values():
            p.setHidden(False)

        for key in params:
            if not key in self.getParNames():
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            self.fitParams[key].setHidden(True)

    def setParamUnits(self, units={}):
        """
        Set units to the given parameters.

        The units are appended to the ``getLabel()`` result,
        which is used in the the ``str()`` function and to print the fit
        parameters in the plot legend.

        Args:
            units (dict): A dictionary of the fit units, like ``{ 'name': 'unit' }``.
        """
        for key, val in units.items():
            if not key in self.getParNames():
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            self.fitParams[key].unit = val

    def setParamAliases(self, aliases={}):
        """
        Set aliases for the given parameter names.

        The aliases are uses in the ``getLabel()`` result,
        which is used in the the ``str()`` function and to print the fit
        parameters in the plot legend.

        It is possible to use TeX command enclosed in ``r"$ $"'' structures.

        Args:
            aliases (dict): A dictionary of the fit aliases, like ``{ 'name': 'alias' }``.
        """
        for key, val in aliases.items():
            if not key in self.getParNames():
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            self.fitParams[key].alias = val

    def hideParams(self):
        """
        Set all parameters to be hidden.

        Hidden parameters are not shown in the ``getLabel()`` result,
        which is used in the the ``str()`` function and to print the fit
        parameters in the plot legend.
        """
        for p in self.fitParams.values():
            p.setHidden(True)

    def setFixedParams(self, params=[]):
        """
        Set the given parameters to be fixed.

        Fixed parameters are not varied when a fit is applied.
        This does not work with the standard curve_fit method.

        Args:
            params (list): A list of parameter names.
        """
        for p in self.fitParams.values():
            p.setFixed(False)

        for key in params:
            if not key in self.getParNames():
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            self.fitParams[key].setFixed(True)

    def getFixedParams(self):
        """
        Get the fixed parameters.

        Return:
            list: A list of parameter names.
        """
        params = []
        for p in self.fitParams.values():
            if p.isFixed():
                params.append(p.getName())

        return params

    def setFitValues(self, values, ignore=False):
        """
        Set the fit values to the given values.

        These values are used as initial values for the fit method.
        The parameters are given as a dictionary of values, using the
        parameter names as key. If a key that does not match a parameter
        name of the fit object, a ``KeyError`` is thrown.

        Args:
            values (dict): A dictionary of the fit parameters, like ``{ 'name': value }``.
        """
        for key,val in values.items():
            if not key in self.getParNames():
                if ignore:
                    continue
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            self.fitParams[key].setValue(val)

    def resetFitValues(self):
        """
        Reset all fit parameters.
        """
        for p in self.fitParams.values():
            p.setValue(0.)

    def getFitValues(self):
        """
        Return the fit values as a dictionary.

        Returns:
            dict: A dictionary of the fit values, like ``{ 'name': value }``.
        """
        result = OrderedDict()
        for key, p in self.fitParams.items():
            result[key] = p.getValue()

        return result

    def setFitErrors(self, errors, ignore=False):
        """
        Set the fit errors to the given values.

        The parameters are given as a dictionary of values, using the
        parameter names as key. If a key that does not match a parameter
        name of the fit object, a ``KeyError`` is thrown.

        Args:
            errors (dict): A dictionary of the fit parameters, like ``{ 'name': (low, high) }``.
        """
        for key,val in errors.items():
            if not key in self.getParNames():
                if ignore:
                    continue
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            if len(val) != 2:
                raise ValueError("{0}: <{1}> should be a tuple of two values".format(self._className(), val))
            self.fitParams[key].setErrors(val)

    def resetFitErrors(self):
        """
        Reset all fit errors.
        """
        for p in self.fitParams.values():
            p.setErrors((0., 0.))

    def getFitErrors(self):
        """
        Return the fit errors as a dictionary.

        Returns:
            dict: A dictionary of the fit results, like ``{ 'name': (low, high) }``.
        """
        result = OrderedDict()
        for key, p in self.fitParams.items():
            result[key] = p.getErrors()

        return result

    def setFitParams(self, params, ignore=False):
        """
        Set the fit parameters to the given values.

        These parameters are used as initial values for the fit method.
        The parameters are given as a dictionary of values, using the
        parameter names as key. If a key that does not match a parameter
        name of the fit object, a ``KeyError`` is thrown.

        Args:
            params (dict): A dictionary of the fit parameters, like ``{ 'name': <FitParam object> }``.
        """
        for key,p in params.items():
            if not key in self.getParNames():
                if ignore:
                    continue
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            self.fitParams[key] = p

    def resetFitParams(self):
        """
        Reset all fit parameters.
        """
        for p in self.fitParams.values():
            p.setValue(0.)
            p.setErrors((0., 0.))

    def getFitParams(self):
        """
        Return the fit results as a dictionary.

        Returns:
            dict: A dictionary of the fit results, like ``{ 'name': <FitParam object> }``.
        """
        return self.fitParams

    def setFitLimits(self, limits, ignore=False):
        """
        Set the fit limits to the given values (use ``None`` to indicate no limit).

        These parameters are used as parameter constrains for the fit method.
        The parameters are given as a dictionary of values, using the
        parameter names as key. If a key that does not match a parameter
        name of the fit object, a ``KeyError`` is thrown.

        Args:
            limits (dict): A dictionary of the fit limits, like ``{ 'name': (lower, upper) }``.
        """
        for key,val in limits.items():
            if not key in self.getParNames():
                if ignore:
                    continue
                raise KeyError("{0}: <{1}> is not a fit parameter".format(self._className(), key))
            if len(val) != 2:
                raise ValueError("{0}: <{1}> should be a tuple of format ( l1, l2 )".format(self._className(), val))
            self.fitParams[key].setLimits(val)

    def resetFitLimits(self):
        """
        Reset all fit limits.
        """
        for p in self.fitParams.values():
            p.setLimits((-np.inf, np.inf))

    def getFitLimits(self):
        """
        Return the fit limits as a dictionary.

        Returns:
            dict: A dictionary of the fit limits, like ``{ 'name': (lower, upper) }``.
        """
        result = OrderedDict()
        for key, p in self.fitParams.items():
            result[key] = p.getLimits()

        return result

    def getFitCovariances(self):
        """
        Return the fit covariances as a dictionary.

        Returns:
            dict: A dictionary of the fit limits, like ``{ 'name': { 'othername': covar } }``.
        """
        result = OrderedDict()
        for key, p in self.fitParams.items():
            result[key] = OrderedDict()
            for okey, op in self.fitParams.items():
                result[key][okey] = p.getCovariance(op)

        return result

    def hasFitRange(self):
        return ( self.fitRange[0] > -np.inf or self.fitRange[1] < np.inf )

    def hasFitDrawRange(self):
        return ( self.fitDrawRange[0] > -np.inf or self.fitDrawRange[1] < np.inf )

    def getFitRange(self):
        return self.fitRange[:]

    def getFitDrawRange(self):
        return self.fitDrawRange[:]

    def setFitRange(self, begin=-np.inf, end=np.inf):
        """
        Set the fit range to the given values (use ``None`` to use all data points).

        The fit range can be used to only include a selection of data
        points in the fit.

        Args:
            begin (float): The lower bound of the fit range.
            end (float): The upper bound of the fit range.
        """
        self.fitRange = (begin, end)

    def setFitDrawRange(self, begin=-np.inf, end=np.inf):
        """
        Set the fit draw range to the given values (use ``None`` to use fit range).

        The fit draw range can be used to limit the range where the
        fit function is drawn. This does not affect the selection of
        data points that are used for fitting.

        Args:
            begin (float): The lower bound of the fit draw range.
            end (float): The upper bound of the fit draw range.

        See also:
            :py:func:`Peaberry.Fit.setFitRange` used to set the fit range.
        """
        self.fitDrawRange = (begin, end)

########################################################################
########################################################################

class Fit(object):
    """
    Basic class for fitting data points.

    This basic implementation use the ``curve_fit`` method from
    ``scipy.optimize``, which will apply a least-squares fit to
    a number of data points defined in a graph or histogram object.
    The graph or histogram has to be associated with the fit object.

    The initializer sets the number of free fit parameters (based
    on the list of parameter names) and sets their start values to
    zero, their limits to ``None`` (i.e. no limits).

    Args:
        graph (instance): A graph/histogram to which the fit associates.
        parnames (list): A list of parameter names that also defines
            the number of free parameters for the fit function.

    Raises:
        TypeError: If ``graph`` is not an instance of ``Peaberry.Base.Graph``.

    See also:
        :py:class:`Peaberry.Base.Graph` is the base class for all graphs
        to which a fit can be applied.
    """
    def __init__(self, fitfunc, graph=None):
        if graph != None and not isinstance(graph, Graph):
            raise TypeError

        self.fitfunc      = fitfunc
        self.graph        = graph
        self.rvalues      = np.array([])
        self.rerrorsLo    = np.array([])
        self.rerrorsHi    = np.array([])

        self.useYerrors   = True
        self.useXerrors   = False

        self.status       = -1  # status = before fit

        self.scale        = 1.

    def __len__(self):
        """
        Return the number of free fit parameters.

        Returns:
            int: Number of fit parameters.
        """
        return len(self.fitfunc)

    def __str__(self):
        """
        Return a string representing the fit, including parameter values.

        The ``label()`` method is used to show the parameters.

        Returns:
            string: The string representation.
        """
        result = u"fit of order {0}: {1}".format(len(self), self.getLabel(delimiter=" "))
        return result

    def _className(self):
        return self.__class__.__name__

    @staticmethod
    def _adjust_digits(value, digits=0):
        quant = "0"
        if digits < 0:
            quant += ".{0}1".format('0' * (-digits-1))
        try:
            return value.quantize(Decimal(quant))
        except:
            return value

    @staticmethod
    def _format_error(param, extend=0, texmode=False):
        value = Decimal(float(param.getValue()))
        error = Decimal(float(param.getErrorMean()))

        def adjust(value, error):
            vdigits = value.adjusted()
            edigits = error.adjusted()
            value   = Fit._adjust_digits(value, edigits)
            error   = Fit._adjust_digits(error, edigits)
            return (value, error)

        value, error = adjust(value, error)
        if str(value)[-1] == str(error)[-1] == "0":
            # must be run again to account for "lost" digits after first adjustment
            value, error = adjust(value, error)

        if texmode:
            return r"%s \pm %s" % (value, error)
        else:
            return u"%s±%s" % (value, error)

    @staticmethod
    def _derivative(x, y):

        dx = np.gradient(x)
        dy = np.gradient(y)

        d = dy / dx

        #dx  = np.diff(x)
        #dx1 = np.hstack(( 0, dx ))
        #dx2 = np.hstack(( dx, 0 ))
        #z1 = np.hstack(( y[0], y[:-1] ))
        #z2 = np.hstack(( y[1:], y[-1] ))

        #d = (z2 - z1) / (dx2 + dx1)

        return d

    def _residuals(self, p, x, y, xerr=None, yerr=None):

        model = self.fitfunc.fit(x, *p)
        delta = y - model

        if yerr == None:
            sigma = model
        else:
            sigma = yerr
            # x-errors are not supported in SciPy.curve_fit

        return (delta, sigma)

    def _chisquare(self, p, x, y, xerr=None, yerr=None):

        try:
            delta, sigma = self._residuals(p, x, y, xerr, yerr)
        except AssertionError:
            return np.full(x.size, np.inf)

        return np.square(delta / sigma)

    def useErrors(self, xerr=False, yerr=True):
        """
        Set the computation mode for chi-square result.

        By default, chi-square only include the given y-errors.
        Optionally, the given x-errors can be transformed by
        the gradient of the function to be included in the computation
        as well.

        Args:
            xerr (bool): If `True`, include x-errors in the fit.
            yerr (bool): If `True`, include y-errors in the fit.
        """
        self.useXerrors = xerr
        self.useYerrors = yerr

    def getDimension(self):
        """
        Return the dimension of the fit (number of fit parameters).
        """
        return len(self.fitfunc)

    def getSize(self):
        """
        Return the size of the fit (number of data points).
        """
        return len(self.graph)

    def getRvalues(self, thinning=0):
        """
        Retrieve residual-values in this fit's graph.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        if len(self.rvalues) == 0:
            self.getResiduals()

        return thinned(self.rvalues, thinning=thinning)

    def getRerrors(self, thinning=0):
        """
        Retrieve residual-errors in this fit's graph.
        If the graph has asymmetric errors, the mean value is returned.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        rerrorsLo, rerrorsHi = self.getRerrorsAsym(thinning=thinning)
        return np.sqrt( 0.5 * (np.square(rerrorsLo) + np.square(rerrorsHi)) )

    def getRerrorsAsym(self, thinning=0):
        """
        Retrieve residual-errors in this fit's graph.
        This function always returns two arrays even if the errors are symmetric.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        return ( thinned(self.rerrorsLo, thinning=thinning),\
                 thinned(self.rerrorsHi, thinning=thinning) )

    def setGraph(self, graph):
        """
        Associate a graph/histogram with this fit.

        Args:
            graph (instance): A graph/histogram to which the fit associates.

        See also:
            :py:class:`Peaberry.Base.Graph` is the base class for all graphs
            to which a fit can be applied.
        """
        self.graph = graph

    def printResults(self):
        """
        Print the fit result (chi^2 etc.) and the list of parameters (values and errors).
        """
        chi2, ndf, prob = self.getChiSquare()

        topline = u"order=%d ndf=%d chi2=%.3f chi2/ndf=%.3f p=%.6g" % \
                    (len(self), ndf, chi2, chi2/ndf if ndf > 0 else 0., prob)
        linewidth = max(len(topline), 72)

        message(topline, level=3, bold=True)

        # parameter values
        message("=" * linewidth, level=3)
        message(u"%-12s    %12s %12s" % ("", "value", "error"), level=3, bold=True)
        for p in self.fitfunc.getFitParams().values():
            message(u"%-12s    %12.6g %12.6g" % (p.getName(), p.getValue(), p.getErrorMean()), level=3)

        # extra values (for specialized fit functions)
        extra = self.fitfunc.getExtraResults()
        if extra:
            message("-" * linewidth, level=3)
            for p in extra.values():
                message(u"%-12s    %12.6g %12.6g" % (p.getName(), p.getValue(), p.getErrorMean()), level=3)

        message("-" * linewidth, level=3)

    def getLabel(self, delimiter=u"\n", **kwargs):
        """
        Label function used to show fit results in a plot legend.

        The label includes all (non-hidden, see ``setHiddenParams()``)
        parameters and their values.

        Args:
            **kwargs: Passed on to the ``_format_error`` function.

        Returns:
            string: A string representing the fit results.
        """
        result = []
        for p in self.fitfunc.getFitParams().values():
            if p.isHidden():
                continue

            name = p.getName()
            unit = p.getUnit()

            texmode = False
            if p.getAlias():
                name = p.getAlias()
                if name.startswith('$') and name.endswith('$'):
                    texmode = True

            if p.getErrorMean() == 0.:
                param = p.getValue()
            else:
                param = self._format_error(p, texmode=texmode, **kwargs)

            if texmode:
                name = "".join(name.split('$'))
                label = "$" + name + " = " + param
                if unit:
                    label += " " + unit
                label += "$"
            else:
                label = u"{}={}".format(name, param)
                if unit:
                    label += u" {}".format(unit)

            result.append(label)

        return delimiter.join(result)

    def getChiSquare(self, popt=[]):
        """
        Compute the fit result (chi^2 etc.) using the given fit parameters.

        If no fit parameters are defined, the current parameter set
        of the fit is used. This method returns the chi^2 value,
        the degrees of freedom, and the fit probability.
        The computation is based on the corresponding ROOT function.

        Args:
            popt (list): Parameters to be used in the calculation,\
                if not using the current values.

        Returns:
            (chi2, ndf, prob) (tuple): chi-square, degrees of freedom, and fit probability.
        """
        if not len(popt):
            popt = self.fitfunc.getFitValues().values()

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        ndim = len(self.fitfunc)
        ndf = yvalues.size - ndim
        if ndf <= 0:
            return (0, ndf, 0.)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            yerrors  = self.graph.getYerrors(filtered=True)

        xerrors = None
        if self.useXerrors and self.graph.hasErrorsX():
            xerrors  = self.graph.getXerrors(filtered=True)

        chi2 = self._chisquare(popt, xvalues, yvalues, xerr=xerrors, yerr=yerrors).sum()
        prob = stats.chi2.sf(chi2, ndf)  # better than 1 - cdf(chi2, ndf)

        return (chi2, ndf, prob)

    def getResidualSpread(self):
        """
        Get the mean and sigma of the fit residuals.

        Note:
            The residuals must be computed beforehand with
            ``py:func:Base.Fit.computeResiduals``.

        Returns:
            (mean, width) (tuple): The mean and std.dev. of the residuals.
        """
        N    = self.rvalues.size
        mean = self.rvalues.mean()

        if N < 1:
            return (mean, 0.)

        #width = np.sqrt( 1./(N-1) * np.square(self.rvalues - mean).sum() )
        width = self.rvalues.std()

        return (mean, width)

    def computeResiduals(self, popt=[], normalized=True):
        """
        Compute the residuals of the fit (difference between data and model).

        If no fit parameters are defined, the current parameter set
        of the fit is used. This method computes the residuals for each
        data point in the fit range, and returns the mean and standard
        deviation of the combined residuals.

        Args:
            popt (list): Parameters to be used in the calculation,
                if not using the current values.
            normalized (bool): If `True`, the residuals will be normalized
                to their corresponding error bars.
        """
        if not len(popt):
            popt = self.fitfunc.getFitValues().values()

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            yerrors = self.graph.getYerrors(filtered=True)

        xerrors = None
        if self.useXerrors and self.graph.hasErrorsX():
            xerrors  = self.graph.getXerrors(filtered=True)

        delta, sigma = self._residuals(popt, xvalues, yvalues, xerr=xerrors, yerr=yerrors)

        self.rvalues = delta
        if self.graph.hasErrorsY():
            yerrorsLo, yerrorsHi = self.graph.getYerrorsAsym(filtered=True)
            self.rerrorsLo = yerrorsLo.copy()
            self.rerrorsHi = yerrorsHi.copy()

            if normalized:
                self.rvalues   /= yerrors
                self.rerrorsLo /= yerrors
                self.rerrorsHi /= yerrors

    def eval(self, x, popt=[]):
        """
        Evaluate the fit function at a given point.
        """
        return self.fitfunc.eval(x, popt)

    def eval_error(self, x, sigma=1., popt=[], perr=[]):
        """
        Evaluate the fit error at a given point.
        """
        return self.fitfunc.eval_error(x, sigma, popt, perr)

    def save(self, filename):
        """
        Export this fit's results to a JSON file.

        Args:
            filename (string): The filename of the output file.
        """
        params = self.fitfunc.getParNames()
        values = self.fitfunc.getFitValues()
        errors = self.fitfunc.getFitErrors()
        limits = self.fitfunc.getFitLimits()
        covars = self.fitfunc.getFitCovariances()

        try:
            chi2, ndf, prob = self.getChiSquare()
        except:
            chi2, ndf, prob = None, None, None

        try:
            loglike = self.getLikelihood()
        except:
            loglike = None

        with open(filename, 'w') as f:
            result = OrderedDict()
            result['name']     = str(self._className())
            result['func']     = str(self.fitfunc._className())
            result['range']    = self.fitfunc.fitRange
            result['size']     = self.getSize()
            result['dim']      = self.getDimension()
            result['chi2']     = chi2
            result['chi2/ndf'] = chi2 / ndf
            result['ndf']      = ndf
            result['prob']     = prob
            result['loglike']  = loglike

            result['params']   = OrderedDict()
            for key in params:
                item = OrderedDict()
                item['value']  = values[key]
                item['errors'] = errors[key]
                item['limits'] = limits[key]
                item['covar']  = covars[key]
                result['params'][key] = item

            json.dump(result, f, indent=4, sort_keys=False)  # already sorted

    def load(self, filename, exclude=[]):
        """
        Import fit results from a JSON file.

        Args:
            filename (string): The filename of the input file.
            exclude (list): List of parameter names to keep instead of
                    overriding them with results from the input file.
        """
        with open(filename, 'r') as f:
            result = json.load(f)

            if result['func'] != str(self.fitfunc._className()):
                raise TypeError( "Trying to load a fit result from another fit method ({} instead of {}).".format(result['func'], self.fitfunc._className()) )

            if result['dim'] != self.getDimension():
                raise TypeError( "Trying to load a fit result for a different number of fit parameters")

            if 'range' in result:
                self.fitfunc.fitRange = result['range']

            params = self.fitfunc.getFitParams()
            for key, p in params.items():
                if key in exclude:
                    continue

                p.setValue(result['params'][key]['value'])

                if 'limits' in result['params'][key]:
                    p.setLimits(result['params'][key]['limits'])

                if 'errors' in result['params'][key]:
                    p.setErrors(result['params'][key]['errors'])

                if 'covar' in result['params'][key]:
                    for okey, op in  params.items():
                        p.setCovariance(op, result['params'][key]['covar'][okey])

    def getStatus(self):
        """
        Return the status of the fit.

        Returns:
            bool:   -1 if fit has not been run,
                     0 if fit was successful,
                    >0 if errors occured (depends on fit method).
        """
        return self.status

    def prepare(self):
        """
        Prepare the fit by setting all parameters and constraints.

        Returns:
            tuple: Starting parameters and parameter constraints,
                like ``( [p0, p1, ...], [ (p1_lo,p1_hi), (p2_lo,p2_hi), ... ] )``.
        """
        self.ndim = len(self.fitfunc)

        params = self.fitfunc.getFitValues().values()
        limits = self.fitfunc.getFitLimits().values()

        return ( params, limits )

    def finish(self, popt, pcov):
        """
        Finish the fit by computing the uncertainties and saving the result.

        Args:
            popt (list): Fit parameters from ``scipy.curve_fit``.
            pcov (matrix): Covariance matrix from ``scipy.curve_fit``.

        Returns:
            tuple: Resulting fit parameters and errors,
                like ``( [p0, p1, ...], [ (e1_lo,e1_hi), (e2_lo,p2_hi), ... ] )``.
        """
        chi2, ndf, prob = self.getChiSquare(popt)
        try:
            perr = ( np.sqrt(np.diag(pcov)) / np.sqrt(chi2/ndf) ).tolist()
        except:
            perr = [ 0. for p in popt ]

        result  = map(lambda x: x, popt)
        errors  = map(lambda x: (x, x), perr)  # symmetric errors

        params = self.fitfunc.getFitParams().values()
        for i in xrange(self.ndim):
            params[i].value  = result[i]
            params[i].errors = ( errors[i][0], errors[i][1] )

        return ( result, errors )

    def run(self, plot=False,
            maxfev=9000, epsfcn=None, factor=100, **kwargs):
        """
        Run the fit method using the defined fit function and parameters.

        This implementation uses the Least-Squares method from
        ``scipy.optimize.curve_fit``. Extra parameters are passed on
        to this function. The fit is initialized with the currently
        defined set of parameters. After running the fit, the parameters
        and their errors are updated with the fit results, and returned
        via ``getFitParams()``.

        Note::
            There are other fit methods implemented in Peaberry that
            might produce better results.

        Args:
            plot (bool): If `True`, produce a set of fit-performance
                plots (chi-square, probability).
            maxfev (int): Maximum number of calls to the function
                (see ``scipy.optimize.leastsq``).
            epsfcn (float): Variable used in determining a suitable step length
                (see ``scipy.optimize.leastsq``).
            factor (float): Parameter determining the initial step bound
                (see ``scipy.optimize.leastsq``).
            **kwargs (dict): Additional arguments passed to
                ``lmfit.minimize``.
        """
        if len(self) <= 0:
            return None

        if len(self.graph) < len(self):
            raise RuntimeError("{0}: number of data points is too small for fit of order {1}".format(self.graph._className(), len(self)))

        xvalues = self.graph.getXvalues(filtered=True)
        yvalues = self.graph.getYvalues(filtered=True)

        yerrors = None
        if self.useYerrors and self.graph.hasErrorsY():
            yerrors = self.graph.getYerrors(filtered=True)
        if self.useXerrors and self.graph.hasErrorsX():
            raise RuntimeWarning("x-errors are not supported by this fit method!")
            pass  # curve_fit does not support x-errors

        fitfunc = lambda x, *p: self.fitfunc.fit(x, *p)

        message("Beginning the SciPy Least-Squares fit.", level=1)

        params, limits = self.prepare()
        #message("Parameter constraints: {0}".format(" | ".join([ "{0} .. {1}".format(l,h) for l,h in limits ]), level=2))
        message("Starting parameters:  {0}".format(" | ".join([ str(p) for p in params ]), level=2))

        message("Running CurveFit ...", level=1)
        try:
            popt, pcov = optimize.curve_fit(fitfunc, xvalues, yvalues, params, yerrors,
                    maxfev=maxfev, epsfcn=epsfcn, factor=factor, **kwargs)
        except optimize.OptimizeWarning:
            self.status = 1
            return
        except ValueError:
            self.status = 2
            return

        self.status = 0
        message("Done!", level=3)

        message("Computing results.", level=1)
        result, errors = self.finish(popt, pcov)
        message("Result: {0}".format(" | ".join([ str(p) for p in result ]), level=2))

        if plot:
            message("Generating fit result plots...", level=1)
            try:
                self.plotProbability(result, errors)
                self.plotChiSquare(result, errors)
                message("Done!", level=3)
            except Exception as e:
                exception(e, "Failed to plot fit results!")

    def savePlot(self, fig, basename, index=None):
        if index == None and self.graph != None:
            index = self.graph.name

        if index != None:
            filename = "{0}-{1}.png".format(basename, index)
        else:
            filename = "{0}.png".format(basename)
        fig.savefig(filename)

    def plotProbability(self, result, errors, scale=2):
        dim = self.ndim
        params = self.fitfunc.getFitParams().values()

        message("Plotting probability distributions", level=2)
        plt.figure()
        fig, axes = plt.subplots(dim, 1, sharex=False, figsize=(10, 10))
        if dim == 1:
            axes = [axes]
        for i in range(dim):
            N = 100
            pmin = result[i] - scale * errors[i][0]
            pmax = result[i] + scale * errors[i][1]
            prange = np.linspace(pmin, pmax, N)
            pcurve  = np.zeros(N)
            theta = result[:dim]  # make copy
            for k in xrange(N):
                theta[i] = prange[k]
                chi2, ndf, prob = self.getChiSquare(theta)
                pcurve[k] = prob
            #chi2, ndf, prob = self.getChiSquare(theta)
            pcurve = np.array(pcurve)
            axes[i].plot(prange, pcurve, color='b', lw=2)
            axes[i].set_xlim(pmin, pmax)
            axes[i].set_ylim(0, 1.2 * pcurve.max())
            axes[i].axvline(result[i], color='k', ls='--', lw=1)
            axes[i].axvline(result[i] - errors[i][0], color='k', ls=':', lw=1)
            axes[i].axvline(result[i] + errors[i][1], color='k', ls=':', lw=1)
            axes[i].xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i].yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i].xaxis.set_major_locator(MaxNLocator(6))
            axes[i].yaxis.set_major_locator(MaxNLocator(4))
            axes[i].set_ylabel(params[i].getName())
        fig.tight_layout(h_pad=0.0)
        self.savePlot(fig, "fit-prob")

    def plotChiSquare(self, result, errors, scale=2):
        dim = self.ndim
        params = self.fitfunc.getFitParams().values()

        message("Plotting chi2 distributions", level=2)
        plt.figure()
        fig, axes = plt.subplots(dim, 1, sharex=False, figsize=(10, 10))
        if dim == 1:
            axes = [axes]
        for i in range(dim):
            N = 100
            pmin = result[i] - scale * errors[i][0]
            pmax = result[i] + scale * errors[i][1]
            prange = np.linspace(pmin, pmax, N)
            pcurve  = np.zeros(N)
            theta = result[:dim]  # make copy
            for k in xrange(N):
                theta[i] = prange[k]
                chi2, ndf, prob = self.getChiSquare(theta)
                pcurve[k] = chi2/ndf
            chi2, ndf, prob = self.getChiSquare()  # best fit
            axes[i].plot(prange, pcurve, color='b', lw=2)
            axes[i].set_xlim(pmin, pmax)
            axes[i].set_ylim(0, pcurve.max())
            axes[i].axhline(chi2/ndf + stats.chi2.ppf(0.68, dim), color='r', lw=1)
            axes[i].axhline(chi2/ndf + stats.chi2.ppf(0.95, dim), color='r', lw=1, ls='--')
            axes[i].axhline(chi2/ndf + stats.chi2.ppf(0.99, dim), color='r', lw=1, ls=':')
            axes[i].axvline(result[i], color='k', ls='--', lw=1)
            axes[i].axvline(result[i] - errors[i][0], color='k', ls=':', lw=1)
            axes[i].axvline(result[i] + errors[i][1], color='k', ls=':', lw=1)
            axes[i].xaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i].yaxis.set_major_formatter(ScalarFormatter(useOffset=False))
            axes[i].xaxis.set_major_locator(MaxNLocator(6))
            axes[i].yaxis.set_major_locator(MaxNLocator(4))
            axes[i].set_ylabel(params[i].getName())
        fig.tight_layout(h_pad=0.0)
        self.savePlot(fig, "fit-chi2")

########################################################################

class Graph(object):
    """
    Basic class for a graph object with a number data points.

    The class holds all fit data points (x + y) and their errors,
    and can be associated with a plot instance that will display
    this graph. It is also possible to associate a fit instance with
    the graph. Several low- and high-level methods exist to manipulate
    the contained data points.

    Note:
        The ``style`` dictionary allows to override the default plotting
        style for this graph. The following style groups are available:
            * ``(empty)`` - Global options.
            * ``data`` - Options for plotting the data points.
            * ``errorband`` - Options for plotting the error band (instead of error bars).
            * ``fit`` - Options for plotting the fit result.
            * ``fitband`` - Options for plotting the fit uncertainty band.
            * ``residuals`` - Options for plotting the residuals.
            * ``residband`` - Options for plotting the residual uncertainty band.
            * ``residhist`` - Options for plotting the residual histogram.
            * ``label`` - Options for displaying the graph in the legend.

    Args:
        name (string): A unique identifier for the graph that also will
            be used to identify the graph in the plot legend.
        label (string): A label that will be added in the plot legend.
        plot (instance): A plot that is associated with this graph.
        fit (instance): A fit that is associated with this graph.

    Raises:
        TypeError: If ``plot`` is not an instance of ``Peaberry.Main.Plot``.
        TypeError: If ``fit`` is not an instance of ``Peaberry.Base.Fit``.

    Example:
        >>> import numpy as np
        >>> N = 10
        >>> x, y = np.linspace(0, 5, N), np.random.randint(0, 100, N)
        >>>
        >>> from Peaberry import Main, Fits, Base, Callbacks
        >>> myplot  = Main.Plot("An Example")
        >>> myfit   = Fits.Linear()
        >>> mygraph = Base.Graph('mygraph', "data + linear fit", plot=myplot, fit=myfit)
        >>> mygraph.setPoints(x, y)
        >>> mygraph.setPoissonErrors()
        >>> mygraph.addCallback(Callbacks.GuessFitParams)
        >>> mygraph.addCallback(Callbacks.ApplyFit)
        >>> myplot.generate("example", preview=True)

    See also:
        :py:class:`Peaberry.Base.Hist` is the base class for all histograms,
        which inherits from :py:class:`Peaberry.Base.Graph`.
    """

    def __init__(self, name, label="", plot=None, fit=None, secondary=False, colorvalue=None, nolabel=False):
        self.name       = name
        self.label      = label
        self.index      = 0
        self.xvalues    = np.array([])
        self.yvalues    = np.array([])
        self.xerrorsLo  = np.array([])
        self.xerrorsHi  = np.array([])
        self.yerrorsLo  = np.array([])
        self.yerrorsHi  = np.array([])
        self.filename   = None
        self.callbacks  = []
        self.plot       = None
        self.fit        = None
        self.secondary  = secondary
        self.colorvalue = colorvalue
        self.nolabel    = nolabel

        if plot != None:
            self.setPlot(plot)

        if fit != None:
            self.setFit(fit)

        self.style      = {
            '':             {},
            'data':         {},
            'errorband':    {},
            'fit':          {},
            'fitband':      {},
            'residuals':    {},
            'residband':    {},
            'residhist':    {},
            'label':        {},
        }

    def __str__(self):
        result = self._className()
        result += u" <{0}>".format(self.name)
        result += u" with {0} points".format(len(self))
        if self.label:
            result += u" ({0})".format(self.label)
        if self.fit:
            result += u" + fit"
        return result

    def __len__(self):
        """
        Return the number of data points in this graph.

        Returns:
            int: The number of data points.
        """
        return self.xvalues.size

    def __getitem__(self, index):
        """
        Return a single data point (x + y) at the given index.

        Args:
            index (int): The index of the data point to retrieve.

        Returns:
            tuple: The data point, like ``(x, y)``,
                or ``(None, None)`` if index is out of range.
        """
        if 0 <= index < len(self):
            return (self.xvalues[index], self.yvalues[index])
        return (None, None)

    def __delitem__(self, index):
        """
        Remove a single data point at the given index.

        Args:
            index (int): The index of the data point to remove.
        """
        self.removePointIndex(index)

    def __contains__(self, index):
        """
        Check if the given index references an existing data point.

        Returns:
            (bool): ``True`` if the index is within the valid range.
        """
        return (index >= 0 and index < len(self))

    def _className(self):
        return self.__class__.__name__

    def isSecondary(self):
        return self.secondary

    def getColorValue(self):
        if self.colorvalue == None:
            return self.index

        return self.colorvalue

    def save(self, filename, fileformat=None, header=True):
        """
        Export this graph's contents to a file.

        Note::
            The file format is determined from the given filename
            by default, but can be overwritten by the `fileformat`
            argument.
            Supported types are:
                `json`,
                `dat` (gnuplot-style),
                `csv` (excel-style).

        Note::
            This function will only save the graph's data. The output
            file will not include information about histogram bins
            or an associated fit model.

        Args:
            filename (string): The filename of the output file.
            fileformat (string): An optional identifier for the file
                    format (default: determine from filename).
            header (bool): If `True`, a header line will be written
                    if the file format supports it.
        """

        if fileformat == None:
            fileformat = os.path.splitext(filename)[1][1:]

        with open(filename, 'w') as f:
            if fileformat == 'json':
                result = OrderedDict()
                result['name']    = self.name
                result['label']   = self.label
                result['xvalues'] = self.xvalues.tolist()
                result['yvalues'] = self.yvalues.tolist()
                result['xerrors'] = ( self.xerrorsLo.tolist(), self.xerrorsHi.tolist() )
                result['yerrors'] = ( self.yerrorsLo.tolist(), self.yerrorsHi.tolist() )

                json.dump(result, f, indent=4, sort_keys=False)  # already sorted

            elif fileformat == 'dat':
                xvalues = self.xvalues
                yvalues = self.yvalues
                if self.hasErrorsX():
                    xerrorsLo = self.xerrorsLo
                    xerrorsHi = self.xerrorsHi
                else:
                    xerrorsLo = xerrorsHi = [ 0. for x in xvalues ]
                if self.hasErrorsY():
                    yerrorsLo = self.yerrorsLo
                    yerrorsHi = self.yerrorsHi
                else:
                    yerrorsLo = yerrorsHi = [ 0. for x in xvalues ]

                if header == True:
                    header = "# %-10s\t  %-10s\t  %-10s\t  %-10s\t  %-10s\t  %-10s\n" % ( "x", "y", "dx-", "dx+", "dy-", "dy+" )
                    f.write(header)

                for i in xrange(len(self)):
                    line  = "%-12.8e\t%-12.8e\t%-12.8e\t%-12.8e\t%-12.8e\t%-12.8e\n" %\
                            ( xvalues[i], yvalues[i], xerrorsLo[i], xerrorsHi[i], yerrorsLo[i], yerrorsHi[i] )
                    f.write(line)

            elif fileformat == 'csv':
                xvalues = self.xvalues
                yvalues = self.yvalues
                if self.hasErrorsX():
                    xerrorsLo = self.xerrorsLo
                    xerrorsHi = self.xerrorsHi
                else:
                    xerrorsLo = xerrorsHi = [ 0. for x in xvalues ]
                if self.hasErrorsY():
                    yerrorsLo = self.yerrorsLo
                    yerrorsHi = self.yerrorsHi
                else:
                    yerrorsLo = yerrorsHi = [ 0. for x in xvalues ]

                if header == True:
                    header = "#%s,%s,%s,%s,%s,%s\n" % ( "x", "y", "dx-", "dx+", "dy-", "dy+" )
                    f.write(header)

                for i in xrange(len(self)):
                    line = "%.8e,%.8e,%.8e,%.8e,%.8e,%.8e\n" %\
                            ( xvalues[i], yvalues[i], xerrorsLo[i], xerrorsHi[i], yerrorsLo[i], yerrorsHi[i] )
                    f.write(line)

            else:
                raise ValueError("Unsopprted file format {} - graph {} was not saved".format(fileformat, self.name))

    def load(self, filename, keeplabel=True):
        """
        Import graph contents from a JSON.

        Note::
            Although py:func:export supports other file formats than
            JSON, this function only operates on JSON files. One can
            use dedicated modules like py:mod:CSV to read non-JSON
            data files.

        Args:
            filename (string): The filename of the output file.
            keeplabel (bool): If `True`, do not override this graph's
                label with the one from the import file.
        """

        with open(filename, 'r') as f:
            result = json.load(f)

            if keeplabel == False:
                self.label = result['label']

            self.xvalues = np.array(result['xvalues'])
            self.yvalues = np.array(result['yvalues'])

            self.xerrorsLo = np.array(result['xerrors'][0])
            self.xerrorsHi = np.array(result['xerrors'][1])

            self.yerrorsLo = np.array(result['yerrors'][0])
            self.yerrorsHi = np.array(result['yerrors'][1])

    def getXvalues(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve x-values in this graph.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        if filtered:
            return self.filter(self.xvalues, invert=invert, thinning=thinning)
        else:
            return thinned(self.xvalues, thinning=thinning)

    def getYvalues(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve y-values in this graph.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        if filtered:
            return self.filter(self.yvalues, invert=invert, thinning=thinning)
        else:
            return thinned(self.yvalues, thinning=thinning)

    def getXerrors(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve y-errors in this graph.
        If the graph has asymmetric errors, the mean value is returned.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        xerrorsLo, xerrorsHi = self.getXerrorsAsym(filtered=filtered, invert=invert, thinning=thinning)
        return np.sqrt( 0.5 * (np.square(xerrorsLo) + np.square(xerrorsHi)) )  # also correct for symmetric errors

    def getXerrorsAsym(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve x-errors in this graph.
        This function always returns two arrays even if the errors are symmetric.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        if filtered:
            return ( self.filter(self.xerrorsLo, invert=invert, thinning=thinning),\
                     self.filter(self.xerrorsHi, invert=invert, thinning=thinning) )
        else:
            return ( thinned(self.xerrorsLo, thinning=thinning),\
                     thinned(self.xerrorsHi, thinning=thinning) )

    def getYerrors(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve y-errors in this graph.
        If the graph has asymmetric errors, the mean value is returned.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        yerrorsLo, yerrorsHi = self.getYerrorsAsym(filtered=filtered, invert=invert, thinning=thinning)
        return np.sqrt( 0.5 * (np.square(yerrorsLo) + np.square(yerrorsHi)) )  # also correct for symmetric errors

    def getYerrorsAsym(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve y-errors in this graph.
        This function always returns two arrays even if the errors are symmetric.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        if filtered:
            return ( self.filter(self.yerrorsLo, invert=invert, thinning=thinning),\
                     self.filter(self.yerrorsHi, invert=invert, thinning=thinning) )
        else:
            return ( thinned(self.yerrorsLo, thinning=thinning),\
                     thinned(self.yerrorsHi, thinning=thinning) )

    def getTransformedXerrors(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve y-errors in this graph.
        If the graph has asymmetric errors, the mean value is returned.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        yerrorsLo, yerrorsHi = self.getTransformedXerrorsAsym(filtered=filtered, invert=invert, thinning=thinning)
        return np.sqrt( 0.5 * (np.square(yerrorsLo) + np.square(yerrorsHi)) )  # also correct for symmetric errors

    def getTransformedXerrorsAsym(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve transformed x-errors in this graph.
        If the graph has asymmetric errors, the mean value is returned.

        This function will transform given x-errors to y-errors that
        can be used in a chi-square fit where only y-errors are
        considered. The method uses the gradient of the y-values.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        # compute gradient *before* filtering any data points (Note: this function is used without filtering typically)
        xvalues, yvalues = self.xvalues, self.yvalues
        if filtered:
            xgradient = self.filter(np.gradient(xvalues), invert=invert, thinning=thinning)
            ygradient = self.filter(np.gradient(yvalues), invert=invert, thinning=thinning)
        else:
            xgradient = thinned(np.gradient(xvalues), thinning=thinning)
            ygradient = thinned(np.gradient(yvalues), thinning=thinning)

        xerrorsLo, xerrorsHi = self.getXerrorsAsym(filtered=filtered, invert=invert, thinning=thinning)
        yerrorsLo, yerrorsHi = np.zeros(ygradient.size), np.zeros(ygradient.size)

        for i in range(ygradient.size):
            dx, dy = xgradient[i], ygradient[i]
            if dy == 0.:
                continue
            elif dy > 0:
                # low (left) x-error adds to low (down) y-error
                yerrorsLo[i] = xerrorsLo[i] * dy / dx
                yerrorsHi[i] = xerrorsHi[i] * dy / dx
            elif dy < 0:
                # low (left) x-error adds to high (up) y-error
                yerrorsHi[i] = xerrorsLo[i] * -dy / dx
                yerrorsLo[i] = xerrorsHi[i] * -dy / dx

        return (yerrorsLo, yerrorsHi)

    def setPlot(self, plot):
        """
        Associate a plot instance with this graph.

        When the plot is drawn, it will include all associated graph instances.

        Args:
            plot (instance): A plot that is associated with this graph.

        Raises:
            TypeError - If ``plot`` is not an instance of ``Peberry.Main.Plot``.
        """
        if not isinstance(plot, Main.Plot):
            raise TypeError

        self.plot   = plot
        self.index  = len(plot.graphs)
        self.name   = plot.getGraphName(self.name)

        plot.graphs[self.name] = self

    def setFit(self, fit):
        """
        Associate a fit instance with this graph.

        The fit can then be applied to the data contained in this graph.

        Args:
            fit (instance): A fit that is associated with this graph.

        Raises:
            TypeError - If ``fit`` is not an instance of ``Peberry.Base.Fit``.
        """
        if not isinstance(fit, Fit):
            raise TypeError

        self.fit    = fit
        fit.graph   = self

    def addCallback(self, func, **kwargs):
        """
        Add a callback function which is executed before drawing this graph.

        One example of a callback function is ``ApplyFit`` which runs
        the associated fit method. The fit result is then available
        when the graph is plotted.
        Any keyword arguments are passed on to the callback function.

        Args:
            func (function): A function to be called.
            **kwargs: Passed on to the callback function.

        See also:
            :py:mod:`Peaberry.Callbacks` is the module which contains
            the available callback functions.
        """
        self.callbacks.append((func, kwargs))

    def clearCallbacks(self):
        """
        Removes any previously added callback functions.
        """
        self.callbacks = []

    def isHistogram(self):
        """
        Check if this graph instance is a histogram (with bins).

        Returns:
            bool: ``True`` if the instance is a histogram, ``False`` otherwise.

        See also:
            :py:class:`Peaberry.Base.Hist` is the base class for all histograms.
        """
        return False

    def is2D(self):
        """
        Check if this graph instance is two-dimensional (used only by histograms).

        Returns:
            bool: ``True`` if the instance is two-dimensional, ``False`` otherwise.
        """
        return False

    def isEmpty(self):
        """
        Check if the graph is empty (i.e. has no data points).

        Returns:
            bool: ``True`` if the graph has no data points.
        """
        return (len(self) == 0)

    def hasFit(self):
        """
        Check if the graph has an associated fit instance.

        Returns:
            bool: ``True`` if the graph is associated with a fit.

        See Also:
            :py:class:`Peaberry.Base.Fit` is the base class for all fits.
        """
        return (self.fit != None)

    def hasErrorsX(self):
        """
        Check if the graph has x-errors added to the data points.

        Returns:
            bool: ``True`` if the graph has data points with x-errors.
        """
        return (min(self.xerrorsLo.size, self.xerrorsHi.size) > 0)

    def hasErrorsY(self):
        """
        Check if the graph has y-errors added to the data points.

        Returns:
            bool: ``True`` if the graph has data points with y-errors.
        """
        return (min(self.yerrorsLo.size, self.yerrorsHi.size) > 0)

    def hasAsymErrorsX(self):
        """
        Check if all x-errors are symmetric.

        Returns:
            bool: ``True`` if the graph has symmetric x-errors.
        """
        return (self.xerrorsLo == self.xerrorsHi).all()

    def hasAsymErrorsY(self):
        """
        Check if all y-errors are symmetric.

        Returns:
            bool: ``True`` if the graph has symmetric y-errors.
        """
        return (self.yerrorsLo == self.yerrorsHi).all()

    def values(self):
        """
        Return a zipped list of data points.

        Returns:
            list: List of data points, like ``[ (x1, y1), (x2, y2), ... ]``.
        """
        xvalues = self.getXvalues()
        yvalues = self.getYvalues()

        return zip(xvalues, yvalues)

    def errors(self):
        """
        Return a zipped list of data point errors.

        If no errors are defined, ``None`` is returned for either ex or ey.
        Symmetric errors will be returned even if the graph contains
        asymmetric errors.

        See also:
            Function :py:func:`getXerrors`, :py:func:`getYerrors`.

        Returns:
            list: List of data point errors, like ``[ (ex1, ey1), (ex2, ey2), ... ]``.
        """
        xvalues = self.getXvalues()
        xerrors = self.getXerrors()
        yerrors = self.getYerrors()

        if not self.hasErrorsX():
            xerrors = np.full(xvalues.size, None)
        if not self.hasErrorsY():
            yerrors = np.full(xvalues.size, None)
        return zip(xvalues, xerrors, yerrors)

    def filter(self, values, xvalues=None, invert=False, thinning=0):
        """
        Return a filtered list of values, based on the associated fit.

        If the graph has no associated fit, or the fit has an undefine
        fit range, the function returns all input values.
        Otherwise, only the values which correspond to x-values that are
        inside the fit range are returned. This function can be used
        to prepare a list of value for fit routines or plotting.

        Args:
            values (list): A list of input values to filter,
                e.g. a set of y-values.
            xvalues (list): An optional list of x-values to use as a
                base for filtering (if skipped, the x-values of the
                graph are used).
            invert (bool): If true, only values *outside* the fit range
                are returned.
            thinning (int): If positive, the number of data points is
                reduced (subsampling).

        Returns:
            list: List of filtered values.
        """
        if values.size == 0:
            return values.copy()  # empty array

        if self.fit:
            if not xvalues:
                xvalues = self.xvalues  # no copy needed

            xmin, xmax = self.fit.fitfunc.getFitRange()
            if invert:
                if xmin == -np.inf and xmax == np.inf:
                    return np.array([])
                elif xmin == -np.inf:
                    mask = (xvalues > xmax)
                elif xmax == np.inf:
                    mask = (xvalues < xmin)
                else:
                    mask = (xvalues < xmin) | (xvalues > xmax)
            else:
                if xmin == -np.inf and xmax == np.inf:
                   return values
                elif xmin == -np.inf:
                    mask = (xvalues <= xmax)
                elif xmax == np.inf:
                    mask = (xvalues >= xmin)
                else:
                    mask = (xvalues >= xmin) & (xvalues <= xmax)
            values = values[mask]

        return thinned(values, thinning=thinning)

    def runCallbacks(self):
        """
        Execute all the added callback functions sequentially.

        See also:
            Function :py:func:`addCallback`.
        """
        for func,args in self.callbacks:
            func(self, **args)

    def copyFrom(self, other):
        """
        Copy over data points from another graph.

        This method just copies the data points, and does not change any
        associated plots or fits.

        Args:
            other (instance): A graph to copy the data from.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Graph``.
        """
        if not isinstance(other, Graph):
            raise TypeError

        self.xvalues    = np.copy(other.xvalues)
        self.yvalues    = np.copy(other.yvalues)
        self.xerrorsLo  = np.copy(other.xerrorsLo)
        self.xerrorsHi  = np.copy(other.xerrorsHi)
        self.yerrorsLo  = np.copy(other.yerrorsLo)
        self.yerrorsHi  = np.copy(other.yerrorsHi)

    def shift(self, value):
        """
        Shift data points by the given amount along the x-axis.

        Args:
            value (float): A length on the x-axis by which to shift all data points.
        """
        self.xvalues += value

    def scale(self, value):
        """
        Scale data point y-values by the given amount.

        If defined, the y-errors will be scaled as well.

        Args:
            value (float): A factor by which to multiply the y-value of all data points.
        """
        self.yvalues *= value
        if self.hasErrorsY():
            self.yerrorsLo *= value
            self.yerrorsHi *= value

    def flip(self):
        """
        Reverse the ordering of the data points.
        """
        self.xvalues = self.xvalues[::-1]
        self.yvalues = self.yvalues[::-1]
        if self.hasErrorsX():
            self.xerrorsLo = self.xerrorsLo[::-1]
            self.xerrorsHi = self.xerrorsHi[::-1]
        if self.hasErrorsY():
            self.yerrorsLo = self.yerrorsLo[::-1]
            self.yerrorsHi = self.yerrorsHi[::-1]

    def sort(self, reverse=False):
        """
        Sort the data points based on their x-values.
        """
        order = np.argsort(self.xvalues)
        if reverse == True:
            order = order[::-1]

        self.xvalues = self.xvalues[order]
        self.yvalues = self.yvalues[order]
        if self.hasErrorsX():
            self.xerrorsLo = self.xerrorsLo[order]
            self.xerrorsHi = self.xerrorsHi[order]
        if self.hasErrorsY():
            self.yerrorsLo = self.yerrorsLo[order]
            self.yerrorsHi = self.yerrorsHi[order]

    def append(self, other):
        """
        Append another graph to this one.

        The x- and y-values will from ``other'' will be appended to the
        values of this graph. If errors are defined for this graph,
        the function will also append errors from ``other''.

        Note:
            This function will change the data points of this graph in-place.

        Args:
            other (instance): Another graph to be combined into this one.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Graph``.
            DomainError - If ``other`` is not compatible with this graph,
                    e.g. when errors are only defined in `` other``.
        """
        if not isinstance(other, Graph):
            raise TypeError

        if len(other) == 0:
            return

        self.xvalues = np.append(self.xvalues, other.xvalues)
        self.yvalues = np.append(self.yvalues, other.yvalues)

        if self.hasErrorsX() or other.hasErrorsX():
            if not other.hasErrorsX():
                raise ValueError
            self.xerrorsLo = np.append(self.xerrorsLo, other.xerrorsLo)
            self.xerrorsHi = np.append(self.xerrorsHi, other.xerrorsHi)

        if self.hasErrorsY() or other.hasErrorsY():
            if not other.hasErrorsY():
                raise ValueError
            self.yerrorsLo = np.append(self.yerrorsLo, other.yerrorsLo)
            self.yerrorsHi = np.append(self.yerrorsHi, other.yerrorsHi)

    def combine(self, other, func, errfunc=None):
        """
        Combine two graphs into this one, using the given callback function(s).

        The new y-values will be computed with ``func`` (i.e. a lambda
        function). This function must have two parameters for the y-values
        of this and another graph. If y-errors are defined in both
        graphs, ``errfunc`` or ``func`` will be used to compute the new
        y-errors. The length of both graphs must be the same.

        Note:
            This function will change the data points of this graph in-place.

        Args:
            other (instance): Another graph to be combined into this one.
            func (function): A callback function to compute the new y-values,
                like ``lambda y1,y2: y1-y2``.
            errfunc (function): An optional callback function to compute the new y-errors
                like ``lambda y1,y2,ey1,ey2: ey1+ey2``.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Graph``.
            DomainError - If both graphs have different lengths.
        """
        if not isinstance(other, Graph):
            raise TypeError

        if self.hasErrorsY():
            if errfunc:
                self.yerrorsLo = errfunc(self.yvalues, other.yvalues, self.yerrorsLo, other.yerrorsLo)
                self.yerrorsHi = errfunc(self.yvalues, other.yvalues, self.yerrorsHi, other.yerrorsHi)
            else:
                self.yerrorsLo = np.zeros(self.yvalues)
                self.yerrorsHi = np.zeros(self.yvalues)
        self.yvalues = func(self.yvalues, other.yvalues)  # do this after error computation!

    def setStyle(self, group, values):
        """
        Override the default plotting style with user-defined values.

        The ``group`` argument refers to the plot object to which the
        style is applied (e.g. ``data``). The values are given as
        dictionary where the key and value is a Matplotlib draw option.

        If an option is not defined, the global style in the associated
        plot or the default style is used.

        Args:
            group (string): The style group in which the given values are set.
            values (dict): A dictionary with the new style value,
                like ``{ 'option': value, ... }``.

        Raises:
            KeyError: If ``group`` is not a valid style group.

        See also:
            :py:class:`Peaberry.Base.Graph` lists all of the available
            style groups.
            :py:class:`Peaberry.Main.Plot` lists the avaible options
            for each group.
        """
        if not group in self.style:
            raise KeyError

        for k,v in values.items():
            # allow to add new values (which overwrite defaults)
            self.style[group][k] = v

    def getFuncResult(self, func):
        """
        Apply the given function to all data points and return the result.

        Args:
            func (function): A function which is applied to all data points,
                e.g. ``lambda x: x**2``.

        Returns:
            tuple: An (unzipped) list of the resulting values,
                like ``( [x1, x2, ...], [y1, y2, ...])``.
        """
        if not len(self):
            return (0., 0.)
        return ( func(self.xvalues), func(self.yvalues) )

    def getRangeX(self):
        """
        Return the lower/upper limit of x-values.

        Returns:
            tuple: The x-range of data points, like ``(l, h)``.
        """
        return (self.xvalues.min(), self.xvalues.max())

    def getRangeY(self):
        """
        Return the lower/upper limit of y-values.

        Returns:
            tuple: The y-range of data points, like ``(l, h)``.
        """
        return (self.yvalues.min(), self.yvalues.max())

    def getMean(self):
        """
        Return the mean of the data points along both axes.

        Returns:
            tuple: The mean of all data points, like ``(x, y)``.
        """
        return self.getFuncResult(lambda x: np.mean(x))

    def getMedian(self):
        """
        Return the median of the data points along both axes.

        Returns:
            tuple: The median of all data points, like ``(x, y)``.
        """
        return self.getFuncResult(lambda x: np.median(x))

    def getStdDev(self):
        """
        Return the standard deviation of the data points along both axes.

        Returns:
            tuple: The std.dev. of all data points, like ``(x, y)``.
        """
        return self.getFuncResult(lambda x: np.std(x))

    def getVariance(self):
        """
        Return the variance of the data points along both axes.

        Returns:
            tuple: The variance of all data points, like ``(x, y)``.
        """
        return self.getFuncResult(lambda x: np.var(x))

    def getRMS(self):
        """
        Return the root mean square of the data points along both axes.

        Returns:
            tuple: The RMS of all data points, like ``(x, y)``.
        """
        return self.getFuncResult(lambda x: np.sqrt(np.mean(np.square)))

    def allocatePoints(self, n, xerr=False, yerr=False):
        """
        Allocate memory for the given number of points.

        Note:
            Use ``setPoint`` with an index to modify data in the
            allocated array.

        Args:
            n (int): Number of points to allocate.
            xerr (bool): Allocate space for x-errors.
            yerr (bool): Allocate space for y-errors.
        """
        self.xvalues = np.zeros(n)
        self.yvalues = np.zeros(n)
        if xerr == True:
            self.xerrorsLo = np.zeros(n)
            self.xerrorsHi = np.zeros(n)
        if yerr == True:
            self.yerrorsLo = np.zeros(n)
            self.yerrorsHi = np.zeros(n)

    def setPoint(self, i, x, y, xerr=None, yerr=None):
        """
        Set the data point with given index.

        Note:
            Use ``allocatePoints`` to prepare data arrays.
        """
        if i < 0 or i >= len(self):
            raise ValueError

        self.xvalues[i] = x
        self.yvalues[i] = y
        if xerr != None:
            if not self.hasErrorsX():  # make sure that array exists
                self.xerrorsLo = np.zeros(len(self))
                self.xerrorsHi = np.zeros(len(self))
            if np.ndim(xerr) == 1:  # symmetric errors
                self.xerrorsLo[i] = xerr
                self.xerrorsHi[i] = xerr
            else:
                self.xerrorsLo[i] = xerr[0]
                self.xerrorsHi[i] = xerr[1]
        if yerr != None:
            if not self.hasErrorsY():  # make sure that array exists
                self.yerrorsLo = np.zeros(len(self))
                self.yerrorsHi = np.zeros(len(self))
            if np.ndim(yerr) == 1:  # symmetric errors
                self.yerrorsLo[i] = yerr
                self.yerrorsHi[i] = yerr
            else:
                self.yerrorsLo[i] = yerr[0]
                self.yerrorsHi[i] = yerr[1]

    def setPoints(self, x, y, xerr=None, yerr=None):
        """
        Set the data points in this graph to the given values.

        If errors are given to this function, they can be either
        symmetric and asymmetric.

        Note::
            This function silently replaces any existing data points.

        Args:
            x (list): A list of x-values to set.
            y (list): A list of y-values to set.
            xerr (list): An optional list of x-errors to set.
            yerr (list): An optional list of y-errors to set.

        Raises:
            ValueError: If the length of y-values (or x-/y-errors) does
                not match the length of x-values.
        """
        if not len(x) == len(y):
            raise ValueError

        self.xvalues = np.array(x)
        self.yvalues = np.array(y)
        if xerr != None:
            if not len(xerr) == len(x):
                raise ValueError
            if np.ndim(xerr) == 1:  # symmetric errors
                self.xerrorsLo = np.array(xerr)
                self.xerrorsHi = np.array(xerr)
            else:
                self.xerrorsLo = np.array(xerr[0])
                self.xerrorsHi = np.array(xerr[1])
        if yerr != None:
            if not len(yerr) == len(y):
                raise ValueError
            if np.ndim(yerr) == 1:  # symmetric errors
                self.yerrorsLo = np.array(yerr)
                self.yerrorsHi = np.array(yerr)
            else:
                self.yerrorsLo = np.array(yerr[0])
                self.yerrorsHi = np.array(yerr[1])

    def addPoint(self, x, y, xerr=None, yerr=None):
        """
        Add the given data point to this graph.

        Note::
            If x- or y-errors are defined in this graph, but a data
            point is added without any error values, the respective
            error for the new data point will be set to zero.

        Args:
            x (list): An x-values to add.
            y (list): An y-values to add.
            xerr (list): An optional x-error to add.
            yerr (list): An optional y-error to add.
        """
        if self.isEmpty():
            # first point in the dataset
            self.xvalues = np.array([x])
            self.yvalues = np.array([y])

            if xerr != None:
                if np.ndim(xerr) == 0:  # symmetric errors
                    self.xerrorsLo = self.xerrorsHi = np.array([xerr])
                else:
                    self.xerrorsLo = np.array([xerr[0]])
                    self.xerrorsHi = np.array([xerr[1]])

            if yerr != None:
                if np.ndim(yerr) == 0:  # symmetric errors
                    self.yerrorsLo = self.yerrorsHi = np.array([yerr])
                else:
                    self.yerrorsLo = np.array([yerr[0]])
                    self.yerrorsHi = np.array([yerr[1]])

        else:
            self.xvalues = np.append(self.xvalues, x)
            self.yvalues = np.append(self.yvalues, y)

            if self.hasErrorsX():
                if xerr == None:
                    xerr = 0.
                if np.ndim(xerr) == 0:  # symmetric errors
                    self.xerrorsLo = self.xerrorsHi = np.append(self.xerrorsLo, xerr)
                else:
                    self.xerrorsLo = np.append(self.xerrorsLo, xerr[0])
                    self.xerrorsHi = np.append(self.xerrorsHi, xerr[1])

            if self.hasErrorsY():
                if yerr == None:
                    yerr = 0.
                if np.ndim(yerr) == 0:  # symmetric errors
                    self.yerrorsLo = self.yerrorsHi = np.append(self.yerrorsLo, yerr)
                else:
                    self.yerrorsLo = np.append(self.yerrorsLo, yerr[0])
                    self.yerrorsHi = np.append(self.yerrorsHi, yerr[1])

    def setMinimalErrors(self, xerr=None, yerr=None):
        """
        Set the given absolute errors on all data points
        where the existing error is smaller.

        Note::
            This function silently replaces any existing errors.

        Args:
            xerr (float): An optional x-error to set.
            yerr (float): An optional y-error to set.
        """
        if xerr != None:
            if not self.hasErrorsX():
                self.xerrorsLo = self.xerrorsHi = np.zeros(self.xvalues.size)

            if np.ndim(xerr) == 0:
                self.xerrorsLo = np.fmax(self.xerrorsLo, xerr)
                self.xerrorsHi = np.fmax(self.xerrorsHi, xerr)
            else:
                self.xerrorsLo = np.fmax(self.xerrorsLo, xerr[0])
                self.xerrorsHi = np.fmax(self.xerrorsHi, xerr[1])

        if yerr != None:
            if not self.hasErrorsY():
                self.yerrorsLo = self.yerrorsHi = np.zeros(self.yvalues.size)

            if np.ndim(yerr) == 0:
                self.yerrorsLo = np.fmax(self.yerrorsLo, yerr)
                self.yerrorsHi = np.fmax(self.yerrorsHi, yerr)
            else:
                self.yerrorsLo = np.fmax(self.yerrorsLo, yerr[0])
                self.yerrorsHi = np.fmax(self.yerrorsHi, yerr[1])

    def setAbsoluteErrors(self, xerr=None, yerr=None):
        """
        Set the given absolute errors on all data points.

        Note::
            This function silently replaces any existing errors.

        Args:
            xerr (float): An optional x-error to set.
            yerr (float): An optional y-error to set.
        """
        if xerr != None:
            if np.ndim(xerr) == 0:
                self.xerrorsLo = np.full(self.xvalues.size, xerr)
                self.xerrorsHi = np.full(self.xvalues.size, xerr)
            else:
                self.xerrorsLo = np.full(self.xvalues.size, xerr[0])
                self.xerrorsHi = np.full(self.xvalues.size, xerr[1])

        if yerr != None:
            if np.ndim(yerr) == 0:
                self.yerrorsLo = self.yerrorsHi = np.full(self.yvalues.size, yerr)
            else:
                self.yerrorsLo = np.full(self.yvalues.size, yerr[0])
                self.yerrorsHi = np.full(self.yvalues.size, yerr[1])

    def setRelativeErrors(self, xerr=None, yerr=None, minerror=0):
        """
        Set the given relative errors on all data points.

        Note::
            This function silently replaces any existing errors.

        Args:
            xerr (float): An optional factor to set x-errors.
            yerr (float): An optional factor to set y-errors.
            minerror (float): A mimimal error to keep (to avoid setting
                an error of zero if the data value is zero).
        """
        if xerr != None:
            if np.ndim(xerr) == 0:
                self.xerrorsLo = self.xerrorsHi = np.fmax(xerr * self.xvalues, minerror)
            else:
                self.xerrorsLo = np.fmax(xerr[0] * self.xvalues, minerror)
                self.xerrorsHi = np.fmax(xerr[1] * self.xvalues, minerror)

        if yerr != None:
            if np.ndim(yerr) == 0:
                self.yerrorsLo = self.yerrorsHi = np.fmax(yerr * self.yvalues, minerror)
            else:
                self.yerrorsLo = np.fmax(yerr[0] * self.yvalues, minerror)
                self.yerrorsHi = np.fmax(yerr[1] * self.yvalues, minerror)

    def setPoissonErrors(self, xerr=False, yerr=True, minerror=0):
        """
        Set the given Poissonian errors on all data points.

        By default, this function only sets the y-errors.

        Note::
            This function silently replaces any existing errors.

        Args:
            xerr (bool): If true, compute and set Poissonian x-errors.
            yerr (bool): If true, compute and set Poissonian y-errors.
            minerror (float): A mimimal error to keep (to avoid setting
                an error of zero if the data value is zero).
        """
        if xerr == True:
            self.xerrorsLo = self.xerrorsHi = np.fmax(np.sqrt(self.xvalues), minerror)

        if yerr == True:
            self.yerrorsLo = self.yerrorsHi = np.fmax(np.sqrt(self.yvalues), minerror)

    def setSymmetricErrors(self, xerr=False, yerr=True, average=True):
        """
        Replace asymmetric error by symmetric ones.

        By default, this function only sets the y-errors.

        Note::
            This function silently replaces any existing errors.

        Args:
            xerr (bool): If true, compute and set symmetric x-errors.
            yerr (bool): If true, compute and set symmetric y-errors.
            average (bool): If true, the symemtric errors will be
                computed as the average of the existing errors;
                otherwise the *minimal* error will be used.
        """
        if average == True:
            func = lambda l,h: 0.5 * (l + h)
        else:
            func = lambda l,h: np.fmin(l, h)

        if xerr == True:
            self.xerrorsLo = self.xerrorsHi = func(self.xerrorsLo, self.xerrorsHi)

        if yerr == True:
            self.yerrorsLo = self.yerrorsHi = func(self.yerrorsLo, self.yerrorsHi)

    def clear(self):
        """
        Remove all data points.
        """

        self.xvalues   = np.array([])
        self.yvalues   = np.array([])
        self.xerrorsLo = np.array([])
        self.xerrorsHi = np.array([])
        self.yerrorsLo = np.array([])
        self.yerrorsHi = np.array([])

    def removePointIndex(self, idx):
        """
        Remove a data point at the given index.

        This function supports negative indexing as well (to address
        elements starting from the end of the array).

        Args:
            idx (int): The index of the data point to remove.
        """
        if -len(self) <= idx < len(self):
            self.xvalues = np.delete(self.xvalues, idx)
            self.yvalues = np.delete(self.yvalues, idx)
            if self.hasErrorsX():
                self.xerrorsLo = np.delete(self.xerrorsLo, idx)
                self.xerrorsHi = np.delete(self.xerrorsHi, idx)
            if self.hasErrorsY():
                self.yerrorsLo = np.delete(self.yerrorsLo, idx)
                self.yerrorsHi = np.delete(self.yerrorsHi, idx)
        else:
            message("Warning: index '{0}' out of range, no points removed".format(idx), severity = 2)

    def removePointX(self, x):
        """
        Remove data points at the given x-value.

        If the x-value occurs more than once, all matching data
        points are removed.

        Args:
            x (float): The x-value of the data point to remove.
        """
        idx = 0
        while idx < len(self):
            if self.xvalues[idx] == x:
                self.removePointIndex(idx)
                continue
            idx += 1

    def removePointRangeX(self, xmin, xmax):
        """
        Remove data points in the given range of x-values.

        If more than one x-value occurs in the range, all matching data
        points are removed.

        Args:
            xmin (float): The lower x-value of the data points to remove.
            xmax (float): The upper x-value of the data points to remove.
        """
        idx = 0
        while idx < len(self):
            if self.xvalues[idx] >= xmin and self.xvalues[idx] <= xmax:
                self.removePointIndex(idx)
                continue
            idx += 1

    def removePointY(self, y):
        """
        Remove data points at the given y-value.

        If the y-value occurs more than once, all matching data
        points are removed.

        Args:
            y (float): The y-value of the data point to remove.
        """
        idx = 0
        while idx < len(self):
            if self.yvalues[idx] == y:
                self.removePointIndex(idx)
                continue
            idx += 1

    def removePointRangeY(self, ymin, ymax):
        """
        Remove data points in the given range of y-values.

        If more than one y-value occurs in the range, all matching data
        points are removed.

        Args:
            ymin (float): The lower y-value of the data points to remove.
            ymax (float): The upper y-value of the data points to remove.
        """
        idx = 0
        while idx < len(self):
            if self.yvalues[idx] >= ymin and self.xvalues[idx] <= ymax:
                self.removePointIndex(idx)
                continue
            idx += 1

    def removePointsBelowY(self, y):
        """
        Remove data points below the given y-value.

        If more than one y-value occurs below the value, all matching
        data points are removed.

        Args:
            y (float): The upper y-value of the data points to remove.
        """
        idx = 0
        while idx < len(self):
            if self.yvalues[idx] <= y:
                self.removePointIndex(idx)
                continue
            idx += 1

    def removePointsAboveY(self, y):
        """
        Remove data points above the given y-value.

        If more than one y-value occurs above the value, all matching
        data points are removed.

        Args:
            y (float): The lower y-value of the data points to remove.
        """
        idx = 0
        while idx < len(self):
            if self.yvalues[idx] >= y:
                self.removePointIndex(idx)
                continue
            idx += 1

    def findPoint(self, check, begin=None, end=None, reverse=False):
        """
        Find data point where the given condition is true.

        Args:
            check (function): Function to be evaluated at each point,
                    e.g. ``lambda x: x == 0``.
            begin (int): Index of the first data point to check.
            end (int): Index of the last data point to check.
            reverse (bool): ``True`` to change the search direction
                    (i.e. start with the last data point).

        Returns:
            int: Index of the data point, or ``None`` if no match was found.
        """
        if self.isEmpty():
            return None

        if reverse == False:
            idx = 0
            while idx < len(self) - 1:
                if begin != None:
                    if self.xvalues[idx] < begin:
                        idx += 1
                        continue
                if end != None:
                    if self.xvalues[idx] > end:
                        idx += 1
                        continue
                if check(self.yvalues[idx], self.yvalues[idx+1]) == True:
                    return idx
                idx += 1
        else:
            idx = len(self)
            while idx > 1:
                if begin != None:
                    if self.xvalues[idx] > begin:
                        idx -= 1
                        continue
                if end != None:
                    if self.xvalues[idx] < end:
                        idx -= 1
                        continue
                if check(self.yvalues[idx], self.yvalues[idx-1]) == True:
                    return idx
                idx -= 1
        return None

########################################################################

class Hist(Graph):
    """
    Basic class for a histogram object with a number data points.

    The class inherits from :py:class:`Peaberry.Base.Graph`, but adds
    some more arrays to hold the bin data (edges, sizes and centers).

    """
    def __init__(self, name, label="", plot=None, fit=None, secondary=False, colorvalue=None, barplot=True):
        Graph.__init__(self, name, label, plot, fit,
                secondary=secondary, colorvalue=colorvalue)

        self.xdata      = np.array([])
        self.xbins      = np.array([])
        self.xvalues    = np.array([])
        self.yvalues    = np.array([])
        self.weights    = np.array([])
        self.yerrorsLo  = np.array([])
        self.yerrorsHi  = np.array([])

        self.setBarplot(barplot)

    def allocatePoints(self, n, weights=False):
        """
        Allocate memory for the given number of points.

        Note:
            Use ``setPoint`` with an index to modify data in the
            allocated array.

        Args:
            n (int): Number of points to allocate.
            weights (bool): Allocate space for weights.
        """
        self.xdata = np.zeros(n)
        if weights == True:
            self.weights = np.zeros(n)

    def setPoint(self, i, x, weight=None):
        """
        Set the data point with given index.

        Note:
            Use ``allocatePoints`` to prepare data arrays.
        """
        if i < 0 or i >= len(self.xdata):
            raise ValueError

        self.xdata[i] = x
        if weight != None:
            if not self.isWeighted():  # make sure that array exists
                self.weights = np.zeros(len(self))
            self.weights[i] = weight

    def setPoints(self, x, weights=None):
        """
        Set the data points in this graph to the given values.

        Note::
            This function silently replaces any existing data points.

        Args:
            x (list): A list of x-values to set.
            weights (list): An optional list of weights to set.

        Raises:
            ValueError: If the length of y-values (or x-/y-errors) does
                not match the length of x-values.
        """
        self.xdata = np.array(x)
        if weights != None:
            if not len(weights) == len(x):
                raise ValueError
            self.weights = np.array(weights)

    def addPoint(self, x, weight=None):
        """
        Add the given data point to this histogram.

        Note:
            You must run ``build`` to create a histogram from the
            given data set after it has been filled with values.

        Args:
            x (list): An x-values to add.
        """
        self.xdata = np.append( self.xdata, x )

        if weight != None:
            self.weights = np.append( self.weights, weight)

    def removePointIndex(self, idx):
        """
        Remove a data point at the given index.

        This function supports negative indexing as well (to address
        elements starting from the end of the array).

        Args:
            idx (int): The index of the data point to remove.
        """
        if -len(self) <= idx < len(self):
            self.xdata = np.delete(self.xdata, idx)
            if self.isWeighted():
                self.weights = np.delete(self.weights, idx)
        else:
            message("Warning: index '{0}' out of range, no points removed".format(idx), severity = 2)

    def build(self, bins=20, **kwargs):
        """
        Create a histogram from the given data set.

        Note:
            You must run ``addPoint`` beforehand to fill the data set.
        """
        weights = self.weights if self.isWeighted() else None
        n, bins = np.histogram(self.xdata, weights=weights, bins=bins, **kwargs)

        self.xbins   = bins
        self.xvalues = bins[:-1]  # left edges of bins
        self.yvalues = n

    def buildFrom(self, xdata, weights=None, bins=20, **kwargs):
        """
        Create new histogram from given data.

        This function uses NumPy's ``histogram`` method to compute
        histogram bins from the given data set. Optional arguments
        to this method are passed on to NumPy.

        Args:
            data (list): Data set to construct histogram from.
            \*\*kwargs: Options passed to ``numpy.histogram``.
        """
        n, xbins = np.histogram(xdata, weights=weights, bins=bins, **kwargs)

        self.xdata   = xdata
        self.xbins   = xbins
        self.xvalues = xbins[:-1]  # left edges of bins
        self.yvalues = n[:]
        self.weights = weights

    def setBarplot(self, barplot=True):
        """
        Set the plotting style of the histogram.

        Args:
            barplot (bool): If ``True``, the histogram will be plotted
            using ``matplotlib.pyplot.bar``; otherwise, it will be
            plotted using ``matplotlib.pyplot.plot`` with a stepped
            linestyle.
        """
        self.barplot = barplot

    def isHistogram(self):
        """
        See :py:func:`Peaberry.Base.Graph.isHistogram`.
        """
        return True

    def isWeighted(self):
        return (self.weights.size > 0)

    def getWeights(self):
        return self.weights.copy()

    def getXdata(self):
        return self.xdata.copy()

    def getXbins(self):
        return self.xbins.copy()

    def getRangeX(self):
        """
        Return the lower/upper limit of x-values.

        Returns:
            tuple: The x-range of data points, like ``(l, h)``.
        """
        return (self.xbins.min(), self.xbins.max())  # use bins here to get correct bounds

    def getRangeY(self):
        """
        Return the lower/upper limit of x-values.

        Returns:
            tuple: The x-range of data points, like ``(l, h)``.
        """
        return (self.yvalues.min(), self.yvalues.max())

    def save(self, filename, fileformat=None, header=True):
        """
        Export this histograms's contents to a file.

        Note::
            The file format is determined from the given filename
            by default, but can be overwritten by the `fileformat`
            argument.
            Supported types are:
                `json`,
                `dat` (gnuplot-style),
                `csv` (excel-style).

        Note::
            This function will only save the graph's data. The output
            file will not include information about histogram bins
            or an associated fit model.

        Args:
            filename (string): The filename of the output file.
            fileformat (string): An optional identifier for the file
                    format (default: determine from filename).
            header (bool): If `True`, a header line will be written
                    if the file format supports it.
        """

        if fileformat == None:
            fileformat = os.path.splitext(filename)[1][1:]

        with open(filename, 'w') as f:
            if fileformat == 'json':
                result = OrderedDict()
                result['name']    = self.name
                result['label']   = self.label
                result['xdata']   = self.xdata.tolist()
                result['xbins']   = self.xbins.tolist()
                result['xvalues'] = self.xvalues.tolist()
                result['yvalues'] = self.yvalues.tolist()
                result['weights'] = self.weights.tolist()

                json.dump(result, f, indent=4, sort_keys=False)  # already sorted

            else:
                raise ValueError("Unsopported file format {} - histogram {} was not saved".format(fileformat, self.name))

    def load(self, filename, keeplabel=True):
        """
        Import histogram contents from a JSON.

        Note::
            Although py:func:export supports other file formats than
            JSON, this function only operates on JSON files. One can
            use dedicated modules like py:mod:CSV to read non-JSON
            data files.

        Args:
            filename (string): The filename of the output file.
            keeplabel (bool): If `True`, do not override this graph's
                label with the one from the import file.
        """

        with open(filename, 'r') as f:
            result = json.load(f)

            if keeplabel == False:
                self.label = result['label']

            self.xdata   = np.array(result['xdata'])
            self.xbins   = np.array(result['xbins'])
            self.xvalues = np.array(result['xvalues'])
            self.yvalues = np.array(result['yvalues'])
            self.weights = np.array(result['weights'])

    def append(self, other):
        """
        Append another histogram to this one.

        The x- and y-values will from ``other'' will be appended to the
        values of this histogram.

        Note:
            This function will change the data points of this histogram in-place.

        Args:
            other (instance): Another histogram to be combined into this one.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Hist``.
        """
        if not isinstance(other, Hist):
            raise TypeError

        self.xdata   = np.append(self.xdata, other.xdata)
        self.xbins   = np.append(self.xbins, other.xbins)
        self.xvalues = np.append(self.xvalues, other.xvalues)
        self.yvalues = np.append(self.yvalues, other.yvalues)
        self.weights = np.append(self.weights, other.weights)
        if self.hasErrorsY():
            if not other.hasErrorsY():
                raise ValueError
            self.yerrorsLo = np.append(self.yerrorsLo, other.yerrorsLo)
            self.yerrorsHi = np.append(self.yerrorsHi, other.yerrorsHi)

    def stack(self, other):
        """
        Stack another histogram on top of this one.

        The y-values (and if defined, the y-errors) of the other
        histogram will be added to all data points in this one.

        Note:
            This function will change the data points of this histogram in-place.

        Args:
            other (instance): Another graph to be combined into this one.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Hist``.
            DomainError - If both graphs have different lengths.
        """
        if not isinstance(other, Hist):
            raise TypeError

        if not (self.xbins == other.xbins).all():
            raise ValueError

        self.yvalues += other.yvalues
        if self.hasErrorsY():
            if not other.hasErrorsY():
                raise ValueError
            self.yerrorsLo += other.yerrorsLo
            self.yerrorsHi += other.yerrorsHi

########################################################################

class Hist2D(Hist):
    """
    Basic class for a 2D histogram object with a number data points.

    The class inherits from :py:class:`Peaberry.Base.Graph`, but adds
    some more arrays to hold the bin data (edges, sizes and centers).

    """
    def __init__(self, name, label="", plot=None, fit=None, secondary=False, colorvalue=None):
        Graph.__init__(self, name, label, plot, fit,
                secondary=secondary, colorvalue=colorvalue)

        self.xdata      = np.array([])
        self.ydata      = np.array([])
        self.xbins      = np.array([])
        self.ybins      = np.array([])
        self.xvalues    = np.array([])
        self.yvalues    = np.array([])
        self.zvalues    = np.array([])
        self.weights    = np.array([])

    def allocatePoints(self, n, weights=False):
        """
        Allocate memory for the given number of points.

        Note:
            Use ``setPoint`` with an index to modify data in the
            allocated array.

        Args:
            n (int): Number of points to allocate.
            weights (bool): Allocate space for weights.
        """
        self.xdata = np.zeros(n)
        self.ydata = np.zeros(n)
        if weights == True:
            self.weights = np.zeros(n)

    def setPoint(self, i, x, y, weight=None):
        """
        Set the data point with given index.

        Note:
            Use ``allocatePoints`` to prepare data arrays.
        """
        if i < 0 or i >= len(self.xdata):
            raise ValueError

        self.xdata[i] = x
        self.ydata[i] = y
        if weight != None:
            if not self.isWeighted():  # make sure that array exists
                self.weights = np.zeros(len(self))
            self.weights[i] = weight

    def setPoints(self, x, y, weights=None):
        """
        Set the data points in this graph to the given values.

        Note::
            This function silently replaces any existing data points.

        Args:
            x (list): A list of x-values to set.
            weights (list): An optional list of weights to set.

        Raises:
            ValueError: If the length of y-values (or x-/y-errors) does
                not match the length of x-values.
        """
        if not len(x) == len(y):
            raise ValueError

        self.xdata = np.array(x)
        self.ydata = np.array(y)
        if weights != None:
            if not len(weights) == len(x):
                raise ValueError
            self.weights = np.array(weights)

    def addPoint(self, x, y, weight=None):
        """
        Add the given data point to this histogram.

        Note:
            You must run ``build`` to create a histogram from the
            given data set after it has been filled with values.

        Args:
            x (list): An x-values to add.
            y (list): An y-values to add.
        """
        self.xdata = np.append(self.xdata, x)
        self.ydata = np.append(self.ydata, y)

        if weight != None:
            self.weights = np.append(self.weights, weight)

    def removePointIndex(self, idx):
        """
        Remove a data point at the given index.

        This function supports negative indexing as well (to address
        elements starting from the end of the array).

        Args:
            idx (int): The index of the data point to remove.
        """
        if -len(self) <= idx < len(self):
            self.xdata = np.delete(self.xdata, idx)
            self.ydata = np.delete(self.ydata, idx)
            if self.isWeighted():
                self.weights = np.delete(self.weights, idx)
        else:
            message("Warning: index '{0}' out of range, no points removed".format(idx), severity = 2)

    def build(self, bins=20, **kwargs):
        """
        Create a histogram from the given data set.

        Note:
            You must run ``addPoint`` beforehand to fill the data set.
        """
        n, xbins, ybins = np.histogram2d(self.xdata, self.ydata, weights=self.weights, bins=bins, **kwargs)

        self.xbins   = xbins
        self.ybins   = ybins
        self.xvalues = xbins[:-1]  # left edges of bins
        self.yvalues = ybins[:-1]  # left edges of bins
        self.zvalues = n

    def buildFrom(self, xdata, ydata, weights=None, bins=20, **kwargs):
        """
        Create new histogram from given data.

        This function uses NumPy's ``histogram`` method to compute
        histogram bins from the given data set. Optional arguments
        to this method are passed on to NumPy.

        Args:
            data (list): Data set to construct histogram from.
            \*\*kwargs: Options passed to ``numpy.histogram``.
        """

        n, xbins, ybins = np.histogram2d(xdata, ydata, weights=weights, bins=bins, **kwargs)

        self.xdata   = xdata
        self.ydata   = ydata
        self.xbins   = xbins
        self.ybins   = ybins
        self.xvalues = xbins[:-1]  # left edges of bins
        self.yvalues = ybins[:-1]  # left edges of bins
        self.zvalues = n
        self.weights = weights

    def is2D(self):
        """
        See :py:func:`Peaberry.Base.Graph.is2D`.
        """
        return True

    def getYdata(self):
        return self.ydata.copy()

    def getYbins(self):
        return self.ybins.copy()

    def getZvalues(self, filtered=False, invert=False, thinning=0):
        """
        Retrieve z-values in this graph.

        Args:
            filtered (bool): ``True`` to return a filtered selection.
            invert (bool): ``True`` to invert the selection in filtered output.

        See also:
            Function :py:func:`filter`.
        """
        if filtered:
            return self.filter(self.zvalues, invert=invert, thinning=thinning)
        else:
            return thinned(self.zvalues, thinning=thinning)

    def getRangeY(self):
        """
        Return the lower/upper limit of y-values.

        Returns:
            tuple: The y-range of data points, like ``(l, h)``.
        """
        return (self.ybins.min(), self.ybins.max())  # use bins here to get correct bounds

    def getRangeZ(self):
        """
        Return the lower/upper limit of z-values.

        Returns:
            tuple: The z-range of data points, like ``(l, h)``.
        """
        return (self.zvalues.min(), self.zvalues.max())

    def save(self, filename, fileformat=None, header=True):
        """
        Export this histograms's contents to a file.

        Note::
            The file format is determined from the given filename
            by default, but can be overwritten by the `fileformat`
            argument.
            Supported types are:
                `json`,
                `dat` (gnuplot-style),
                `csv` (excel-style).

        Note::
            This function will only save the graph's data. The output
            file will not include information about histogram bins
            or an associated fit model.

        Args:
            filename (string): The filename of the output file.
            fileformat (string): An optional identifier for the file
                    format (default: determine from filename).
            header (bool): If `True`, a header line will be written
                    if the file format supports it.
        """

        if fileformat == None:
            fileformat = os.path.splitext(filename)[1][1:]

        with open(filename, 'w') as f:
            if fileformat == 'json':
                result = OrderedDict()
                result['name']    = self.name
                result['label']   = self.label
                result['xdata']   = self.xdata.tolist()
                result['ydata']   = self.ydata.tolist()
                result['xbins']   = self.xbins.tolist()
                result['ybins']   = self.ybins.tolist()
                result['xvalues'] = self.xvalues.tolist()
                result['yvalues'] = self.yvalues.tolist()
                result['zvalues'] = self.zvalues.tolist()
                result['weights'] = self.weights.tolist()

                json.dump(result, f, indent=4, sort_keys=False)  # already sorted

            else:
                raise ValueError("Unsopported file format {} - histogram {} was not saved".format(fileformat, self.name))

    def load(self, filename, keeplabel=True):
        """
        Import histogram contents from a JSON.

        Note::
            Although py:func:export supports other file formats than
            JSON, this function only operates on JSON files. One can
            use dedicated modules like py:mod:CSV to read non-JSON
            data files.

        Args:
            filename (string): The filename of the output file.
            keeplabel (bool): If `True`, do not override this graph's
                label with the one from the import file.
        """

        with open(filename, 'r') as f:
            result = json.load(f)

            if keeplabel == False:
                self.label = result['label']

            self.xdata   = np.array(result['xdata'])
            self.ydata   = np.array(result['ydata'])
            self.xbins   = np.array(result['xbins'])
            self.ybins   = np.array(result['ybins'])
            self.xvalues = np.array(result['xvalues'])
            self.yvalues = np.array(result['yvalues'])
            self.zvalues = np.array(result['zvalues'])
            self.weights = np.array(result['weights'])

    def append(self, other):
        """
        Append another histogram to this one.

        The x-, y- and z-values will from ``other'' will be appended to
        the values of this histogram.

        Note:
            This function will change the data points of this histogram in-place.

        Args:
            other (instance): Another 2D histogram to be combined into this one.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Hist2D``.
        """
        if not isinstance(other, Hist2D):
            raise TypeError

        self.xdata   = np.append(self.xdata, other.xdata)
        self.ydata   = np.append(self.ydata, other.ydata)
        self.xbins   = np.append(self.xbins, other.xbins)
        self.ybins   = np.append(self.ybins, other.ybins)
        self.xvalues = np.append(self.xvalues, other.xvalues)
        self.yvalues = np.append(self.yvalues, other.yvalues)
        self.zvalues = np.append(self.zvalues, other.zvalues)
        self.weights = np.append(self.weights, other.weights)

    def stack(self, other):
        """
        Stack another histogram on top of this one.

        The y-values (and if defined, the y-errors) of the other
        histogram will be added to all data points in this one.

        Note:
            This function will change the data points of this histogram in-place.

        Args:
            other (instance): Another graph to be combined into this one.

        Raises:
            TypeError - If ``other`` is not an instance of ``Peaberry.Base.Hist``.
            DomainError - If both graphs have different lengths.
        """
        if not isinstance(other, Hist2D):
            raise TypeError

        if not ( (self.xbins == other.xbins).all() and (self.ybins == other.ybins).all() ):
            raise ValueError

        self.zvalues += other.zvalues

########################################################################
